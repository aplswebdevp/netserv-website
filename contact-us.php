<!DOCTYPE html>
<html lang="zxx">

<head>
   <!-- meta tag -->
   <meta charset="utf-8">
   <title>NetServ - Contact-Us</title>
   <meta name="description" content="If you have any questions about any of our services, Please complete the request form and one of our Technology experts will contact you.">
   <meta name="keywords" content="mail us, contact us at email, contact us page, contact us form, contact us form for website, contact us by mail, contact us with email, contact with email,">
   <!-- responsive tag -->
   <meta http-equiv="x-ua-compatible" content="ie=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- favicon -->
   <link rel="apple-touch-icon" href="">
   <link rel="canonical" href="https://www.ngnetserv.com/contact-us" />
   <?php include 'service_csslinks.php'; ?>
   <link rel="shortcut icon" type="image/x-icon" href="<?php echo main_url; ?>/assets/images/favicon.png">
   <link rel="stylesheet" type="text/css" href="<?php echo main_url; ?>/assets/css/contact_us.css">
   <script type='application/ld+json'>
      {
         "@context": "http://www.schema.org",
         "@type": "WebSite",
         "name": "NetSev",
         "url": "http://www.ngnetserv.com/"
      }
   </script>
   <style type="text/css">
      .rs-breadcrumbs.bg-1 {
         /* background-image: url(assets/images/breadcrumbs/1.jpg); */
         background-image: linear-gradient(240deg, #ffffff 0%, rgb(234 235 237 / 60%) 50%, rgb(255 255 255 / 0%) 100%), url('assets/images/contactus.jfif');
         background-position: center;
         background-repeat: no-repeat;
         background-size: cover;
      }
   </style>
</head>

<body>
   <!-- Preloader area start here -->
   <!-- Google Tag Manager (noscript) -->
   <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
   <!-- End Google Tag Manager (noscript) -->
   <!--End preloader here -->
   <!--Full width header Start-->
   <div class="full-width-header header-style4">
      <!--Header Start-->
      <?php include 'header.php'; ?>
      <!--Header End-->
   </div>
   <!-- Main content Start -->
   <div class="main-content">
      <!-- Breadcrumbs Section Start -->
      <div class="rs-breadcrumbs bg-1">
         <div class="container">
            <div class="content-part text-center">
               <h1 class="breadcrumbs-title mb-0">Contact</h1>
            </div>
         </div>
      </div>
      <div class="rs-cta mt-5 mb-5">
         <div class="Container-fluid">
            <div class="content part pt-50 pb-50 md-pt-50 md-pb-50">
               <div class="sec-title2">
                  <p class="title title2 title4 pb-30 text-center sec-title2 p-heading text-heading">
                     If you have any questions about any of our services, Please complete the request form and one of our Technology experts will contact you shortly !
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="main-content">
      <!--test-->
      <div class="rs-contact style1 gray-bg pt-35 pb-35 md-pt-80 md-pb-80">
         <div class="container">
            <div class="white-bg">
               <div class="row">
                  <div class="col-lg-8 form-part">
                     <div class="sec-title mb-35 md-mb-30">
                        <div class="sub-title text- primary">CONTACT US</div>
                        <h2 class="title mb-0">Get In Touch</h2>
                     </div>
                     <div id="form-messages"></div>
                     <?php include 'contact.php'; ?>
                  </div>

                  <div class="col-lg-4 pl-0 md-pl-pr-15 md-order-first">
                     <div class="contact-info">
                        <div class="contact_txt_center ">
                           <h3 class="title">Contact Info</h3>

                           <div class="info-wrap mb-20 mt-5">
                              <div class="icon-part">
                                 <i class="flaticon-email"></i>
                              </div>
                              <div class="content-part">
                                 <h4>Mail Us</h4>
                                 <p> info@ngnetserv.com</p>
                              </div>
                           </div>

                           <div class="info-wrap mb-25">
                              <div class="icon-part">
                                 <i class="flaticon-location"></i>
                              </div>
                              <div class="content-part">
                                 <h4>US Office</h4>
                                 <p>4580, Auto Mall Pkwy STE 121, Fremont,
                                    CA 94538, USA<br>Phone: 4083378781</p>
                              </div>
                           </div>
                           <div class="info-wrap mb-25">
                              <div class="icon-part">
                                 <i class="flaticon-location"></i>
                              </div>
                              <div class="content-part">
                                 <h4>India Office</h4>
                                 <p>
                                    104 & 105, A-Wing, Shreenath Plaza, FC Road, Shivajinagar, Pune, Maharashtra 411005, India.<br> Phone: 020-48648865</p>
                              </div>
                           </div>
                           <div class="info-wrap mb-25">
                              <div class="icon-part">
                                 <i class="flaticon-location"></i>
                              </div>
                              <div class="content-part">
                                 <h4>Philippines Office</h4>
                                 <p>
                                    AEJ Building, #12 Q. Abeto St., Abeto -Mirasol, Mandurriao, Iloilo City, Philippines


                                 </p>
                              </div>
                           </div>

                           <div class="info-wrap mb-20 pt-20 mt-5">
                              <div class="icon-part">
                                 <i class="flaticon-comment"></i>
                              </div>

                              <div class="content-part">
                                 <h4>Connect with</h4>
                                 <p><a href="https://twitter.com/netservio"><i class="fa fa-twitter"></i></a><a href="https://www.linkedin.com/company/netserv-llc/"><i class="fa fa-linkedin pl-4"></i></a>
                                    <a href="https://www.youtube.com/channel/UCcTzZVwmHwFHK-YOYb51yOA/featured"><i class="fa fa-youtube-play pl-4"></i></a>
                                    <!-- <a><i class="fa fa-facebook pl-4"></i></a> -->
                                 </p>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--#test-->


   </div>
   <!-- Main content End -->
   <!-- Main content End -->
   <!-- Footer Start -->
   <?php include 'footer.php'; ?>
   <!-- Footer End -->
   <!-- start scrollUp  -->
   <div id="scrollUp">
      <i class="fa fa-angle-up"></i>
   </div>
   <!-- End scrollUp  -->
   <?php include 'service_jslinks.php'; ?>
   <script src="<?php echo main_url; ?>/assets/js/contact.form.js"></script>
   <script src="<?php echo main_url; ?>/assets/js/contact-form-script.js"></script>
</body>

</html>