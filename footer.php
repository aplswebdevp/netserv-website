<footer id="rs-footer" class="rs-footer">
    <div class="container">
        <div class="footer-content pt-50 pb-20 md-pb-30">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12 footer-widget md-mb-39 text-white">
                    <div class="about-widget pr-15">
                        <h4 class="text-white">Company</h4>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/about" class="text-white">About</a></p>

                        <p class="mb-1"><a href="<?php echo main_url; ?>/terms" class="text-white">Terms</a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/privacy-policy" class="text-white">Privacy Policy</a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/become-a-partner" class="text-white">Become a Partner</a></p>

                        <p class="mb-1"><a href="<?php echo main_url; ?>/contact-us" class="text-white">Contact</a></p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 footer-widget md-mb-39 text-white">
                    <div class="about-widget pr-15">
                        <h4 class="text-white">Services</h4>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/consulting-services/consulting" class="text-white">Consulting</a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/professional-services/professional-services" class="text-white">Professional Services</a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/managed-services/managed-services" class="text-white">Managed Services</a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/software-development/software-development" class="text-white">Software Development</a></p>
                        <!-- <p class="mb-1"><a href="<?php echo main_url; ?>/services/training/training" class="text-white">Training</a></p> -->

                        <!-- <p class="mb-1"><a href="<?php echo main_url; ?>/services/services-startups/services-for-startups" class="text-white">Services for Startups</a></p> -->

                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 footer-widget md-mb-39 text-white">
                    <div class="about-widget">
                        <h4 class="text-white">Managed Services</h4>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/managed-services/managed-cybersecurity-services" class="text-white">Cybersecurity </a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/managed-services/managed-soc-and-noc-services" class="text-white">SOC and NOC</a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/managed-services/managed-network-services" class="text-white">Network and Unified Communication</a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/managed-services/managed-data-backup-and-disaster-recovery-services" class="text-white">Data Backup and Disaster Recovery</a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/managed-services/managed-cloud-services" class="text-white">Cloud Optimization</a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/consulting-services/advisory-services" class="text-white">Consulting and Advisory </a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/managed-services/managed-infrastructure" class="text-white">Infrastructure</a></p>
                        <!-- <p class="mb-1"><a href="<?php echo main_url; ?>/services/managed-services/managed-daas-and-vdi" class="text-white">Managed VDI/DaaS</a></p> -->
                        <!-- <p class="mb-1"><a href="<?php echo main_url; ?>/services/managed-services/managed-infrastructure" class="text-white">Managed Infrastructure</a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/managed-services/managed-daas-and-vdi" class="text-white">Managed VDI/DaaS Services</a></p> -->

                        <!-- <p class="mb-1"><a href="<?php echo main_url; ?>/services/managed-services/application-management-services" class="text-white">
                                Application Managed Services</a></p> -->
                        <!-- <p class="mb-1"><a href="<?php echo main_url; ?>/services/managed-services/managed-cloud-services" class="text-white">Managed Cloud Services</a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/managed-services/managed-security-services" class="text-white">
                                Managed Security Services</a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/managed-services/full-stack-managed-services" class="text-white">Full Stack Managed AIOps</a></p>
                     -->
                    </div>
                </div>

                <!-- <div class="col-lg-3 col-md-12 col-sm-12 footer-widget md-mb-39 text-white">
                    <div class="about-widget pr-15">
                        <h4 class="text-white">Services for Startups</h4>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/services-startups/support-services-startups" class="text-white">Support Services</a></p>
                        
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/software-development/software-development" class="text-white">Software Development</a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/services-startups/partner-enablement" class="text-white">Partner Enablement</a></p>
                        <p class="mb-1"><a href="<?php echo main_url; ?>/services/services-startups/managed-services-startups" class="text-white">
                                Managed Services</a></p>
                    </div>
                </div> -->

            </div>
        </div>
        <div class="footer-bottom p-3">
            <div class="row y-middle">
                <div class="col-lg-6 col-md-8 col-sm-6 col-5 sm-mb-21">
                    <div class="copyright">
                        <p class="text-white" id="footer-id">© <span id="autoupdate-year"></span> NetServ. All Rights Reserved.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-4 col-sm-5 col-7 text-right sm-text-center mt-3">
                    <ul class="footer-social">
                        <li><a href="https://twitter.com/netservllc"><i class="fa fa-twitter" alt='twitter-image' title="twitter"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/netserv-llc/" alt='linkedin-image' title="linkedin"><i class="fa fa-linkedin"></i></a></li>
                        <li> <a href="https://www.youtube.com/channel/UCcTzZVwmHwFHK-YOYb51yOA/featured" alt='youtube-image' title="youtube"><i class="fa fa-youtube-play"></i></a> </li>
                        <!-- <li> <a><i class="fa fa-facebook" alt='facebook-image' title="facebook"></i></a> </li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<script>
    // start code for automatically update year
    document.getElementById('autoupdate-year').innerHTML = new Date().getFullYear();
    // end code for automatically update year
</script>