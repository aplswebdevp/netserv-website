<!DOCTYPE html>
<html lang="en">

<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Success stories</title>
    <meta name="description" content="NetServ has been our go-to partner for our advanced global networking requirements, bringing extensive experience of advanced networking skillset.">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/success-stories#Customer-Success-SD-campus" />
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <link rel="stylesheet" type="text/css" href="assets/css/about.css">
    <link rel="stylesheet" type="text/css" href="assets/css/success-stories.css">
    <script type='application/ld+json'>
        {
            "@context": "http://www.schema.org",
            "@type": "WebSite",
            "name": "NetSev",
            "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style>
    .scrol-div {
        width: 100%;
        height: auto;
        overflow: auto;
    }

    .scrol {
        width: 100%;
        height: 227px;
        /* overflow: auto; */
    }

    .scrol-div::-webkit-scrollbar-thumb {
        background: rgba(0, 0, 0, .2);
        border-radius: 10px;
    }

    .scrol-div::-webkit-scrollbar {
        width: 0.3em;

    }
</style>

<body>
    <!-- Preloader area start here -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!--End preloader here -->
    <!--Full width header Start-->
    <div class="full-width-header header-style4">
        <!--Header Start-->
        <?php include 'header.php'; ?>
        <!--Header End-->
    </div>
    <!--Full width header End-->
    <!-- Main content Start -->
    <div class="main-content">
        <div class="rs-project style1 bg13 pt-100 pb-100 md-pt-80 md-pb-80">
            <div class="container">
                <div class="row mb-60 md-mb-40 lg-col-padding">
                </div>
                <div class="row lg-col-padding">
                    <div class="col-xl-6">
                        <div class="video-part border border-info">
                            <img src="<?php echo main_url; ?>/assets/images/services/sucessstory/main-img.jpg" alt="Customer Success Stories" title="Customer Success Stories">
                        </div>
                    </div>
                    <div class="col-xl-6 pl-55">
                        <div class="sec-title">
                            <h1 class="title mb-14 txt_clr" style="font-size: 36px">Customer Success Stories</h1>
                            <p class="desc mb-46">SD-WAN - Design, Implementation and Migration NetServ has been our go-to partner for our advanced global networking requirements, bringing extensive experience of advanced networking skillset. Because of their knowledge, not only in SD-WAN but also with advanced networking and WAN expertise they were able to design, implement and migrate a vendor agnostic SD-WAN solution that really solved our business challenges.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- title section starts -->
        <div class="rs-questions style1 pt-80 pb-80  md-pb-80" id="Customer-Success-SD-WAN">
            <div class="container">
                <div class="row md-col-padding">
                    <div class="col-lg-6 pl-40">
                        <h2 class="title mb-21 md-mb-10"><span class="txt_clr">Customer Success </span> SD-WAN
                        </h2>
                        <h5 class="mb-2">SD-WAN Design and Implementation</h5>
                        <p class="desc mb-46">Whether you are looking for global networking design, running into security, bandwidth, capacity, performance and compliance issues, NetServ has more than two decades of experience in design and deployment of advanced networking solutions. As a leader in SD-WAN design and deployment, we will help you with a vendor agnostic advanced networking/ SD-WAN solution which will meet your business outcomes.</p>
                    </div>
                    <div class="col-lg-6 pr-30 md-mb-30 xs-mb-0">
                        <div class="row gutter-20 hidden-xs">
                            <img src="<?php echo main_url; ?>/assets/images/services/sucessstory/deal1.jpg" alt="Customer Success Stories" title="Customer Success Stories">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- title section ends  -->
        <!-- customer challenges starts  -->
        <div class="rs-about style10  pt-130 pb-100 md-pt-70 md-pb-70" data-aos="fade-up" data-aos-duration="2000">
            <div class="container">

                <div class="row">
                    <div class="col-md-12 pb-50" align="center">
                        <div class="sec-title4 mb-30">
                            <!--  <span class="sub-title new pb-10">About Us</span> -->
                            <h2 class="title pb-20">Customer Challenges</h2>

                            <p class="">One of our customers is a global leader in developing, manufacturing, and marketing broad range of innovative products<br> for life science research and clinical diagnosis had following technology challenges:</p>

                        </div>
                    </div>
                    <div class="col-lg-6 pr-70 md-pr-15 md-mb-50">
                        <ul class="listing-style2 mb-33">
                            <li>Customer’s existing legacy network had huge latency and poor internet performance.</li>
                            <li>To enforce security policies with Legacy Architecture, they were backhauling traffic to a data center which was causing additional latency.</li>
                            <li>Management issues with HIPAA Compliance.</li>
                            <li>There was not real visibility.</li>
                            <li>No central point of management.</li>
                            <li>Lack of Automation.</li>
                            <li>Customer’s resources, skills, and knowledge specific to building a new architecture were limited.</li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-content">
                            <div class="images-part">
                                <img src="<?php echo main_url; ?>/assets/images/services/sucessstory/success1.jpg" alt="Customer Success Stories" title="Customer Success Stories">
                            </div>
                            <div class="rs-animations">
                                <div class="spinner dot">
                                    <img class="scale" src="assets/images/about/solutions/2.png" alt="Images">
                                </div>
                                <div class="spinner ball">
                                    <img class="dance2" src="assets/images/about/solutions/3.png" alt="Images">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- customer challenges ends  -->
        <!-- why netserv starts  -->
        <div id="rs-about" class="rs-about style1 gray-bg md-pt-80 pb-70">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12 pt-70 pb-40" align="center">
                        <h3 class="sub-title">Why NetServ ?</h3>
                    </div>
                    <div class="col-lg-6">

                        <ul class="listing-style2 mb-33">
                            <li>Ability to rapidly provide a diverse team of experienced architects and engineers to handle the scope and deadline required for this initiative.</li>

                        </ul>

                    </div>
                    <div class="col-lg-6 ">

                        <ul class="listing-style2 mb-33">
                            <li>Deep Cloud infrastructure expertise.</li>
                            <li>Deep knowledge of advanced Networking architectures.</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <!-- why netserv ends  -->
        <!-- solution Section Start -->
        <div class="rs-counter style1 modify  pt-92 pb-100 md-pt-72 md-pb-80 text-black">
            <div class="container">
                <div class="row y-middle">
                    <div class="col-md-12 sec-title text-center mb-52 md-mb-29">
                        <h3 class="title mb-0 "><span class="txt_clr">Solution:</span> SD-WAN Design and Deployment</h3>
                    </div>
                    <div class="col-lg-6 md-mb-30">

                        <ul class="listing-style2 mb-33">
                            <li>NetServ team helped design, implement and migrate to SD-WAN solution.</li>
                            <li>We performed a detailed assessment with key Network architects, business stakeholders, creating a clear current state of the customer’s network environment, focusing on the current challenges and the desired future state and gaps.</li>
                            <li>Next, our team designed the future-state SD-WAN architecture focused on meeting the defined business needs, recommending an initial release to POV of the proposed future state.</li>
                            <li>Deployed SD-WAN with centralized Orchestration and Zero-touch provisioning.</li>
                            <li>Traffic isolation for Control and Data forwarding plane.</li>
                            <li>Application driven routing policy implementation.</li>
                            <li>Secure and Scalable global connectivity.</li>
                        </ul>
                    </div>
                    <div class="col-lg-6 pl-50 lg-pl-35 md-pl-15">
                        <img src="<?php echo main_url; ?>/assets/images/services/sucessstory/analysis1.jpg" alt="Customer Success Stories" title="Customer Success Stories">
                    </div>
                </div>
            </div>
        </div>
        <!-- solution Section End -->
        <!-- business outcome starts -->
        <div class="rs-questions style1 pt-100 pb-100 md-pt-173 md-pb-80">
            <div class="container">
                <div class="row md-col-padding">
                    <div class="col-md-12 sec-title text-center mb-52 md-mb-29">
                        <h3 class="title mb-21 md-mb-10 sm-mt-20">Our <span class="txt_clr">Business Outcomes</span></h3>
                    </div>
                    <div class="col-lg-6 pr-30 md-mb-30 xs-mb-0">
                        <img class="fullWidth" src="<?php echo main_url; ?>/assets/images/services/sucessstory/outcome3.jpg" alt="Customer Success Stories" title="Customer Success Stories">
                    </div>
                    <div class="col-lg-6 pl-40">
                        <ul class="listing-style2 mb-33">
                            <li> Reduced CAPEX and OPEX by decommissioning MPLS</li>
                            <li>Significantly reduced the WAN circuits costs by 65-70% across the globe.</li>
                            <li>Increased the bandwidth at each site by 1000% (10 MB to 100 MB) by investing in low-cost business broadband circuits at each branch location.</li>
                            <li>Successful SD-WAN migration helped customer reduced the CAPEX and OPEX maintenance costs by 50-55%.</li>
                            <li>Centralized Management</li>
                            <li>Higher capacity bandwidth</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- bussiness outcome ends  -->
        <!-- sucess sd campus starts  -->
        <div class="rs-questions style1 gray-bg5 pt-90 pb-90" id="Customer-Success-SD-campus">
            <div class="container">
                <div class="row md-col-padding">
                    <div class="col-md-12 sec-title text-center mb-52 md-mb-29">
                        <h2 class="title mb-21 md-mb-10"><span class="txt_clr">Customer Success </span>SD-Campus</h2>
                    </div>
                    <div class="col-lg-6 pr-30 md-mb-30 xs-mb-0 y-middle">
                        <div class="row gutter-20 hidden-xs">
                            <img src="<?php echo main_url; ?>/assets/images/services/sucessstory/deal2.jpg" alt="Customer Success Stories" title="Customer Success Stories">
                        </div>
                    </div>
                    <div class="col-lg-6 pl-40">
                        <h5 class="mb-2">SD-Campus Design and Implementation</h5>
                        <p class="desc ">NetServ team was there leading our time-sensitive legacy campus network to SD-Campus migration. NetServ team really understood our environment ,challenges and business outcomes, which helped us towards a successful design, implementation and migration to the SD-campus environment.</p>
                        <p class="desc">Whether you are looking for emerging campus networking design, running into security, bandwidth, capacity, performance and compliance issues, NetServ has more than two decades of experience in design and deployment of advanced campus networking solutions. As a leader in SD-Campus design and deployment, we will help you with a vendor agnostic advanced networking/ SD-Campus solution which will meet your business outcomes.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- sucess sd campus ends -->
        <!-- customer challenges starts  -->
        <div class="rs-whychooseus style8 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 sec-title text-center mb-52 md-mb-29 pt-70">
                        <h3 class="title mb-18">Customer Challenges</h3>
                    </div>
                    <div class="col-lg-5 md-mb-50">
                        <ul class="listing-style2 mb-33">
                            <li>Legacy campus networks had many challenges due to limited segmentation, manual configuration and fragmented tools.</li>
                            <li>Lack of consistent security policy enforcement due to rapid growth.</li>
                            <li>Network operational challenges</li>
                            <li>Implementing changes was a major task causing huge delays.</li>
                            <li>HIPAA/PCI Compliance management issues.</li>
                            <li>Lack of centralized management.</li>
                            <li>Delay in provisioning new services to patients.</li>
                            <li>Delay in provisioning new services to staffs.</li>
                            <li>Lack of Automation.</li>
                        </ul>

                    </div>
                    <div class="col-lg-7 pl-70 md-pl-15">
                        <div class="images-part">
                            <img src="assets/images/whychooseus/style8/seo-image.png" alt="images">
                        </div>
                        <div class="rs-animations">
                            <div class="shape-icons one">
                                <img class="dance2" src="assets/images/whychooseus/style8/shape/1.png" alt="images">
                            </div>
                            <div class="shape-icons two">
                                <img class="scale" src="assets/images/whychooseus/style8/shape/2.png" alt="images">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--        <div class="rs-whychooseus style4 pt-100 md-pt-72 md-pt-80 pb-100 lg-pb-93 md-pb-70">-->
        <!--            <div class="container">-->
        <!--                <div class="row md-col-padding">-->
        <!--                    <div class="col-lg-7 pr-110 lg-pr-30">-->
        <!--                        <div class="sec-title mb-40">-->
        <!--                            <h2 class="title mb-18">Customer Challenges</h2>-->
        <!--                        </div>-->
        <!--                        <div class="desc" One of our customers, who is a leader in the healthcare industry, has multiple hospitals and remote clinics. With advanced Bio-Medical and other IOT devices along with advanced staff and patient devices to provide state of the art patient care had following technology challenges:</div>-->
        <!--                            <ul class="listing-style2 mb-33">-->
        <!--                                <li>Legacy campus networks had many challenges due to limited segmentation, manual configuration and fragmented tools.</li>-->
        <!--                                <li>Lack of consistent security policy enforcement due to rapid growth.</li>-->
        <!--                                <li>Network operational challenges</li>-->
        <!--                                <li>Implementing changes was a major task causing huge delays.</li>-->
        <!--                                <li>HIPAA/PCI Compliance management issues.</li>-->
        <!--                                <li>Lack of centralized management.</li>-->
        <!--                                <li>Delay in provisioning new services to patients.</li>-->
        <!--                                <li>Delay in provisioning new services to staffs.</li>-->
        <!--                                <li>Lack of Automation.</li>-->
        <!--                            </ul>-->
        <!--                        </div>-->
        <!--                    </div>-->
        <!--                    <div class="col-lg-5 pl-0 md-mb-30 md-order-first">-->
        <!--                        <div class="image-part"><img src="--><?php //echo main_url; 
                                                                            ?><!--/assets/images/services/sucessstory/success2.jpg" alt="Customer Success Stories" title="Customer Success Stories">-->
        <!--                        </div>-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <!-- customer challenges ends  -->
        <!-- why netserv starts  -->
        <div id="rs-about" class="rs-about style1 gray-bg pb-70">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12 pt-70 pb-40" align="center">
                        <h3 class="sub-title">Why NetServ ?</h3>
                    </div>
                    <div class="col-lg-6 ">

                        <ul class="listing-style2 mb-33">
                            <li>Ability to rapidly provide a diverse team of experienced architects and engineers to handle the scope and deadline required for this initiative.</li>
                            <li>Deep Cloud infrastructure expertise.</li>
                        </ul>

                    </div>
                    <div class="col-lg-6 ">

                        <ul class="listing-style2 mb-33">
                            <li>Deep knowledge of advanced Networking architectures.</li>
                            <li>Experienced architects with campus networking design and deployment experience.</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <!-- why netserv ends  -->

        <!-- solution Section Start -->
        <!-- <div class="rs-counter style1 modify  pt-92 pb-100 md-pt-72 md-pb-80 text-black">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12 sec-title text-center mb-52 md-mb-29 pt-70">
                        <h3 class="title mb-0 "><span class="txt_clr">Solution:</span> SD-Campus with ISE</h3>
                    </div>
                    <div class="col-lg-6 md-mb-30">
                        <div class="sec-title text-center mb-52 md-mb-29">
                        </div>
                        <ul class="listing-style2 mb-33">
                            <li>We performed a detailed assessment with key Campus Network architects, business stakeholders, creating a clear current state of the customer’s network environment, focusing on the current challenges and the desired future state and gaps.</li>
                            <li>Next, our team designed the future-state SD-Campus architecture focused on meeting the defined business needs, recommending an initial release to POV of the proposed future state.</li>
                            <li>Designed and implemented DNAC Orchestrator.</li>
                            <li>Deployed SD-Campus/ISE fabric for wired and wireless networks.</li>
                            <li>Deployed automation workflows for ZTP, Software upgrades, Zero-trust security and user provisioning.</li>
                            <li>Implemented Segmentation, zero-trust network access.</li>
                            <li>Implemented Assurance capabilities.</li>
                        </ul>
                    </div>
                    <div class="col-lg-6 pl-50 lg-pl-35 md-pl-15 y-middle">
                        <img src="<?php echo main_url; ?>/assets/images/services/sucessstory/analysis2.jpg" alt="Customer Success Stories" title="Customer Success Stories">
                    </div>
                </div>
            </div>
        </div> -->
        <!-- solution Section End -->
        <!-- business outcome starts -->
        <div class="rs-questions style1 pt-50 pb-50 ">
            <div class="container">
                <div class="row md-col-padding">
                    <div class="col-lg-6 pr-30 md-mb-30 xs-mb-0">
                        <img class="fullWidth" src="<?php echo main_url; ?>/assets/images/services/sucessstory/outcome1.jpg" alt="Customer Success Stories" title="Customer Success Stories">
                    </div>
                    <div class="col-lg-6 pl-40">
                        <h3 class="title mb-21 md-mb-10 sm-mt-20">Our <span class="txt_clr">Business Outcomes</span></h3>
                        <ul class="listing-style2 mb-33">
                            <li>With consistent wired and wireless policies and workflow automation we were able to reduce the OPEX around 25%.</li>
                            <li>Enabled advanced Security design for campus with network segmentation and zero-trust network access.</li>
                            <li>Centralized management significantly improved the time for provisioning new users.</li>
                            <li>Deep campus network visibility helped resolve issues more quickly.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- bussiness outcome ends  -->

        <!-- sucess sd campus starts  -->
        <div class="rs-questions style1  pt-30 pb-30 " id="hybrid-cloud-managed-services">
            <div class="container">
                <div class="row md-col-padding">
                    <div class="col-md-12 sec-title text-center mb-52 md-mb-29 pt-20">
                        <h2 class="title mb-21 md-mb-10"><span class="txt_clr">Customer Success </span>Hybrid Cloud Managed Services</h2>
                    </div>
                    <div class="col-lg-6 pr-30 md-mb-30 xs-mb-0">
                        <div class="row gutter-20 hidden-xs">
                            <img src="<?php echo main_url; ?>/assets/images/services/sucessstory/deal3.jpg" alt="Customer Success Stories" title="Customer Success Stories">
                        </div>
                    </div>
                    <div class="col-lg-6 pl-40">
                        <h5 class="mb-2">Advanced Cloud Managed Services</h5>
                        <p class="desc ">NetServ has been our incredible partner, bringing extensive experience of hybrid cloud management expertise. With their 24x7 monitoring and operational support, Netserv really functions as an extension of our own IT team.</p>
                        <p class="desc">Modern, cloud-based applications and infrastructure are increasingly distributed, fragmented and complex to run your operations efficiently and securely. Enterprises are experiencing longer service outages, too many redundant tools, and reactive support model.</p>
                        <div class="desc ">Our innovative Managed AIOps Service allows you to improve your operational efficiencies by event correlation, faster root cause analysis and automating manual aspects of IT workflows.</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- sucess sd campus ends -->
        <!-- customer challenges starts  -->
        <div class="rs-whychooseus style8 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 sec-title text-center mb-52 md-mb-29 pt-70">
                        <h3 class="title mb-18">Customer Challenges</h3>
                    </div>
                    <div class="col-lg-5 md-mb-50">
                        <div class="desc">One of our customers is a global leader in developing, manufacturing, and marketing broad range of innovative products for the life science research and clinical diagnostic markets had following challenges :
                        </div>
                        <ul class="listing-style2 mb-33 pt-4">
                            <li>Customer had typical hybrid cloud challenges, including fragmented tools for monitoring, automation etc.</li>
                            <li>Network latency and overall performance issues.</li>
                            <li>Hybrid governance issue.</li>
                            <li>Lack of central point of management.</li>
                            <li>Lack of single point of visibility for hybrid cloud</li>
                            <li>Longer down time and operational ineffectiveness</li>
                        </ul>

                    </div>
                    <div class="col-lg-7 pl-70 md-pl-15">
                        <div class="images-part">
                            <img src="<?php echo main_url; ?>/assets/images/services/sucessstory/success3.jpg" alt="Customer Success Stories" title="Customer Success Stories">
                        </div>
                        <div class="rs-animations">
                            <div class="shape-icons one" style="top: -9%;">
                                <img class="dance2" src="assets/images/whychooseus/style8/shape/1.png" alt="images">
                            </div>
                            <div class="shape-icons two">
                                <img class="scale" src="assets/images/whychooseus/style8/shape/2.png" alt="images">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- customer challenges ends  -->

        <!--test2-->
        <div id="rs-about" class="rs-about style8 pt-100 pb-50 md-pt-70 md-pb-40">
            <div class="container">
                <div class="row ">
                    <div class="col-lg-6 md-mb-50">
                        <div class="images-part">
                            <img src="<?php echo main_url; ?>/assets/images/services/sucessstory/analysis3.jpg" alt="Customer Success Stories" title="Customer Success Stories">
                        </div>
                        <div class="widget-center">
                            <div class="content-part">
                                <h2 class="title">Why?</h2>
                                <p style="font-size: 25px;">Netserv</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 pl-50 md-pl-15">
                        <div class="sec-title3 mt-0">
                            <ul class="listing-style2 mb-33">
                                <li>Advanced Managed services solutions.</li>
                                <li>24x7 Proactive Monitoring and Operational support</li>
                                <li>Managed AIOps services.</li>
                                <li>Deep Cloud infrastructure expertise.</li>
                                <li>Deep knowledge of advanced Networking architectures.</li>
                                <li>Deep cloud infrastructure expertise.</li>
                                <li>Extensive experience with cloud managed services.</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!--test2-->

        <!-- solution Section Start -->
        <div class="rs-counter style1 modify  pt-10 pb-10 md-pt-72 md-pb-80 text-black">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12 sec-title text-center mb-10 md-mb-29 pt-10">
                        <h3 class="title mb-0 "><span class="txt_clr">Solution:</span> SD-Campus with ISE</h3>
                    </div>
                    <div class="col-lg-6 md-mb-30">
                        <div class="sec-title text-center mb-52 md-mb-29">
                        </div>
                        <ul class="listing-style2 mb-33">
                            <li>We performed a detailed assessment with key Campus Network architects, business stakeholders, creating a clear current state of the customer’s network environment, focusing on the current challenges and the desired future state and gaps.</li>
                            <li>Next, our team designed the future-state SD-Campus architecture focused on meeting the defined business needs, recommending an initial release to POV of the proposed future state.</li>
                            <li>Designed and implemented DNAC Orchestrator.</li>
                            <li>Deployed SD-Campus/ISE fabric for wired and wireless networks.</li>
                            <li>Deployed automation workflows for ZTP, Software upgrades, Zero-trust security and user provisioning.</li>
                            <li>Implemented Segmentation, zero-trust network access.</li>
                            <li>Implemented Assurance capabilities.</li>
                        </ul>
                    </div>
                    <div class="col-lg-6 pl-50 lg-pl-35 md-pl-15 y-middle">
                        <img src="<?php echo main_url; ?>/assets/images/services/sucessstory/analysis2.jpg" alt="Customer Success Stories" title="Customer Success Stories">
                    </div>
                </div>
            </div>
        </div>
        <!-- solution Section End -->

        <!--test3  Solution: SD-Campus with ISE start-->
        <!-- <div id="rs-services" class="rs-services gray-bg style1 modify2 pt-100 pb-84 md-pt-80 md-pb-64 aos-init aos-animate" data-aos="fade-up" data-aos-duration="2000">
            <div class="container">

                <div class="sec-title text-center">
                    <h3 class="title" style="font-weight:500;">
                        <strong> Solution: SD-Campus with ISE
                        </strong>
                    </h3>

                </div>
                <div class="row md-col-padding">
                    <div class="col-lg-6 pr-30 md-mb-30 xs-mb-0">
                        <ul class="listing-style pt-2">
                            <li>We performed a detailed assessment with key Campus Network architects, business stakeholders, creating a clear current state of the customer’s network environment, focusing on the current challenges and the desired future state and gaps.</li>
                            <li>Next, our team designed the future-state SD-Campus architecture focused on meeting the defined business
                            </li>
                            <li>needs, recommending an initial release to POV of the proposed future state.
                            </li>

                        </ul>
                    </div>
                    <div class="col-lg-6 pl-40">
                        <ul class="listing-style pt-2">
                            <li>Designed and implemented DNAC Orchestrator
                            </li>
                            <li>Deployed SD-Campus/ISE fabric for wired and wireless networks
                            </li>
                            <li>Deployed automation workflows for ZTP, Software upgrades, Zero-trust security and user provisioning

                            </li>
                            <li>Implemented Segmentation, zero-trust network access

                            </li>
                            <li>Implemented Assurance capabilities

                            </li>

                        </ul>
                    </div>
                </div>

            </div>
        </div> -->
        <!--#test3  Solution: SD-Campus with ISE end-->

        <!-- business outcome starts -->
        <div class="rs-questions style1 pt-100 pb-100 md-pt-173 md-pb-80">
            <div class="container">
                <div class="row md-col-padding">
                    <div class="col-lg-6 pr-30 md-mb-30 xs-mb-0">
                        <img class="fullWidth" src="<?php echo main_url; ?>/assets/images/services/sucessstory/outcome2.jpg" alt="Customer Success Stories" title="Customer Success Stories">
                    </div>
                    <div class="col-lg-6 pl-40">
                        <h3 class="title mb-21 md-mb-10 sm-mt-20">Our <span class="txt_clr">Business Outcomes</span></h3>
                        <ul class="listing-style2 mb-33">
                            <li>Managed Hybrid Cloud monitoring tools helped customer reduce OPEX by around 40%.</li>
                            <li>Central tool for management for visibility and proactive monitoring.</li>
                            <li>Advanced managed operational support.</li>
                            <li>Centralized governance framework for hybrid cloud management.</li>
                            <li>Centralized tool for automation.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- bussiness outcome ends  -->
        <!-- Customer Success SASE POC and Design start-->
        <!--  SASE POC and Design starts  -->

        <div id="nssase" class="rs-about style1 gray-bg pt-100 pb-100 md-pt-80 md-pb-80">
            <div class="container">
                <h2 class="title text-center  md-mb-10">Customer Success <span class="txt_clr">SASE POC</span> and<span class="txt_clr"> Design</span></h2>
                <div class="row y-middle">

                    <div class="col-lg-6 md-mb-50 mt-2">

                        <h4 class=""><span class="txt_clr "><b>SASE POC</span> and <span class="txt_clr">Design</b></span></h4>
                        <p class="desc mt-3">Today’s cloud-centric world drives the need for a secure access service edge (SASE) architecture that combines networking and security functions in the cloud. If you are looking to securely connect users to the apps and data needed — in any environment, from anywhere, then NetServ subject-matter experts can work with you with a vendor-agnostic SASE solution to meet your technical and business requirements.</p>
                        <p class="desc">One of our customers is a leader in delivering financial services and had the following technical requirements to address their existing challenges and support their future security roadmap.</p>


                    </div>

                    <div class="col-lg-6 pl-40 pr-60">
                        <div class="sec-title mt-5">
                            <img src="assets/images/whychooseus/style8/sase01.png" alt="Customer Success SASE " title="Customer Success SASE">
                        </div>
                    </div>
                    <!-- </div> -->



                    <div id="" class="rs-services  style1  modify2  pb-0 md-pt- md-pb-64 aos-init aos-animate how_can_we_help" data-aos="" data-aos-duration="">

                        <div class="container gray-bg ">

                            <div class="row ">

                                <div class="col-lg-6 pl-50 md-pl-15 pr-50 lg-pr-15">
                                    <ul class="listing-style2 mb-33 mt-4">
                                        <li>Providing safe and effective operations across diverse geographical locations.</li>
                                        <li class="">Backhauling internet traffic to central areas was costly and negatively impacted user-experience.</li>
                                        <li>Lacked some key security capabilities to identify and block threats and reduce the risks of shadow IT apps.</li>

                                    </ul>
                                </div>
                                <div class="col-lg-6 pl-50 md-pl-15 pr-50 lg-pr-15">
                                    <ul class="listing-style2 mb-33 mt-4">
                                        <li>Difficult to verify the identity of users and the health of devices​.</li><br>
                                        <li>Simplify consistent policy enforcement for all user access across all sites.</li><br>
                                        <li class="mt-2">Explore cost savings, network simplification, and operation via SDWAN.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--  SASE POC and Design end  -->
        </div>

        <!-- Customer Success SASE POC and Design end -->


        <!--solution starts-->
        <div id="rs-about" class="rs-about style8 pt-30 pb-20 md-pt-70 md-pb-40">
            <div class="container">

                <div class="row ">


                    <div class="col-lg-12 pl-50 md-pl-15 ">
                        <div class="sec-title3 mt-2">
                            <h4 class="mb-4 "> Solution:<span class="txt_clr"> SASE POC and Design </span> </h4>
                            <p>We worked with the Customer and vendor to design and deploy the Cisco SASE-based solution to seamlessly enable the Customer's diverse workforce to be productive from any location, ensuring secure access to shared company resources, including on-prem, public cloud, and SaaS applications. The solution components included Umbrella, Duo, SD-WAN, and identity services edge (ISE) to provide DNS security, secure web gateway, cloud delivered-firewall, cloud access security broker, threat grid, adaptive MFA, device posture, and health and behavioral analytics. They assisted the Customer in more robust integrations across their security stack to reduce response times.</p>

                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!--solution ends-->


        <!-- sase outcome starts  -->

        <div class="rs-whychooseus style8 pb-100 ">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 sec-title  mb- md-mb-29 pt-10">

                    </div>
                    <div class="col-lg-5 md-mb-50 mt-5">
                        <ul class="listing-style2 ">
                            <h4 class="mb-4 mt-2"> Outcomes:<span class="txt_clr"> SASE POC and Design<span> </h4>

                            <li>End-to-end observability with telemetry insights.</li>
                            <li>Integrated multi-cloud connectivity with consistent policies for cloud networks.</li>
                            <li>Zero trust application access for user/device.</li>
                            <li>Cost-saving by switching from MPLS to low-cost internet ISP connections.</li>

                        </ul>

                    </div>
                    <div class="col-lg-7 pl-70 md-pl-15 mt-5">
                        <div class="images-part">
                            <img src="<?php echo main_url; ?>/assets/images/services/sucessstory/sase02.png" alt="Customer Success SASE " title="Customer Success SASE">
                        </div>
                        <div class="rs-animations">
                            <div class="shape-icons one" style="top: -9%;">
                                <img class="dance2" src="assets/images/whychooseus/style8/shape/1.png" alt="images">
                            </div>
                            <div class="shape-icons two" style="bottom: -12%;">
                                <img class="scale" src="assets/images/whychooseus/style8/shape/2.png" alt="images">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  sase outcome  ends  -->
        <!--home-slider managed services for healthcare content start -->
        <div id="Transforming-Healthcare-Security" class="rs-about style8 pt-10 pb-0 md-pt-70 md-pb-40">
            <div class="container">

                <div class="row ">


                    <div class="col-lg-12 pl-50 md-pl-15 ">
                        <div class="sec-title3 mt-2">
                            <h3 class="">Transforming the <sapn class="txt_clr">Healthcare Industry </sapn>for Security

                            </h3>
                            <p class="">In the ever-evolving healthcare industry, our clients face a multitude of challenges. They grappled with poor functionality of their existing security tools, sky-high prices, and sluggish response times from their vendor. They had encountered a cyber attack but lacked a clear response plan, leading to concerns about data leaks. Outsourcing security services to an MSSP left them dissatisfied.</p>

                        </div>
                    </div>

                </div>
            </div>


            <div id="" class="rs-about style3 pb-30 md-pb-80">
                <div class="container">
                    <div class="row y-middle">
                        <div class="col-lg-6 pr-85 lg-pr-50 md-pr-15">
                            <div class="sec-title mb-50">

                                <p>This year, the organization aimed to cut costs, enhance compliance, and fortify its cybersecurity measures. The CISO desired robust security solutions, the CFO sought budget efficiency, and the CIO aimed to transition from Capex to Opex.

                                </p>
                                <p class="">Our proactive approach to addressing their pain points aligned perfectly with their goals. We offered a complimentary SOC gap assessment and organized on-site CISO workshops, demonstrating our commitment to their success.

                                </p>
                                <p>To help them secure budget allocation, we collaborated with the team, assisting in building compelling business cases for upper management and board members. By facilitating cost-effective financial models and optimizing their existing tools, we earned their trust as the "preferred vendor."
                                </p>
                                <p>Our security solutions transformed their operations, boosted compliance, and significantly reduced costs, making every executive look good in the eyes of management. They became our product advocates, endorsing our services through a successful case study.

                                </p>
                            </div>

                        </div>
                        <div class="col-lg-6 md-order-first md-mb-30 mb-130">

                            <div class="row gutter-20 ">
                                <div class="col-6">
                                    <img class="mb-20" src="/assets/images/sec-service7.webp" alt="Transforming Healthcare Security" title="Transforming Healthcare Security">
                                    <img src="/assets/images/services/planstatergy/lifscience1.webp" alt="Transforming Healthcare Security" title="Transforming Healthcare Security">

                                </div>
                                <div class="col-6 mt-30">
                                    <img class="mb-20" src="/assets/images/sec-service18.webp" alt="Transforming Healthcare Security" title="Transforming Healthcare Security">
                                    <img src="/assets/images/sec-service20.webp" alt="Transforming Healthcare Security" title="Transforming Healthcare Security">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--home-slider managed services for healthcare content end -->
        <!-- <div class="rs-about style3 pt-100 lg-pt-90 md-pt-80 pb-92 md-pb-72 sm-pb-80" id="Transforming-Healthcare-Security">
            <div class="container">
                <div class="row y-middle">
                    <div class="col-lg-6 md-mb-30">
                        <div class="image-part mt-4">
                            <img src="<?php echo main_url; ?>/assets/images/services/planstatergy/lifscience1.webp" style="border-radius: 27px;" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6 pl-50 lg-pl-35 md-pl-15">
                        <div class="sec-title">

                            <h4 class="mb-4">Transforming <sapn class="txt_clr"> Healthcare Security</sapn>
                            </h4>
                            <div class="scrol-div">
                                <div class="scrol">
                                    <p class="desc">In the ever-evolving healthcare industry, our clients face a multitude of challenges. They grappled with poor functionality of their existing security tools, sky-high prices, and sluggish response times from their vendor. They had encountered a cyber attack but lacked a clear response plan, leading to concerns about data leaks. Outsourcing security services to an MSSP left them dissatisfied.</p>
                                    <div class="desc">This year, the organization aimed to cut costs, enhance compliance, and fortify its cybersecurity measures. The CISO desired robust security solutions, the CFO sought budget efficiency, and the CIO aimed to transition from Capex to Opex.</div>
                                    <div class="desc">Our proactive approach to addressing their pain points aligned perfectly with their goals. We offered a complimentary SOC gap assessment and organized on-site CISO workshops, demonstrating our commitment to their success.</div>
                                    <div class="desc">To help them secure budget allocation, we collaborated with the team, assisting in building compelling business cases for upper management and board members. By facilitating cost-effective financial models and optimizing their existing tools, we earned their trust as the "preferred vendor."</div>
                                    <div class="desc">Our comprehensive security solutions transformed their operations, boosted compliance, and significantly reduced costs, making every executive look good in the eyes of management. They became our product advocates, endorsing our services through a successful case study. We achieved their goals, and they found a trusted partner in us.</div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!--  -->

        <div id="Revolutionizing-Life-Science-Security-and-Compliance" class="rs-about style1 pt-30 pb-50 md-pt-30 md-pb-30">
            <div class="container">
                <div class="row">
                    <h3 class="title  pl-2">Revolutionizing <span class="txt_clr">Life Science Security</span> and Compliance</h3>
                    <p class="pl-2 pb-3">In the life sciences, prioritizing security and compliance is crucial. Our recent achievement involves a leading life science company grappling with various challenges in its security and compliance activities.</p>
                    <div class="col-lg-6 pr-40 md-pl-pr-15 md-mb-21">
                        <div class="row gutter-20">
                            <div class="col-6">
                                <img src="assets/images/revolt5.webp" alt="Revolutionizing Life Science Security and Compliance" title="Revolutionizing Life Science Security and Compliance">
                            </div>
                            <div class="col-6">
                                <img class="mb-10 mt-1" src="assets/images/sec-service11.webp" alt="Revolutionizing Life Science Security and Compliance" title="Revolutionizing Life Science Security and Compliance">
                                <img src="assets/images/sec-service14.webp" class="mt-3" alt="Revolutionizing Life Science Security and Compliance" title="Revolutionizing Life Science Security and Compliance">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 pl-20 md-pl-pr-15">
                        <div class="sec-title mb-10 pr-85">

                            <h4 class="title mb-10">Customer <span class="txt_clr"> Challenges : </span></h4>

                        </div>
                        <ul class="listing-style">
                            <li>The CFO aimed to streamline expenses without compromising security and compliance.</li>
                            <li>The CISO prioritized meeting compliance requirements to avoid regulatory penalties.</li>
                            <li>A vital data leak prevention tool became a pivotal goal to prevent future breaches.</li>
                            <li>The CIO aimed to strengthen the organization's vulnerability management program.</li>
                            <li>Shifting from a Capex to an Opex model was essential to improve financial flexibility.</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <!--  -->
        <!-- 2nd -->
        <div id="rs-about12" class="rs-about style3 pb-80 md-pb-30">
            <div class="container">
                <div class="row y-middle">
                    <div class="col-lg-6 pr-85 lg-pr-50 md-pr-15">
                        <div class="sec-title mb-10">

                            <h4 class="title mb-10 mt-5">Solution: <span class="txt_clr">Security and Compliance</span></h4>
                            <p>In response to challenges, the organization established ambitious goals:</p>

                        </div>
                        <ul class="listing-style ">
                            <li>Streamlining expenses without compromising security and compliance.</li>
                            <li>Prioritizing compliance to avoid regulatory penalties.</li>
                            <li>Implementing a robust prevention tool to avoid future breaches.</li>
                            <li>Strengthening the organization's vulnerability management program.</li>
                            <li>Shifting from Capex to Opex for improved financial flexibility.</li>
                            <li>Demonstrating excellence in roles to position executives for potential promotions.</li>

                            <p class="pt-3">With our collaboration with the best solutions, the company overcame its challenges and achieved its goals for security compliance.</p>
                        </ul>

                    </div>
                    <div class="col-lg-6 md-order-first md-mb-30">
                        <div class="row gutter-20">
                            <div class="col-6">
                                <img src="assets/images/revolt4.webp" alt="Revolutionizing Life Science Security and Compliance" title="Revolutionizing Life Science Security and Compliance">
                            </div>
                            <div class="col-6 mt-30">
                                <img class="mb-20" src="assets/images/sec-service10.webp" alt="Revolutionizing Life Science Security and Compliance" title="Revolutionizing Life Science Security and Compliance">
                                <img src="assets/images/sec-service9.webp" alt="Revolutionizing Life Science Security and Compliance" title="Revolutionizing Life Science Security and Compliance">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- 2nd end -->
        <!-- revolution start-->

        <!-- revolution end -->
    </div>
    <!-- Main content End -->
    <!-- Footer Start -->
    <?php include 'footer.php'; ?>
    <!-- Footer End -->
    <!-- start scrollUp  -->
    <div id="scrollUp">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- End scrollUp  -->
    <?php include 'service_jslinks.php'; ?>
    <script>
        $(document).ready(function() {
            var elem = $('#' + window.location.hash.replace('#', ''));
            if (elem) {
                $('html, body').animate({
                    scrollTop: elem.offset().top - 190
                }, 2000);

            }
        });
    </script>
</body>

</html>