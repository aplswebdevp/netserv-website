<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Sr. Cloud Engineer</title>
    <meta name="description" content="NetServ is seeking a cloud services expert to drive our large scale cloud platform deployment.">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/senior-cloud-engineer"/>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'> 
        {
      "@context": "http://www.schema.org",
      "@type": "WebSite",
      "name": "NetSev",
      "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .bg4{background-image:url(assets/images/bg/bg4.png)}.rs-collaboration.style1 .img-part img{position:relative;bottom:0}.rs-services.style22 .service-wrap .icon-part img{width:53px;height:53px;max-width:unset}ul.listing-style li{position:relative;padding-left:30px;line-height:34px;font-weight:500;font-size:14px}ul.listing-style.regular2 li{font-weight:400;margin-bottom:0}.rs-about.style10 .accordion .card .card-body{background:#fff}
</style>
<body class="home-eight">
<!-- Preloader area start here -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--End preloader here -->
<!--Full width header Start-->
<div class="full-width-header header-style4">
    <!--header-->
    <?php include 'header.php'; ?>
    <!--Header End-->
</div>
<!--Full width header End-->
<!-- Main content Start -->
<div class="main-content">
    <!-- Services Section Start -->
    <div class="rs-pricing style1">
        <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
            <div class="container">
                <div class="sec-title">
                    <!-- <div class="sub-title white-color">Pricing Plan</div> -->
                    <h1 class="title white-color mb-0 text-center" style="font-size: 36px;"> Sr. Cloud Engineer</h1>
                    <div class="sub-title text-center white-color">United States (Remote) | 3 - 5 Years experience</div>
                </div>
            </div>
        </div>
    </div>
    <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-left">
                        
                         <p> <span class="txt_clr"><strong>Designation</strong> : </span> Sr. Cloud Engineer </p>
                        <p> <span class="txt_clr"><strong>Salary </strong> : </span>  Best in Industry </p>
                        <p> <span class="txt_clr"><strong>Experience </strong> : </span>  3 - 5 years </p>
                        <p> <span class="txt_clr"><strong>Joining  </strong> : </span>Immediate/15 days </p>
                        <p> <span class="txt_clr"><strong>Location  </strong> : </span> United States (Remote) </p>
                            <!-- <p> <span class="txt_clr"><strong>Roles and Responsibilities</strong></span>
                            </p> -->
                        <p> <span class="txt_clr"><strong>Job Description</strong></span></p>
                        <ol>
                        <li>NetServ is seeking a cloud services expert to drive our large scale cloud platform deployment. You will be engineering our next generation of infrastructure using major Public Cloud Providers (e.g. AWS) in a secure, compliant, and efficient manner.</li>
                        <li>The Senior Cloud Engineer we are looking for is ready to join our team working on moving rapidly into the Cloud. You will be using Infrastructure as Code and automation on all of your deployments, in a fast paced manner.</li>
                        <li>You will form part of a global team that will drive best practice in the PaaS/IaaS arena and work in a lean agile fashion. You will be expected to collaborate with, and support other product development teams and operational teams whose primary responsibilities will be to support services in production and pre-production environments.</li>
                        <li>Scope could include technical responsibilities, regional and functional influence, collaboration on global initiatives. Expectation of 24/7 365 day on-call rotation. Some travel may be required (infrequent).</li>
                     </ol>
                            <span class="txt_clr"><strong>What you’ll do as a Senior Cloud Engineer :</strong> </span>
                        <ol class="listing-style2 ">
                            <li> Work in a cross-functional team to design and engineer solutions to complicated problems</li>
                            <li>Mentor and coach other engineers</li>
                            <li>Migrate applications from an internal virtualized environment to Public Cloud</li>
                            <li>Utilize Infrastructure as Code Tools to automate deployments of new applications</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong>You could be a great fit if you have :</strong> </span>
                        <ol>
                            <li>3+ Years of experience with a Public Cloud Provider (e.g. AWS)</li>
                            <li>2+ Years of experience with Terraform</li>
                            <li>4+ Years of experience with Ansible, puppet, or chef</li>
                            <li>5+ Years of experience with Enterprise Server Operating Systems (Windows/Unix/Linux)</li>
                            <li>A deep understanding of distributed system design and dependency management</li>
                            <li>A strong grasp of monitoring tools, approach and implementation</li>
                            <li>Able to show examples of previous work exploiting containerized services</li>
                            <li>Solid working experience of continuous integration practices & tools (Jenkins, Travis CI etc…)</li>
                            <li>Working Knowledge of both relational and/or NoSQL databases such as MySQL and DynamoDB</li>
                            <li>Leadership experience</li>
                            <li>Experience with DevSecOps</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong>Personal Skills :</strong> </span>
                        <ol>
                            <li>Previous experience of contributing to war rooms and blameless postmortems</li>
                            <li> Superb communication skills, written and verbal
                                Experience of working in a true DevOps environment with daily collaborations
                            </li>
                            <li> Experience screening new candidates to join the team</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong>CI/CD DevOps Architect :</strong> </span>
                        <ol>
                            <li>Design, architect and implement next generation CICD Platform and automation solution</li>
                            <li>10+ Years of proven development and/or DevOps experience deploying and maintaining multi-tiered infrastructure and web applications</li>
                            <li>Strong hands-on experience in Linux/Unix/AWS environment and scripting languages: Shell,Python</li>
                            <li>Exposure in designing and implementing collaborated solutions on AWS and DevOps</li>
                            <li>Strong experience in automating Continuous Integration, Continuous Delivery and Agile practices for a highly scalable system</li>
                            <li>Perform analysis of the current practices and design and implement best practices and emerging concepts in CICD landscape</li>
                            <li>Experience with Docker, Microservices and container deployment and service orchestration</li>
                            <li>Experience with Blue-Green Deployment to reduce downtime and risk, provide continuous deployment and fast rollback</li>
                            <li>Experience with configuration management tools such as Puppet or Chef</li>
                            <li>Experience with Infrastructure automation (Terraform, Cloud formation)</li>
                        </ol>
                        </p>
                    </div>
                    <div class="btn-part">
                        <a href="mailto:hr@ngnetserv.com" class="btn btn-primary">Apply</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main content End -->
<!-- Footer Start -->
<?php include 'footer.php'; ?>
<!-- Footer End -->
<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->
<?php include 'service_jslinks.php'; ?>
</body>
</html>