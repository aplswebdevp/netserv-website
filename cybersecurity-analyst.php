<!DOCTYPE html>
<html lang="en">

<head>
       <!-- meta tag -->
       <meta charset="utf-8">
       <title>NetServ - Cybersecurity-Analyst</title>
       <meta name="description" content="As the world around us becomes more connected and more digital, there are increased opportunities for fraud and disruption due to cybersecurity attacks.">
       <!-- responsive tag -->
       <meta http-equiv="x-ua-compatible" content="ie=edge">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <!-- favicon -->
       <link rel="apple-touch-icon" href="">
       <link rel="canonical" href="https://www.ngnetserv.com/cybersecurity-analyst"/>
       <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
       <?php include 'service_csslinks.php'; ?>
       <script type='application/ld+json'> 
        {
      "@context": "http://www.schema.org",
      "@type": "WebSite",
      "name": "NetSev",
      "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
       .bg4 {
              background-image: url(assets/images/bg/bg4.png);
       }

       .rs-collaboration.style1 .img-part img {
              position: relative;
              bottom: 0px;
       }

       .rs-services.style22 .service-wrap .icon-part img {
              width: 53px;
              height: 53px;
              max-width: unset;
       }

       ul.listing-style li {
              position: relative;
              padding-left: 30px;
              line-height: 34px;
              font-weight: 500;
              font-size: 14px;
       }

       ul.listing-style.regular2 li {
              font-weight: 400;
              margin-bottom: 0px;
       }

       .rs-about.style10 .accordion .card .card-body {
              background: #ffffff;
       }
</style>

<body class="home-eight">
       <!-- Preloader area start here -->
      <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
       <!--End preloader here -->
       <!--Full width header Start-->
       <div class="full-width-header header-style4">
              <!--header-->
              <?php include 'header.php'; ?>
              <!--Header End-->
       </div>
       <!--Full width header End-->
       <!-- Main content Start -->
       <div class="main-content">
              <!-- Services Section Start -->
              <div class="rs-pricing style1">
                     <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
                            <div class="container">
                                   <div class="sec-title">
                                          <!-- <div class="sub-title white-color">Pricing Plan</div> -->
                                          <h1 class="title white-color mb-0 text-center"> Cybersecurity Analyst </h1>
                                          <div class="sub-title text-center white-color"> United States (Remote) | 5 - 8 years experience</div>
                                   </div>
                            </div>
                     </div>
              </div>

              <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
                     <div class="container">
                            <div class="row">
                                   <div class="col-lg-12">

                                          <div class="text-left">
                                                  <p> <span class="txt_clr"><strong>Designation</strong> : </span> Cybersecurity Analyst </p>
                        <p> <span class="txt_clr"><strong>Salary </strong> : </span>  Best in Industry </p>
                        <p> <span class="txt_clr"><strong>Experience </strong> : </span>  5 - 8 years </p>
                        <p> <span class="txt_clr"><strong>Joining  </strong> : </span>Immediate/15 days </p>
                        <p> <span class="txt_clr"><strong>Location  </strong> : </span> United States (Remote)  </p>
                           
                                                 <p> <span class="txt_clr"><strong>Job Description</strong></span>
                                                 </p>
                                                 <p>
                                                 As the world around us becomes more connected and more digital, there are increased opportunities for fraud and disruption due to cybersecurity attacks. The need for companies, products, and services to be secure is more important than ever in this constantly changing landscape.
                                                 Are you passionate about keeping good people safe from bad actors? We are too!
                                                 </p>
                                                 <!-- <p>
                                                        As part of Cyber Security Center of Excellence team, your responsibilities includes providing technical guidance, technology evaluations, Proof of Concepts development, Cloud distributed application architecture, design, design review, coding practices, writing code, code review, continuous integration, continuous deployment, automated testing, scaling the products and solutions for SaaS products and solutions.
                                                 </p> -->
                                                 <!-- <p><span class="txt_clr"><strong>Primary Skills :</strong> </span>
                                                 <ol>
                                                        <li>Deep understanding and experience of architecting and developing full stack end to end scalable and distributed Cloud application serviced out of Amazon AWS</li>
                                                        <li>Solid SaaS Application architecture and Cloud Deployment architecture principles with deep rooted experience on Amazon AWS</li>
                                                        <li>Expertise in loosely coupled design, Micro-services development, Message queues and containerized applications deployment using technologies like RESTful services, Message Queues, and Docker</li>
                                                        <li>Strong computer science fundamentals, and algorithms</li>
                                                        <li>Hands on deep expertise on Python and Python Web, Django</li>
                                                        <li>Experience working with SQL Databases like MySQL and PostgreSQL</li>
                                                        <li>Experience working with NoSQL Databases like MongoDB, Cassandra</li>
                                                        <li>Good understanding of HTML5, CSS3, JavaScript, OOJS.</li>
                                                        <li>Good familiarity with Linux operating system</li>
                                                        <li>Understanding and awareness of Secure software development lifecycle and web application vulnerabilities counter measures, e.g. OWASP Top 10 Security Risks</li>
                                                        <li>Understanding of CapEx and Opex estimates for applications.</li>
                                                        <li>Good understanding of licensing and subscription</li>
                                                        <li>Should be strong in financial planning for application hosting and server configurations.</li>
                                                        <li>Should Have strong knowledge of AWS Billing cycle, TCO. Cost Calculator, Pricing principles and AWS purchasing options (on-demand, reserve and spot bidding).</li>
                                                        <li>Experience with CI/CD pipeline with detailed understanding of Git, Jenkins, TeamCity, Artifactory, Cloudformation & Terraform.</li>
                                                        <li>Test Driven Development (TDD) mindset and orientation of 100% test automation</li>
                                                 </ol>
                                                 </p> -->
                                                 <p><span class="txt_clr"><strong>Experience :</strong> </span>
                                                 <ol>
                                                        <li>Experience across threat hunting/ compromise assessments</li>
                                                        <li>Parsing the logs in SIEM (Sentinel) solution and keep functional by ensuring that all relevant log sources are actively parsed</li>
                                                        <li>Security Event Correlation as received from Level 2 Security Operations or Incident Response staff or relevant sources to determine the increased risk</li>
                                                        <li>Support the annual penetration testing by ensuring that our pen test consultants can log into our environment and maneuver through it</li>
                                                        <li>5 to 8 years’ experience in Cyber Forensic, Cyber Defense and Threat Intelligence</li>
                                                        
                                                 </ol>
                                                 </p>
                                                 <!-- <p><span class="txt_clr"><strong>Secondary Skills :</strong> </span>
                                                 <ol>
                                                        <li>Knowledge of JavaScript and frontend frameworks React and Angular</li>
                                                        <li>Knowledge of Java and Java Web is an advantage</li>
                                                 </ol>
                                                 </p> -->
                                                 <p><span class="txt_clr"><strong>Responsibilities :</strong> </span>
                                                 <ol>
                                                        <li>Analyze security events from endpoints (Windows, Mac, Linux), Network IDS, Web-proxies,
                                                 Mail-gateways, Active Directory infrastructure.
                                                  </li>
                                                        <li>Perform event correlation analysis on potential threats identified through a SIEM tool.</li>
                                                        <li>Investigate anomalies observed within the network and remediate network and systems outages.</li>
                                                        <li>Detect and investigate information security incidents.</li>
                                                        <li>Propose Incident response actions and remediation plan.</li>
                                                        <li>Identification of potential vectors of attacks, develop detection methods of these attacks by
                                                               existing technological solutions
                                                        </li>
                                                        <li>Adjust detection logic to fit Customer needs (filter out false positives, customize correlation
                                                               rules.
                                                        </li>
                                                        <li>Communicate with Customers regarding detected incidents and suspicious activities.
                                                        </li>
                                                 </ol>
                                                 </p>
                                                 <!-- <p><span class="txt_clr"><strong> Educational/professional qualification required :</strong> </span>
                                                 <ol>
                                                        <li>Bachelor in Computer Science or equivalent. Preferably a Master degree in computer science.</li>

                                                 </ol>
                                                 </p> -->
                                                 <p><span class="txt_clr"><strong> Skills and Qualification:</strong> </span>
                                                 <ol>
                                                        <li>SIEM - Splunk /QRadar/Sentinel Certification.</li>
                                                        <li>Must have a technical working knowledge SIEM, EDR, antimalware, penetration testing,
                                                 vulnerability scans, ACLs, and IDS/IPS Concepts.
                                                 </li>
                                                 <li>CEH/ OSCP/ CISSP/CISM and other relevant Certifications.
                                                 </li>
                                                 </ol>
                                                 </p>
                                                 <!-- <p><span class="txt_clr"><strong> Certification :</strong> </span>
                                                 <ol>
                                                        <li>Candidates with AWS Solution Architect (Associate/Professional) would be prefered.</li>

                                                 </ol>
                                                 </p> -->
                                                 <!-- <p><span class="txt_clr"><strong>Soft skills/competencies :</strong> </span>
                                                 <ol>
                                                        <li>Problem solving mind and attitude</li>
                                                        <li>Effective communication skills – written, spoken, listening and presentation</li>
                                                        <li>Great Team player and experience working with global teams and global organizations</li>
                                                        <li>Genuine interest in learning and knowledge sharing
                                                               Must have written technical paper, blogs, speaker in technical forums, or patents</li>
                                                 </ol>
                                                 </p> -->
                                          </div>
                                          <div class="btn-part">

                                                 <a class="btn btn-primary" href="mailto:hr@ngnetserv.com">Apply</a>
                                          </div>
                                   </div>

                            </div>
                     </div>
              </div>

       </div>
       <!-- Main content End -->
       <!-- Footer Start -->
       <?php include 'footer.php'; ?>
       <!-- Footer End -->
       <!-- start scrollUp  -->
       <div id="scrollUp">
              <i class="fa fa-angle-up"></i>
       </div>
       <!-- End scrollUp  -->
       <?php include 'service_jslinks.php'; ?>
</body>

</html>