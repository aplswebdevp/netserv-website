<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Cloud Architect - Application + Billings</title>
    <meta name="description" content="As a Cloud Solutions Architect, we look towards you as an expert with deep understanding of scalable and distributed SaaS application architecture, development and deployment in Agile DevOps environment.">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/cloud-architect-application-billings"/>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'> 
        {
      "@context": "http://www.schema.org",
      "@type": "WebSite",
      "name": "NetSev",
      "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .bg4{background-image:url(assets/images/bg/bg4.png)}.rs-collaboration.style1 .img-part img{position:relative;bottom:0}.rs-services.style22 .service-wrap .icon-part img{width:53px;height:53px;max-width:unset}ul.listing-style li{position:relative;padding-left:30px;line-height:34px;font-weight:500;font-size:14px}ul.listing-style.regular2 li{font-weight:400;margin-bottom:0}.rs-about.style10 .accordion .card .card-body{background:#fff}
</style>
<body class="home-eight">
<!-- Preloader area start here -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--End preloader here -->
<!--Full width header Start-->
<div class="full-width-header header-style4">
    <!--header-->
    <?php include 'header.php'; ?>
    <!--Header End-->
</div>
<!--Full width header End-->
<!-- Main content Start -->
<div class="main-content">
    <!-- Services Section Start -->
    <div class="rs-pricing style1">
        <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
            <div class="container">
                <div class="sec-title">
                    <!-- <div class="sub-title white-color">Pricing Plan</div> -->
                    <h1 class="title white-color mb-0 text-center" style="font-size: 36px;"> Cloud Architect - Application + Billings </h1>
                    <div class="sub-title text-center white-color"> United States (Remote) | 6 - 8 Years experience</div>
                </div>
            </div>
        </div>
    </div>
    <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-left">
                         <p> <span class="txt_clr"><strong>Designation</strong> : </span> Cloud Architect - Application + Billings </p>
                        <p> <span class="txt_clr"><strong>Salary </strong> : </span>  Best in Industry </p>
                        <p> <span class="txt_clr"><strong>Experience </strong> : </span>   6 - 8 Years </p>
                        <p> <span class="txt_clr"><strong>Joining  </strong> : </span>Immediate/15 days </p>
                        <p> <span class="txt_clr"><strong>Location  </strong> : </span> United States (Remote)  </p>
                           
                        <p> <span class="txt_clr"><strong>Job Description</strong></span>
                        </p>
                        <p>
                            As a Cloud Solutions Architect, we look towards you as an expert with deep understanding of scalable and distributed SaaS application architecture, development and deployment in Agile DevOps environment. Your role would be to provide architectural and design leadership with deep-rooted programming experience for the next generation of products and solutions for in the domain of cybersecurity providing foundational security controls. As part of the planning team, you would also be responsible for cost estimating and budgeting. Existing system analysis would be a key role where would work with program managers to do application assessment and their feasibility with cloud services.
                        </p>
                        <p>
                            As part of Cyber Security Center of Excellence team, your responsibilities includes providing technical guidance, technology evaluations, Proof of Concepts development, Cloud distributed application architecture, design, design review, coding practices, writing code, code review, continuous integration, continuous deployment, automated testing, scaling the products and solutions for SaaS products and solutions.
                        </p>
                        <p><span class="txt_clr"><strong>Primary Skills :</strong> </span>
                        <ol>
                            <li>Deep understanding and experience of architecting and developing full stack end to end scalable and distributed Cloud application serviced out of Amazon AWS</li>
                            <li>Solid SaaS Application architecture and Cloud Deployment architecture principles with deep rooted experience on Amazon AWS</li>
                            <li>Expertise in loosely coupled design, Micro-services development, Message queues and containerized applications deployment using technologies like RESTful services, Message Queues, and Docker</li>
                            <li>Strong computer science fundamentals, and algorithms</li>
                            <li>Hands on deep expertise on Python and Python Web, Django</li>
                            <li>Experience working with SQL Databases like MySQL and PostgreSQL</li>
                            <li>Experience working with NoSQL Databases like MongoDB, Cassandra</li>
                            <li>Good understanding of HTML5, CSS3, JavaScript, OOJS.</li>
                            <li>Good familiarity with Linux operating system</li>
                            <li>Understanding and awareness of Secure software development lifecycle and web application vulnerabilities counter measures, e.g. OWASP Top 10 Security Risks</li>
                            <li>Understanding of CapEx and Opex estimates for applications.</li>
                            <li>Good understanding of licensing and subscription</li>
                            <li>Should be strong in financial planning for application hosting and server configurations.</li>
                            <li>Should Have strong knowledge of AWS Billing cycle, TCO. Cost Calculator, Pricing principles and AWS purchasing options (on-demand, reserve and spot bidding).</li>
                            <li>Experience with CI/CD pipeline with detailed understanding of Git, Jenkins, TeamCity, Artifactory, Cloudformation & Terraform.</li>
                            <li>Test Driven Development (TDD) mindset and orientation of 100% test automation</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong>Secondary Skills :</strong> </span>
                        <ol>
                            <li>Knowledge of JavaScript and frontend frameworks React and Angular</li>
                            <li>Knowledge of Java and Java Web is an advantage</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong> Educational/professional qualification required :</strong> </span>
                        <ol>
                            <li>Bachelor in Computer Science or equivalent. Preferably a Master degree in computer science.</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong> Certification :</strong> </span>
                        <ol>
                            <li>Candidates with AWS Solution Architect (Associate/Professional) would be prefered.</li>
                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong>Soft skills/competencies :</strong> </span>
                        <ol>
                            <li>Problem solving mind and attitude</li>
                            <li>Effective communication skills – written, spoken, listening and presentation</li>
                            <li>Great Team player and experience working with global teams and global organizations</li>
                            <li>Genuine interest in learning and knowledge sharing
                                Must have written technical paper, blogs, speaker in technical forums, or patents
                            </li>
                        </ol>
                        </p>
                    </div>
                    <div class="btn-part">
                        <a href="mailto:hr@ngnetserv.com" class="btn btn-primary" >Apply</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main content End -->
<!-- Footer Start -->
<?php include 'footer.php'; ?>
<!-- Footer End -->
<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->
<?php include 'service_jslinks.php'; ?>
</body>
</html>