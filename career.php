<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>NetServ - Careers</title>
    <meta name="description" content="At Netserv we love to work as a team. Our focus is to procure client growth along with growth of our team. We are constantly looking for good talent to join us helo our clients. If you are one of them who want to collaborate and grow we have some opportunities for you.">
    <meta name="keywords" content="it software developer, career, career opportunities, stack developer, web developer jobs, developer jobs in united state, developer jobs, software developer jobs work from home">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/career" />
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png"> <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'>
        {
            "@context": "http://www.schema.org",
            "@type": "WebSite",
            "name": "NetSev",
            "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .bg4 {
        background-image: url(assets/images/bg/bg4.png)
    }

    .rs-collaboration.style1 .img-part img {
        position: relative;
        bottom: 0;
    }

    .rs-services.style22 .service-wrap .icon-part img {
        width: 53px;
        height: 53px;
        max-width: unset
    }

    ul.listing-style li {
        position: relative;
        padding-left: 30px;
        line-height: 34px;
        font-weight: 500;
        font-size: 14px
    }

    ul.listing-style.regular2 li {
        font-weight: 400;
        margin-bottom: 0
    }

    .rs-about.style10 .accordion .card .card-body {
        background: #fff
    }

    .accordian-space {
        margin-bottom: 3px !important;
    }

    .service-wrap {
        padding: 15px 15px 30px;
        background: #ffffff;
        box-shadow: 0px 14px 30px rgb(0 0 0 / 7%);
    }

    .active {
        display: block !important;
    }

    .page_no:hover {
        cursor: pointer
    }

    .active_1 {
        font-weight: 900;
        color: #106eea !important;
    }

    .pagenav-link ul li {
        color: #727983;
    }
</style>

<body class="home-eight">
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <div class="full-width-header header-style4"> <?php include 'header.php'; ?> </div>
    <div class="main-content career_cover_section">
        <div class="rs-collaboration style1 bg4 mb-100">
            <div class="wrap-1400">
                <div class="row y-middle">
                    <div class="col-lg-6 col-md-12 sm-mb-40">
                        <div class="img-part"><img src="assets/images/career.png" alt="Careers"></div>
                    </div>
                    <div class="col-lg-6 col-md-12 pl-50 pt-100 pb-100">
                        <div class="video-btn text-center mb-50"></div>
                        <div class="sec-title text-center mb-40 pr-20 pl-20">
                            <h1 class="title mb-25">Careers</h1>
                            <div class="desc">At Netserv we love to work as a team. Our focus is to procure client growth
                                along with growth of our team. We are constantly looking for good talent to join us helo our
                                clients. If you are one of them who want to collaborate and grow we have some opportunities
                                for you.
                            </div>
                        </div>
                        <div class="dual-btn">
                            <div class="dual-btn-wrap"><a class="btn-right" href="<?php echo main_url; ?>/contact-us"><span>Contact Us</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="rs-about style10 gray-bg5 pt-130 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 pr-70 md-pr-15 md-mb-50">
                        <div class="sec-title4 mb-30">
                            <h2 class="title pb-20">Current Openings</h2>
                        </div>
                        <div id="accordion" class="accordion">
<!-- first card for new jobs -->
                                <div class="card accordian-space">
                                <div class="card-header"><a class="collapsed card-link" data-toggle="collapse" href="#collapsedemo-5">Desktop Support Engineer </a></div>
                                <div id="collapsedemo-5" class="collapse show" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    Onsite Hercules, California (USA) (Onsite)</p>
                                                <p class="text-muted"><i class="fa fa-paypal" aria-hidden="true"></i> Best
                                                    in Industry</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="text-muted mb-0"><i class="fa fa-clock-o" aria-hidden="true"></i> Full
                                                    time</p>
                                                <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i>
                                                    Immediate Joiner / 15 Days </p>
                                            </div>
                                        </div>
                                        <p class="text-muted mb-0 mt-2">NetServ is looking for Desktop Support Engineer who can independently troubleshoot and providing solutions to unresolved hardware and software problems through trouble-ticket system <a href="<?php echo main_url; ?>/desktop-support-engineer">Read More..</a></p>
                                    </div>
                                </div>
                                    </div><!-- </div><div id="accordion" class="accordion"> -->
                                    <!-- first card end -->

                                <!-- first card for new jobs -->
                                <div class="card accordian-space">
                                <div class="card-header"><a class="collapsed card-link" data-toggle="collapse" href="#collapsedemo-1">Network Engineer </a></div>
                                <div id="collapsedemo-1" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    United States (Remote)</p>
                                                <p class="text-muted"><i class="fa fa-paypal" aria-hidden="true"></i> Best
                                                    in Industry</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="text-muted mb-0"><i class="fa fa-clock-o" aria-hidden="true"></i> Full
                                                    time</p>
                                                <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i>
                                                    Immediate Joiner / 15 Days </p>
                                            </div>
                                        </div>
                                        <p class="text-muted mb-0 mt-2">NetServ is looking  for Network Engineer with hands-on experience on Cisco routers, switches and firewall vendors like Pa <a href="<?php echo main_url; ?>/network-engineer">Read More..</a></p>
                                    </div>
                                </div>
                                    </div><!-- </div><div id="accordion" class="accordion"> -->
                                    <!-- first card end -->

                            <!-- first card for new jobs -->
                            <div class="card accordian-space">
                                <div class="card-header"><a class="collapsed card-link" data-toggle="collapse" href="#collapsedemo-2">Senior Network Architect </a></div>
                                <div id="collapsedemo-2" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    United States (Onsite/Hybrid)</p>
                                                <p class="text-muted mb-0"><i class="fa fa-user" aria-hidden="true"></i> 4+
                                                    years experience </p>
                                                <p class="text-muted"><i class="fa fa-paypal" aria-hidden="true"></i> Best
                                                    in Industry</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="text-muted mb-0"><i class="fa fa-clock-o" aria-hidden="true"></i> Full
                                                    time</p>
                                                <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i>
                                                    Immediate Joiner / 15 Days </p>
                                            </div>
                                        </div>
                                        <p class="text-muted mb-0 mt-2">NetServ Inc is looking for a Network Architect who can configure, manage, and support network devices across the Campus and DC environments. <a href="<?php echo main_url; ?>/senior-networkarchitect">Read More..</a></p>
                                    </div>
                                </div>
                            </div><!-- </div><div id="accordion" class="accordion"> -->
                            <!-- first card end -->
                            <div class="card accordian-space">
                                <div class="card-header"><a class="collapsed card-link" data-toggle="collapse" href="#collapsedemo-3">Finance SME </a></div>
                                <div id="collapsedemo-3" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    Budapest</p>
                                                <p class="text-muted mb-0"><i class="fa fa-user" aria-hidden="true"></i> 5+
                                                    years experience </p>
                                                <p class="text-muted"><i class="fa fa-paypal" aria-hidden="true"></i> Best
                                                    in Industry</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="text-muted mb-0"><i class="fa fa-clock-o" aria-hidden="true"></i> Full
                                                    time</p>
                                                <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i>
                                                    Immediate Joiner / 15 Days </p>
                                            </div>
                                        </div>
                                        <p class="text-muted mb-0 mt-2">Ensures business processes, administration,
                                            and
                                            financial management. Maintains accounting system. <a href="<?php echo main_url; ?>/finance-sme">Read More..</a></p>
                                    </div>
                                </div>
                            </div><!-- </div><div id="accordion" class="accordion"> -->
                            <div class="card accordian-space">
                                <div class="card-header"><a class="collapsed card-link" data-toggle="collapse" href="#collapsedemo-4">UX Engineer</a></div>
                                <div id="collapsedemo-4" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    Budapest</p>
                                                <p class="text-muted mb-0"><i class="fa fa-user" aria-hidden="true"></i> 8+
                                                    years experience </p>
                                                <p class="text-muted"><i class="fa fa-paypal" aria-hidden="true"></i> Best
                                                    in Industry</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="text-muted mb-0"><i class="fa fa-clock-o" aria-hidden="true"></i> Full
                                                    time</p>
                                                <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i>
                                                    Immediate Joiner / 15 Days </p>
                                            </div>
                                        </div>
                                        <p class="text-muted mb-0 mt-3">Translate concepts into user flows,
                                            wireframes,
                                            mock-ups, and prototypes that lead to intuitive user experiences. <a href="<?php echo main_url; ?>/ux-engineer">Read More..</a></p>
                                    </div>
                                </div>
                            </div><!-- </div><div id="accordion" class="accordion"> -->
                            <div class="card accordian-space">
                                <div class="card-header"><a class="collapsed card-link" data-toggle="collapse" href="#security_arch">Security Architect</a></div>
                                <div id="security_arch" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    United States (Remote) </p>
                                                <p class="text-muted mb-0"><i class="fa fa-user" aria-hidden="true"></i> 8+
                                                    years experience</p>
                                                <p class="text-muted"><i class="fa fa-paypal" aria-hidden="true"></i> Best
                                                    in Industry</p>
                                            </div>
                                            <div class="col-md-6">
                                                <p class="text-muted mb-0"><i class="fa fa-clock-o" aria-hidden="true"></i> Full
                                                    time</p>
                                                <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i>
                                                    Immediate/15 days </p>
                                            </div>
                                        </div>
                                        <p class="text-muted mb-0 mt-3">We are seeking a Cloud and Network Security
                                            Architect to define the Cloud Security Strategy, Risk and Compliance. <a href="<?php echo main_url; ?>/security-architect">Read
                                                More..</a></p>
                                    </div>
                                </div><!-- </div><div id="" class="accordion"> -->
                                <div class="card accordian-space">
                                    <div class="card-header"><a class="collapsed card-link" data-toggle="collapse" href="#project_manager">Project Manager</a></div>
                                    <div id="project_manager" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        United States (Remote)</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-user" aria-hidden="true"></i>
                                                        5+ years experience</p>
                                                    <p class="text-muted"><i class="fa fa-paypal" aria-hidden="true"></i>
                                                        Best in Industry</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        Full time</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i>
                                                        Immediate/15 days
                                                    </p>
                                                </div>
                                            </div>
                                            <p class="text-muted mb-0 mt-3">Project Managers are responsible for
                                                making
                                                amazing things take place with our customers. Our teams make world
                                                class
                                                products, and our project managers guide those new technology into
                                                our
                                                customer’s production environments. <a href="<?php echo main_url; ?>/project-manager">Read
                                                    More..</a></p>
                                        </div>
                                    </div>
                                </div><!-- </div><div id="" class="accordion"> -->
                                <div class="card accordian-space">
                                    <div class="card-header"><a class=" collapsed card-link" data-toggle="collapse" href="#collapsefirstL2network">L2 Network Operation
                                            Engineer </a></div>
                                    <div id="collapsefirstL2network" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        United States (Remote)</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-user" aria-hidden="true"></i>
                                                        5+ years experience</p>
                                                    <p class="text-muted"><i class="fa fa-paypal" aria-hidden="true"></i>
                                                        Best in Industry</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        Full time</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i> Immediate
                                                        Joiner /
                                                        15 Days </p>
                                                </div>
                                            </div>
                                            <p class="text-muted mb-0 mt-3">NetServ offers a full suite of
                                                solutions, which
                                                can be leveraged to empower your IT infrastructure goals and
                                                objectives. <a href="<?php echo main_url; ?>/L2-network-operation-engineer">Read
                                                    More..</a></p>
                                        </div>
                                    </div>
                                </div><!-- </div><div id="accordion" class="accordion"> -->
                                <div class="card accordian-space">
                                    <div class="card-header"><a class="collapsed card-link" data-toggle="collapse" href="#collapsedemo">Network Architect </a></div>
                                    <div id="collapsedemo" class="collapse " data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        United States (Remote)</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-user" aria-hidden="true"></i>
                                                        8 - 10 years experience </p>
                                                    <p class="text-muted"><i class="fa fa-paypal" aria-hidden="true"></i>
                                                        Best in Industry</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        Full time</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i> Immediate
                                                        Joiner /
                                                        15 Days </p>
                                                </div>
                                            </div>
                                            <p class="text-muted mb-0 mt-3">Design, test, and inspect data
                                                communications
                                                systems. Ability to create design (HLD, LLD) for Target state
                                                network
                                                architecture. <a href="<?php echo main_url; ?>/network-architect">Read
                                                    More..</a></p>
                                        </div>
                                    </div>
                                </div><!-- </div><div id="accordion" class="accordion"> -->
                                <div class="card accordian-space">
                                    <div class="card-header"><a class=" collapsed card-link" data-toggle="collapse" href="#collapsefirst">Cloud Architect </a></div>
                                    <div id="collapsefirst" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        United States (Remote)</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-user" aria-hidden="true"></i>
                                                        10 years experience </p>
                                                    <p class="text-muted"><i class="fa fa-paypal" aria-hidden="true"></i>
                                                        Best in Industry</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        Full time</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i> Immediate
                                                        Joiner /
                                                        15 Days </p>
                                                </div>
                                            </div>
                                            <p class="text-muted mb-0 mt-3"> As a Cloud Solution Architect you will
                                                be
                                                responsible for the implementation of our IT Infrastructure Cloud
                                                Solutions
                                                and Services globally. <a href="<?php echo main_url; ?>/cloud-architect">Read
                                                    More..</a></p>
                                        </div>
                                    </div>
                                </div><!-- </div><div id="accordion" class="accordion"> -->
                                <div class="card accordian-space">
                                    <div class="card-header"><a class="collapsed card-link" data-toggle="collapse" href="#collapseZero">Full Stack Developer</a></div>
                                    <div id="collapseZero" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        United States (Remote)</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-user" aria-hidden="true"></i>
                                                        3+ years experience </p>
                                                    <p class="text-muted"><i class="fa fa-paypal" aria-hidden="true"></i>
                                                        Best in Industry</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        Full time</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i> Immediate
                                                        Joiner /
                                                        15 Days </p>
                                                </div>
                                            </div>
                                            <p class="text-muted mb-0 mt-2"> We are looking for a highly skilled
                                                computer
                                                programmer who is comfortable with both front and back-end
                                                programming. Full
                                                stack developers are responsible for developing and designing front
                                                end web
                                                architecture, ensuring the responsiveness of applications, and
                                                working
                                                alongside graphic designers for web design features, among other
                                                duties.
                                                Full stack developers will be required to see out a project from
                                                conception
                                                to final product, requiring good organizational skills and attention
                                                to
                                                detail.<a href="<?php echo main_url; ?>/full-stack-developer">Read
                                                    More..</a></p>
                                        </div>
                                    </div>
                                </div><!-- </div><div id="accordion" class="accordion"> -->
                                <div class="card">
                                    <div class="card-header"><a class="collapsed card-link" data-toggle="collapse" href="#collapseOne">Cybersecurity Analyst</a></div>
                                    <div id="collapseOne" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        United States (Remote)</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-user" aria-hidden="true"></i>
                                                        5 - 8 years experience </p>
                                                    <p class="text-muted m-0"><i class="fa fa-paypal" aria-hidden="true"></i> Best in
                                                        Industry
                                                    </p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        Full time</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i> Immediate
                                                        Joiner /
                                                        15 Days </p>
                                                </div>
                                            </div>
                                            <p class="text-muted mb-0 mt-3">As the world around us becomes more
                                                connected
                                                and more digital, there are increased opportunities for fraud and
                                                disruption
                                                due to cybersecurity attacks. The need for companies, products, and
                                                services
                                                to be secure is more important than ever in this constantly changing
                                                landscape. Are you passionate about keeping good people safe from
                                                bad
                                                actors? We are too! <a href="<?php echo main_url; ?>/cybersecurity-analyst">Read
                                                    More..</a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card accordian-space">
                                    <div class="card-header"><a class="collapsed card-link" data-toggle="collapse" href="#collapseten">Identity and Access Management
                                            (IAM)/AD
                                            Expert </a></div>
                                    <div id="collapseten" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        United States (Remote)</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-user" aria-hidden="true"></i>
                                                        years experience </p>
                                                    <p class="text-muted m-0"><i class="fa fa-paypal" aria-hidden="true"></i> Best in
                                                        Industry
                                                    </p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        Full time</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i> Immediate
                                                        Joiner /
                                                        15 Days </p>
                                                </div>
                                            </div>
                                            <p class="text-muted mb-0 mt-3">
                                            <ul>
                                                <li> This opportunity is for an Identity and Access Management
                                                    Expert
                                                </li>
                                            </ul>
                                            <a href="<?php echo main_url; ?>/Identity-access-management">Read
                                                More..</a> </p>
                                        </div>
                                    </div>
                                </div><!-- </div><div id="accordion" class="accordion"> -->
                                <div class="card">
                                    <div class="card-header"><a class="collapsed card-link" data-toggle="collapse" href="#collapseEight">Cloud Architect - Application
                                            +
                                            Billings </a></div>
                                    <div id="collapseEight" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        United States (Remote)</p>
                                                    <p class="text-muted m-0"><i class="fa fa-user" aria-hidden="true"></i>
                                                        6 - 8 Years experience</p>
                                                    <p class="text-muted m-0"><i class="fa fa-paypal" aria-hidden="true"></i> Best in
                                                        Industry
                                                    </p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="text-muted m-0"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        Full time</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i> Immediate
                                                        Joiner /
                                                        15 Days </p>
                                                </div>
                                            </div>
                                            <p class="text-muted mb-0 mt-3"> As a Cloud Solutions Architect, we look
                                                towards
                                                you as an expert with deep understanding of scalable and distributed
                                                SaaS
                                                application architecture, development and deployment in Agile DevOps
                                                environment. Your role would be to provide architectural and design
                                                leadership with deep-rooted programming experience for the next
                                                generation
                                                of products and solutions for in the domain of cybersecurity
                                                providing
                                                foundational security controls. <a href="<?php echo main_url; ?>/cloud-architect-application-billings">Read
                                                    More..</a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header"><a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">Cloud Full Stack Architect</a>
                                    </div>
                                    <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        United States (Remote)</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-user" aria-hidden="true"></i>
                                                        10 - 15 Years experience</p>
                                                    <p class="text-muted m-0"><i class="fa fa-paypal" aria-hidden="true"></i> Best in
                                                        Industry
                                                    </p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        Full time</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i> Immediate
                                                        Joiner /
                                                        15 Days </p>
                                                </div>
                                            </div>
                                            <p class="text-muted mb-0 mt-3">
                                            <ul>
                                                <li> Previous working experience on MERN stacks</li>
                                                <li> In depth knowledge of NodeJS, ExpressJS, Angular and React</li>
                                                <li> Hands on Experience with AWS Marketplace, Azure Cloud</li>
                                            </ul>
                                            <a href="<?php echo main_url; ?>/cloud-full-stack-architect">Read
                                                More..</a> </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header"><a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo1">Sr. Cloud Engineer</a></div>
                                    <div id="collapseTwo1" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="text-muted mb-0"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        United States (Remote)</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-user" aria-hidden="true"></i>
                                                        3 - 5 Years experience</p>
                                                    <p class="text-muted m-0"><i class="fa fa-paypal" aria-hidden="true"></i> Best in
                                                        Industry
                                                    </p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p class="text-muted m-0"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        Full time</p>
                                                    <p class="text-muted mb-0"><i class="fa fa-check" aria-hidden="true"></i> Immediate
                                                        Joiner /
                                                        15 Days </p>
                                                </div>
                                            </div>
                                            <p class="text-muted mb-0 mt-2"> NetServ is seeking a cloud services
                                                expert to
                                                drive our large scale cloud platform deployment. You will be
                                                engineering our
                                                next generation of infrastructure using major Public Cloud Providers
                                                (e.g.
                                                AWS) in a secure, compliant, and efficient manner. <a href="<?php echo main_url; ?>/senior-cloud-engineer">Read
                                                    More..</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 y-middle ">
                        <div class="about-content">
                            <div class="images-part "><img src="assets/images/career1.png " alt="career"></div>
                            <div class="rs-animations">
                                <div class="spinner dot"><img class="scale" src="assets/images/about/solutions/2.png" alt="Images"></div>
                                <div class="spinner ball"><img class="dance2" src="assets/images/about/solutions/3.png" alt="Images"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="rs-services" class="rs-services style22 pt-100 mt-5 pb-70 md-pt-70 md-pb-70">
            <div class="container">
                <div class="sec-title7 mb-47 md-mb-42">
                    <h2 class="title"><span class="watermark">Careers</span> Join
                        us our
                        team </h2>
                    <div class="bottom-img"><img src="assets/images/consultation/corporate2/underline-number.png" alt="Careers"></div>
                </div>
                <div class="row no-gutters">
                    <div class="col-lg-4">
                        <div class="service-wrap">
                            <div class="icon-part"><img src="assets/images/communication-1.png" alt="Consulting">
                            </div>
                            <div class="content-part">
                                <h3 class="title"><a href="#">Consulting</a></h3>
                                <div class="desc">With the Rapid Evolution of Technology, Enterprise organizations
                                    must
                                    continuously transform their IT Strategy to remain competitive, that requires a
                                    Full
                                    Services Lifecycle partner engagement.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="service-wrap light-bg">
                            <div class="icon-part"><img src="assets/images/worker-1.png" alt="Professional Services"></div>
                            <div class="content-part">
                                <h3 class="title"><a href="#">Professional Services</a></h3>
                                <div class="desc">Our Professional Services enable our customers to successfully
                                    implement a
                                    Full Services Life Cycle. Our team specializes in providing services for next
                                    generation
                                    digital solutions that are tailored to your needs.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="service-wrap">
                            <div class="icon-part"><img src="assets/images/team.png" alt="Managed Services"></div>
                            <div class="content-part">
                                <h3 class="title"><a href="#">Managed Services</a></h3>
                                <div class="desc">Our innovative Managed AIOps solution allows you to improve your
                                    operational efficiencies by event correlation, faster root cause analysis and
                                    automating
                                    manual aspect of IT workflows.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="service-wrap light-bg">
                            <div class="icon-part"><img src="assets/images/computer-1.png" alt="Software Development"></div>
                            <div class="content-part">
                                <h3 class="title"><a href="#">Software Development</a></h3>
                                <div class="desc">With our decades of software development expertise, We start with
                                    discovering your business requirements, design and develop software, deploy in
                                    test to
                                    prod migration, maintenance, and support.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="service-wrap">
                            <div class="icon-part"><img src="assets/images/online-learning.png" alt="Training">
                            </div>
                            <div class="content-part">
                                <h3 class="title"><a href="#">Training</a></h3>
                                <div class="desc">We offer a comprehensive set of advanced technical training
                                    courses. These
                                    Courses enable IT professionals with the knowledge they need to adopt new
                                    technologies
                                    and perform implementation based on Industry best practices.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="service-wrap light-bg">
                            <div class="icon-part"><img src="assets/images/rocket-1.png" alt="Services for Startups"></div>
                            <div class="content-part">
                                <h3 class="title"><a href="#">Services for Startups</a></h3>
                                <div class="desc">These technical support offerings are intended for technology
                                    startups
                                    looking to free up internal resources to focus on product/solution development.
                                    However,
                                    at the same time, want to accelerate the time to market.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- My professional path Section Start -->

        <div id="rs-services" class="rs-services style1 pt-20 modify2 pb-100 pb-0 md-pt-10 md-pb-100 aos-init aos-animate how_can_we_help" data-aos="" data-aos-duration="">
            <div class="container">
                <div class="sec-title text-center">
                    <h2 class="title mb-0">
                        <span class="txt_clr">My Professional Path</span>
                        <br>
                    </h2>
                    <p class="pt-2">Discover what it's like to work at NetServ via anecdotes of our <br>exceptional employees, culture, and work.</p>

                </div>



                <div class="text-center mb-5 mt-4">

                    <div class="dual-btn-wrap"><a href="https://www.ngnetserv.com/career-journeys/" class=""><button type="button" class="btn btn-primary w-30"><span> NetServ Employee Life Blog</span></button>
                        </a>
                    </div>
                </div>
                <!-- <div class="dual-btn">
                    <div class="dual-btn-wrap"><a class="btn-right" href="<?php echo main_url; ?>/contact-us"><span>Contact Us</span></a>
                    </div>
                </div> -->
            </div>
        </div>
        <!-- My professional path Section End -->

    </div>
    </div><?php include 'footer.php'; ?>
    <div id="scrollUp"><i class="fa fa-angle-up"></i></div><?php include 'service_jslinks.php'; ?>
    <script>
        $(document).ready(function() {
                    $('.pages').hide();
                    $(".page_no").click(function() {
                                var a = $(this).data('id');
                                $("#get_number").val(a); //alert(a); $('.pages').hide(); $('.pages').removeClass('active'); $('.page_'+a).show(); $('.page_'+a).addClass('active'); $('.page_no').removeClass('active_1'); $('.page_no_'+a).addClass('active_1');}); // $(".next_arrow").click(function(){// alert(); // var b=parseInt($("#get_number").val()); // alert(b); // var next=b + 1; // alert(next); // $('.pages').hide(); // $('.pages').removeClass('active'); // $('.page_'+next).show(); // $('.page_'+next).addClass('active'); // $("#get_number").val(next); // $('.page_no').removeClass('active_1'); // $('.page_no_'+next).addClass('active_1'); //});});
    </script>
</body>

</html>
