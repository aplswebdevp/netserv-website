<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>NetServ - Simplify and Secure ITOps.</title>
    <meta name="description" content="We empower businesses to get ahead and stay ahead of change by delivering mission-critical IT services. We are driven to lead organizations to modernize IT Operations, Data Center, Cloud and security environments.">
    <meta name="keywords" content="managed it services, it support services, technology solutions, it services company, it support company, infrastructure services, networking services">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com" />
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <link rel="stylesheet" type="text/css" href="<?php echo main_url; ?>/assets/owl/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo main_url; ?>/assets/owl/owl.theme.default.css">
    <script type='application/ld+json'>
        {
            "@context": "http://www.schema.org",
            "@type": "WebSite",
            "name": "NetSev",
            "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style>
/* trusted by section css start */

     .rs-partner.modify5 .partner-item a {
    display: inline-block;
    padding: 0px 40px;
}
/* trusted by section css end */

/* technology partner logo css start */
.rs-partner.modify2 .partner-item a img {
    opacity: 1;
    max-width: 150px;
}
/* technology partner logo css end */

    /* start css for slider scrollbar */
    #categoryes {
        max-height: 180px !important;
        overflow-y: auto !important;

    }

    /* end css for slider scrollbar */


    @media (min-width:768px) and (max-width:991.98px) {
        .i-frame-container iframe {
            width: 100%;
            height: 348px
        }

        .rs-partner.modify5 .partner-item a {
    display: inline-block;
    padding: 0px 25px;
}
    }

    @media (min-width:576px) and (max-width:767.98px) {
        .i-frame-container iframe {
            width: 100%;
            height: 300px
        }
    }

    @media (max-width:575.98px) {
        .i-frame-container iframe {
            width: 100%;
            height: 300px
        }

        @media only screen and (max-width:480px) {
            .i-frame-container iframe {
                width: 100%;
                height: 227px
            }
        }
    }

    /* start css for 4 cards inner icon width and card padding   */
    .plan-st-img-icn {
        width: 14%;
    }

    .rs-services.style13 .service-wrap .content-part {

        padding: 21px 0px 233px;
    }

    .rs-services.style13 .service-wrap .content-part .title {
        font-size: 16px;

    }

    /* end css for 4 cards inner icon width   */

    /* start css for banner images heading text  */
    .rs-slider.slider8 .slider .content-part .sl-title {
        font-size: 40px;
        line-height: 52px;
        font-weight: 800;
        color: #0c121d;
    }

    /* end css for banner images heading text  */

    /* start css for banner height */
    .slider8 .slider {
        height: 700px;

    }

    .rs-slider.slider8 .slider {
        padding: 159px 0 230px;
    }

    /* end css for banner height */

    /* banner sub heading css start*/
    b,
    strong {
        font-weight: 400;
    }

    /* banner sub heading css end*/
    /* start css for banner text and images responsive in mobile view*/
    .slider-bottom {
        font-size: 30px;
    }

    /* end css for banner text and images responsive in mobile view*/

    /* card bottom and top spacing css */
    .rs-services.style13 .service-wrap .content-part {
        padding: 1px 0px 72px;
    }

    /* .rs-services.style13 .service-wrap .content-part {
        padding: 1px 0px 72px;
    } */

    /* card bottom and top spacing css */
    .our-sec {
        border-radius: 25px;
    }

    .img-width {
        width: 100%;
    }

    /* start css for new Services Offerings card 4-11-23 */
    .font-css {
        font-size: 1.2rem;
    }

    /* start css for card text */

    /* .text-css {
        font-size: 16px;
    } */

    /* end css for card text */
    /* end css for new Services Offerings card 4-11-23 */

    /* start css for card*/

    @media only screen and (min-width: 992px) and (max-width: 1199px) {
        .mng-cld {
            padding-top: 1.8rem;
        }
    }

    /* end css for card */

    /* start css for customer-success-stories img-slider */
    @media only screen and (min-width: 280px) and (max-width: 320px) {
        .stories_img_txt {
            font-size: 7px;
            top: 7%;
        }

        .project-inner p {
            font-size: 7px;
        }

        .stories_subtext {

            font:small-caption;
            font-size: 7px;
            display: block;
            padding: 0px !important;
        }

        .qute {
            width: 2% !important;
        }

        .stories_btn {
            background: 0 0;
            padding: 0;
            border-radius: 0;
            color: #fff;
            font-size: 5px !important;
        }

        .rs-project.style4 .project-item .project-inner {
            padding: 3px 6px;
        }

        .round {
            position: absolute;
            bottom: 20px;
            left: 80% !important;
            transform: translateX(-50%);
            border: 2px solid white;
            width: 45px;
            height: 45px;
            border-radius: 100%;
            background: #106eea;
        }

       /* start css for card eading heading and text  */
       .font-css {
    font-size: 0.6rem;
}
p {
  
    font-size: 12px;
}
        /* end css for card eading heading  and text*/

        /* start css for trusted by logo */
        .montage {
                padding-top: 10px;
                width: 85px;
        }
        /* end css for trusted by logo */
        /* trusted by section css start */

     .rs-partner.modify5 .partner-item a {
    display: inline-block;
    padding: 0px 2px;
}
/* trusted by section css end */

    }

    @media only screen and (min-width: 320px) and (max-width: 375px) {
        .stories_img_txt {
            font-size: 8px;
            top: 7%;
        }

        .project-inner p {
            font-size: 7px;
        }

        .stories_subtext {

            font: small-caption;
            font-size: 8px;
            display: block;
            padding: 0px !important;
        }

        .qute {
            width: 2% !important;
        }

        .stories_btn {
            background: 0 0;
            padding: 0;
            border-radius: 0;
            color: #fff;
            font-size: 6px !important;
        }

        .rs-project.style4 .project-item .project-inner {
            padding: 3px 6px;
        }
        /* start css for card eading heading and text  */
        .font-css {
    font-size: 0.7rem;
}
p {
  
    font-size: 13px;
}
        /* end css for card eading heading  and text*/
         /* start css for trusted by logo */
         .montage {
                padding-top: 7px;
                width: 116px;
        }
        /* end css for trusted by logo */
        /* trusted by section css start */

     .rs-partner.modify5 .partner-item a {
    display: inline-block;
    padding: 0px 2px;
}
/* trusted by section css end */
    }

    @media only screen and (min-width: 375px) and (max-width: 480px) {
        .stories_img_txt {
            font-size: 10px;
            top: 12%;
        }

        .project-inner p {
            font-size: 7px;
        }

        .stories_subtext {

            font: small-caption;
            font-size: 9px;
            display: block;
            padding: 0px !important;
        }

        .qute {
            width: 2% !important;
        }

        .stories_btn {
            background: 0 0;
            padding: 0;
            border-radius: 0;
            color: #fff;
            font-size: 9px !important;
        }

        .rs-project.style4 .project-item .project-inner {
            padding: 6px 6px;
        }

        /* start css for card eading heading and text  */
        .font-css {
    font-size: 0.9rem;
}
p {
  
    font-size: 13px;
}
        /* end css for card eading heading  and text*/
        /* start css for trusted by logo */
        .montage {
                padding-top: 7px;
                width: 128px;
        }
        /* end css for trusted by logo */
        /* trusted by section css start */

     .rs-partner.modify5 .partner-item a {
    display: inline-block;
    padding: 0px 7px;
}
/* trusted by section css end */

    }



    @media only screen and (min-width: 480px) and (max-width: 600px) {
        .stories_img_txt {
            font-size: 12px;
            top: 12%;
        }

        .project-inner p {
            font-size: 10px;
        }

        .stories_subtext {

            font: menu;
            font-size: 12px;
            display: block;
            padding: 0px !important;
        }

        /* .qute {
            width: 2% !important;
        } */

        .stories_btn {
            background: 0 0;
            padding: 0;
            border-radius: 0;
            color: #fff;
            font-size: 11px !important;
        }

        .rs-project.style4 .project-item .project-inner {
            padding: 6px 6px;
        }

        /* start css for card eading heading  */
        .font-css {
    font-size: 1rem;
}
        /* end css for card eading heading  */

     
         /* start css for trusted by logo */
         .montage {
                padding-top: 10px;
        }
        /* .splunk{
            padding-top: 7px;
        } */
        /* end css for trusted by logo */

    }

    @media only screen and (min-width: 600px) and (max-width: 992px) {
        .stories_img_txt {
            font-size: 15px;
            top: 12%;
        }

        .project-inner p {
            font-size: 10px;
        }

        .stories_subtext {

            font: menu;
            font-size: 15px;
            display: block;
            padding: 0px !important;
        }

        /* .qute {
            width: 2% !important;
        } */

        .stories_btn {
            background: 0 0;
            padding: 0;
            border-radius: 0;
            color: #fff;
            font-size: 15px !important;
        }

        .rs-project.style4 .project-item .project-inner {
            padding: 6px 6px;
        }

        
    }

    /* end css for customer-success-stories img-slider */
    @media only screen and (min-width: 280px) and (max-width: 425px) {
        .rs-slider.slider8 .slider .content-part .sl-title {
            font-size: 30px;
            line-height: 35px;
            font-weight: 600;
            color: #0c121d;
        }

        .slider-bottom {
            font-size: 17px;
        }

        .stories_learn_more {
            padding-top: 0px !important;
        }

        /* start css for cards padding */
        .cards_padding{
            padding:1rem !important;
        }
        /* end css for cards padding */


    }

    @media only screen and (min-width: 425px) and (max-width: 576px) {
        .rs-slider.slider8 .slider .content-part .sl-title {
            font-size: 40px;
            line-height: 45px;
            font-weight: 600;
            color: #0c121d;
        }

        .slider-bottom {
            font-size: 25px;
        }

        .stories_learn_more {
            padding-top: 0px !important;
        }

         /* start css for cards padding */
         .cards_padding{
            padding:1rem !important;
        }
        /* end css for cards padding */
    }

    .fly-btn a {
        width: 40px;
        height: 40px;
        line-height: 40px;
        border-radius: 100%;
        background: #106eea;
        text-align: center;
        display: inline-block;
        color: white;

    }

    .fly-btn a :hover {
        width: 40px;
        height: 40px;
        line-height: 40px;
        border-radius: 100%;
        background: #108aea;
        ;
        text-align: center;
        display: inline-block;
        color: white;

    }

    /* start css for card and  arrow btn */
    .fly-btn {
        position: absolute;
        bottom: 20px;
        left: 85%;
        transform: translateX(-50%);
    }

    .rs-services.style8 .service-wrap {
        padding: 32px 23px;
        background: #fff;
        /* box-shadow: 0 0 34px 20px rgba(0, 0, 0, 0.02); */
        box-shadow: 0 0.2rem 2rem rgba(0, 0, 0, 0.15);
        border-radius: 5px;
    }

    @media only screen and (min-width: 280px) and (max-width: 992px) {
        .mngclod {
            padding-bottom: 10px !important;
        }
        
    }

    @media only screen and (min-width: 990px) and (max-width: 1200px) {
        .slidertxt {
            font-size: 14px;
        }
        .rs-partner.modify5 .partner-item a {
    display: inline-block;
    padding: 0px 25px;
}
    }

    /* end css for card and  arrow btn */

    /* start css for Security Services Offerings */
    .rs-services.style19 .services-item .services-wrap .icon-part.purple-bg {
        background: #007bff;
    }

    .rs-services.style19 .services-item .services-wrap .icon-part.purple-bg:before {
        border: 1px solid #007bff;
    }

    .rs-services.style19 .services-item .services-wrap .services-content .services-title .title a:hover {
        color: #04004d;
    }

    /* end css for Security Services Offerings */
    /* start css for 1st section cards text */
    

    @media only screen and (min-width: 1715px) and (max-width: 2860px) {
        .mngclod {
            padding-bottom: 8px !important;
        }
    }

    @media only screen and (min-width: 1269px) and (max-width: 1440px) {
        .font-css {
    font-size: 1rem;
    }
    .cycs {
        padding-bottom: .5rem !important;
    }
    .network {
        padding-bottom: .5rem !important;
    }
    }

    @media only screen and (min-width: 990px) and (max-width: 1268px) {

        .mngclod {
            padding-bottom: .5em !important;
        }

        .databacup {
            padding-bottom: .5rem !important;
        }
        .cycs {
            padding-bottom: 1.3rem !important;
        }

        .network {
            padding-bottom: .5px !important;
        }


        .font-css {
            font-size: 15px;
        }

        .rs-services.style8 .service-wrap .content-part .title {
            margin-bottom: 9px !important;
            font-weight: 600 !important;
            line-height: 22px !important;

        }

        /* .font-css {
            font-size: 13px;
        }

        p {

            font-size: 15px;
        } */

    }
    @media only screen and (min-width: 1148px) and (max-width: 1268px) {
        .cycs {
        padding-bottom: 0rem !important;
    }
    }
    @media only screen and (min-width: 992px) and (max-width: 1118px) {
        .databacup  {
            padding-bottom: 0px !important;
        }
        .mngclod {
        padding-bottom: 1.3em !important;
    }
    }
    .rs-services.style8 .service-wrap .content-part .title {
        /* margin-bottom: 9px;
        font-weight: 600;
        line-height: 22px; */
        font-weight: 500;
        line-height: 22px;
        font-family: "Poppins", sans-serif;
    }

    /* end css for 1st section cards text css */

    /* start css for card arrow  */


    .center-con {
        display: flex;

        align-items: center;
        justify-content: end;
    }



    .round {
        position: absolute;
        bottom: 20px;
        left: 85%;
        transform: translateX(-50%);

        border: 2px solid white;
        width: 45px;
        height: 45px;
        border-radius: 100%;
        background: #106eea;

    }

    #cta {
        width: 100%;
        cursor: pointer;
        position: absolute;
    }

    #cta .arrow {
        left: 30%;
    }

    .arrow {
        position: absolute;
        bottom: 0;
        margin-left: 0px;
        width: 12px;
        height: 12px;
        top: 14px;
    }

    .segunda {
        margin-left: 8px;
    }

    .next {
        background-image: url(data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2aWV3Qm94PSIwIDAgNTEyIDUxMiI+PHN0eWxlPi5zdDB7ZmlsbDojZmZmfTwvc3R5bGU+PHBhdGggY2xhc3M9InN0MCIgZD0iTTMxOS4xIDIxN2MyMC4yIDIwLjIgMTkuOSA1My4yLS42IDczLjdzLTUzLjUgMjAuOC03My43LjZsLTE5MC0xOTBjLTIwLjEtMjAuMi0xOS44LTUzLjIuNy03My43UzEwOSA2LjggMTI5LjEgMjdsMTkwIDE5MHoiLz48cGF0aCBjbGFzcz0ic3QwIiBkPSJNMzE5LjEgMjkwLjVjMjAuMi0yMC4yIDE5LjktNTMuMi0uNi03My43cy01My41LTIwLjgtNzMuNy0uNmwtMTkwIDE5MGMtMjAuMiAyMC4yLTE5LjkgNTMuMi42IDczLjdzNTMuNSAyMC44IDczLjcuNmwxOTAtMTkweiIvPjwvc3ZnPg==);
    }

    @keyframes bounceAlpha {
        0% {
            opacity: 1;
            transform: translateX(0px) scale(1);
        }

        25% {
            opacity: 0;
            transform: translateX(10px) scale(0.9);
        }

        26% {
            opacity: 0;
            transform: translateX(-10px) scale(0.9);
        }

        55% {
            opacity: 1;
            transform: translateX(0px) scale(1);
        }
    }

    .bounceAlpha {
        animation-name: bounceAlpha;
        animation-duration: 1.4s;
        animation-iteration-count: infinite;
        animation-timing-function: linear;
    }

    .arrow.primera.bounceAlpha {
        animation-name: bounceAlpha;
        animation-duration: 1.4s;
        animation-delay: 0.2s;
        animation-iteration-count: infinite;
        animation-timing-function: linear;
    }

    .round:hover .arrow {
        animation-name: bounceAlpha;
        animation-duration: 1.4s;
        animation-iteration-count: infinite;
        animation-timing-function: linear;
    }

    .round:hover .arrow.primera {
        animation-name: bounceAlpha;
        animation-duration: 1.4s;
        animation-delay: 0.2s;
        animation-iteration-count: infinite;
        animation-timing-function: linear;
    }

    /* end css for card arrow  */

    /* start css for security services offering cards */
    .headtxt {
        font-size: 1.2rem;
    }

    .servic-offer:hover {
        animation-name: pulse;
        animation-duration: 2s;
        animation-timing-function: ease-in-out;
        animation-iteration-count: 1;
    }

    /* end css for security services offering cards */
</style>

<body class="home-eight"><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

    <div class="full-width-header header-style4">
        <?php include 'header.php'; ?> </div>
        <!-- <div class="alert py-3 text-light fw-normal" role="alert" style="background-color:#50a265; font-size:17px; border-radius:0px !important; margin-bottom:0px !important;">
        As of September 19, 2024, NetServ has been acquired by Gruve. Read the press release <a class="text-light " href="https://www.gruve.ai/news/gruve-acquires-netserv-to-drive-ai-powered-services-for-enterprise-growth"><u>here</u></a>
</div> -->
    <!-- main-content div start -->
    <div class="main-content">
        <div id="rs-slider" class="rs-slider slider8 banner-text">
            <div class="slider-carousel owl-carousel">
                <!-- new 1st slide added start -->
                <div class="slider slide1">
                    <div class="container">
                        <div class="content-part">
                            <div class="slider-des">
                                <h2 class="sl-title mb-0  banner-text ">Customer <span class="txt_clr "> Challenges </span></h2>
                                <p class="pt-2 slider-bottom text-black banner-text"><strong>Lack of resources, Evolving threat landscape, and Limited Budget. </strong> </p>
                            </div>
                            <div class="slider-bottom mt-40">
                                <ul>
                                    <li><a href="<?php echo main_url; ?>contact-us" class="readon contact_us banner-text">Talk to us</a></li>
                                </ul>
                            </div>
                            <p class="mt-80"></p>
                        </div>
                    </div>
                </div>
                <!-- new 1st slide added end -->

                <!-- new 2nd slide added start -->
                <div class="slider slide2">
                    <div class="container">
                        <div class="content-part">
                            <div class="slider-des">
                                <h2 class="sl-title mb-0 banner-text"><span class="txt_clr">ITOps </span> Services for <span class="txt_clr">Healthcare </span></h2>
                                <p class="pt-3 slider-bottom text-black banner-text"> <strong>Ensuring patient data protection and industry security during digital record transition.</strong> </p>
                            </div>
                            <div class="slider-bottom mt-10">
                                <ul>
                                    <li><a href="<?php echo main_url; ?>contact-us" class="readon contact_us banner-text">Talk to us</a></li>
                                </ul>
                            </div>
                            <p class="mt-80"></p>
                        </div>
                    </div>
                </div>
                <!-- new 2nd slide added end -->

                <!-- new 3rd slide added start -->
                <div class="slider slide3">
                    <div class="container">
                        <div class="content-part">
                            <div class="slider-des">
                                <!-- <h2 class="sl-title mb-0">Services for <span class="txt1">Startups</span></h2> -->
                                <h2 class="sl-title mb-0 banner-text">Managed<span class="txt_clr"> CyberSecurity</span> Services for <br> <span class="txt_clr">Life Science</span></h2>

                                <p class="pt-2 slider-bottom text-black banner-text"><strong>Adopting enhanced strategies drives life sciences industry innovation through reliable and secure IT solutions.</strong> </p>
                            </div>
                            <div class="slider-bottom mt-40">
                                <ul>
                                    <li><a href="<?php echo main_url; ?>contact-us" class="readon contact_us banner-text">Talk to us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- new 3rd slide added end -->

                <!-- new 4rth slide added start -->
                <div class="slider slide4">
                    <div class="container">
                        <div class="content-part">
                            <div class="slider-des">
                                <h2 class="sl-title mb-0 banner-text">Managed <span class="txt_clr">SOC</span> and <span class="txt_clr">NOC</span></h2>
                                <p class="pt-2 slider-bottom text-black banner-text"><strong>Prioritize cybersecurity to outpace threat actors and fortify organizations with a proactive defense strategy.</strong> </p>
                            </div>
                            <div class="slider-bottom mt-40">
                                <ul>
                                    <li><a href="<?php echo main_url; ?>contact-us" class="readon contact_us banner-text">Talk to us</a></li>
                                </ul>
                            </div>
                            <p class="mt-80"></p>
                        </div>
                    </div>
                </div>
                <!-- new 4rth slide added end -->

                <!-- new 5th slide added start -->
                <div class="slider slide5">
                    <div class="container">
                        <div class="content-part">
                            <div class="slider-des">
                                <h2 class="sl-title mb-0 banner-text"> <span class="txt_clr">Security</span> and <span class="txt_clr">Cloud</span> Assessments</h2>
                                <p class="pt-1 slider-bottom text-black banner-text"><strong>Unveil security flaws, assisting organizations in understanding IT risks for strong protection.</strong> </p>
                            </div>
                            <div class="slider-bottom mt-10">
                                <ul>
                                    <li><a href="<?php echo main_url; ?>contact-us" class="readon contact_us banner-text">Talk to us</a></li>
                                </ul>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- new 5th slide added end -->

            </div>
        </div>

        <!-- Services Offerings Start -->
        <div class="rs-services style8 shape-bg2 pt-30 pb-30 md-pt-67 md-pb-50">
            <div class="p-5 cards_padding">
                <div class="sec-title mb-38 md-mb-41">
                    <h2 class="title mb-0 text-center">Services Offerings</h2>
                </div>
                <div class="row">
                    <div class="col-lg-4 mb-30">
                        <div class="service-wrap h-100">
                            <div class="icon-part three">
                                <img src="<?php echo main_url; ?>/assets/images/services/icons/style8/system_1.webp" alt="Cybersecurity" title="Cybersecurity">
                            </div>
                            <div class="content-part">
                                <h6 class="title font-css cycs pb-2"><a href="<?php echo main_url; ?>services/managed-services/managed-cybersecurity-services">
                                         Cybersecurity</a></h6>
                                <p class="desc mb-52 text-css">Improve your protection and safeguard against ransomware, insider threats, and malware with advanced threat intelligence. We prevent dangers with next-gen firewalls, intrusion prevention, and real-time monitoring, featuring MDR, XDR, Vulnerability management, Identity & Access Management Cloud Security.</p>

                                <!-- <div class="fly-btn text-right">
                                    <a href="<?php echo main_url; ?>services/managed-services/managed-cybersecurity-services"><i class="flaticon-right-arrow"></i></a>
                                </div> -->
                                <div class="center-con">
                                    <a href="<?php echo main_url; ?>services/managed-services/managed-cybersecurity-services">

                                        <div class="round">
                                            <div id="cta">
                                                <span class="arrow primera next "></span>
                                                <span class="arrow segunda next "></span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-30">
                        <div class="service-wrap h-100">
                            <div class="icon-part three">
                                <img src="<?php echo main_url; ?>/assets/images/services/icons/style8/system_4.webp" alt="SOC-and-NOC" title="SOC-and-NOC">
                            </div>
                            <div class="content-part">
                                <h6 class="title font-css pb-2 cycs"><a href="<?php echo main_url; ?>services/managed-services/managed-soc-and-noc-services">SOC and NOC</a></h6>
                                <!-- <p class="desc text-css m-0"> Add Proactive monitoring, incident response and management remove AI
                                </p> -->
                                <p class="desc mb-52 text-css">Add Proactive monitoring, incident response and management. SOC utilizes advanced technology for SIEM and incident response and management. Simultaneously, NOC uses predictive analytics and follows the sun monitoring for optimal performance, blending SOAR and advanced analytics to ensure operational continuity and enhance security foresight.</p>

                                <!-- <div class="fly-btn text-right">
                                    <a href="<?php echo main_url; ?>services/managed-services/managed-soc-and-noc-services"><i class="flaticon-right-arrow"></i></a>
                                </div> -->
                                <div class="center-con">
                                    <a href="<?php echo main_url; ?>services/managed-services/managed-soc-and-noc-services">

                                        <div class="round">
                                            <div id="cta">

                                                <span class="arrow primera next "></span>
                                                <span class="arrow segunda next "></span>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-30">
                        <div class="service-wrap h-100">
                            <div class="icon-part three">
                                <img src="<?php echo main_url; ?>/assets/images/services/icons/style8/system_3.webp" alt="Managed-Network-and-Communication" title="Managed-Network-and-Communication">
                            </div>
                            <div class="content-part">
                                <h6 class="title font-css network pb-2"><a href="<?php echo main_url; ?>services/managed-services/managed-network-services">Network and Unified Communication</a></h6>
                                <p class="desc text-css mb-52"> Transform enterprise networks to scalable Cloud-first networks and VoIP solutions. We offer end-to-end management and help customers modernize & improve resilience, operational agility, processes and customer experience in SD-WAN, Network Security, VPN, Network and Infra implementation, SASE, Unified Communications, and global Contact Centers.</p>

                                <!-- <div class="fly-btn text-right">
                                    <a href="<?php echo main_url; ?>services/managed-services/managed-network-services"><i class="flaticon-right-arrow"></i></a>
                                </div> -->
                                <div class="center-con">
                                    <a href="<?php echo main_url; ?>services/managed-services/managed-network-services">

                                        <div class="round">
                                            <div id="cta">

                                                <span class="arrow primera next "></span>
                                                <span class="arrow segunda next "></span>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 mb-30">
                        <div class="service-wrap h-100">
                            <div class="icon-part three">
                                <img src="<?php echo main_url; ?>/assets/images/services/icons/style8/system_5.webp" alt="Managed-Data-Backup-and-Disaster-Recovery" title="Managed-Data-Backup-and-Disaster-Recovery">

                            </div>
                            <div class="content-part">
                                <h6 class="title font-css databacup pb-2"><a href="<?php echo main_url; ?>services/managed-services/managed-data-backup-and-disaster-recovery-services">Data Backup and Disaster Recovery </a></h6>
                                <p class="desc mb-52 text-css">Enhance data resilience with Managed Data Backup and Disaster Recovery Services. Utilizing cloud storage, automation, and immutable backups, we ensure data readiness. AI-driven insights, continuous replication, and instant recovery bolster business continuity, guarding against data loss.</p>

                                <!-- <div class="fly-btn text-right">
                                    <a href="<?php echo main_url; ?>services/managed-services/managed-data-backup-and-disaster-recovery-services"><i class="flaticon-right-arrow"></i></a>
                                </div> -->
                                <div class="center-con">
                                    <a href="<?php echo main_url; ?>services/managed-services/managed-data-backup-and-disaster-recovery-services">

                                        <div class="round">
                                            <div id="cta">

                                                <span class="arrow primera next "></span>
                                                <span class="arrow segunda next "></span>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-30">
                        <div class="service-wrap h-100">
                            <div class="icon-part three">
                                <img src="<?php echo main_url; ?>/assets/images/services/icons/style8/system_6.webp" alt="Managed-Cloud" title="Managed-Cloud">
                            </div>
                            <div class="content-part">
                                <h6 class="title font-css mngclod pb-2"><a href="<?php echo main_url; ?>services/managed-services/managed-cloud-services">Cloud Optimization </a></h6>
                                <p class="desc mb-52 text-css"> Enhance cloud functions with multi-cloud platform management (AWS, Azure, GCP), deployment, configuration, and optimization. Ensure peak performance, scalability, cost-effectiveness, and seamless operations. Our expert team delivers cloud automation and real-time data processing, fostering efficiency and innovation.</p>

                                <!-- <div class="fly-btn text-right">
                                    <a href="<?php echo main_url; ?>services/managed-services/managed-cloud-services"><i class="flaticon-right-arrow"></i></a>
                                </div> -->
                                <div class="center-con">
                                    <a href="<?php echo main_url; ?>services/managed-services/managed-cloud-services">

                                        <div class="round">
                                            <div id="cta">

                                                <span class="arrow primera next "></span>
                                                <span class="arrow segunda next "></span>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-30">
                        <div class="service-wrap h-100">
                            <div class="icon-part three">
                                <img src="<?php echo main_url; ?>/assets/images/services/icons/style8/3.webp" alt="Consulting & Advisory" title="Consulting & Advisory">
                            </div>
                            <div class="content-part">
                                <h6 class="title font-css mngclod pb-2"><a href="<?php echo main_url; ?>services/consulting-services/advisory-services">Consulting and Advisory</a></h6>
                                <p class="desc mb-52 text-css"> Excel in meticulous assessment and cloud protection. Implement contemporary cybersecurity frameworks to safeguard infrastructure longevity, provide a roadmap for a resilient, agile IT ecosystem, and elevate security and fortify IT strength through digital transformation, vCISO expertise, and strategic cloud assessment.</p>

                                <!-- <div class="fly-btn text-right">
                                    <a href="<?php echo main_url; ?>services/consulting-services/advisory-services"><i class="flaticon-right-arrow"></i></a>
                                </div> -->
                                <div class="center-con">
                                    <a href="<?php echo main_url; ?>services/consulting-services/advisory-services">

                                        <div class="round">
                                            <div id="cta">

                                                <span class="arrow primera next "></span>
                                                <span class="arrow segunda next "></span>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Services Offerings End -->

        <!-- Trusted By Start -->
        <div class="rs-partner modify5 pt-80 pb-20" data-aos="" data-aos-duration="">
            <div class="" align="center">
                <h2>Trusted By</h2> <br>
            </div>
            <div class="container">
                <div class="rs-carousel owl-carousel owl-carousel1" data-loop="true" data-items="4" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="3000" data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="2" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="3" data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="2" data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="4" data-lg-device="4" data-md-device-nav="false" data-md-device-dots="false">

                    <div class="partner-item "> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/trusted/armis.webp" loading="lazy" alt="Trusted By"></a> </div>
                    <div class="partner-item pt-1"> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/trusted/odor1.webp" loading="lazy" alt="Trusted By"></a> </div>
                    <div class="partner-item pt-3"> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/trusted/western.webp" loading="lazy" alt="Trusted By"></a> </div>
                    <div class="partner-item pt-2"> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/trusted/kaiserlogo.webp" loading="lazy" alt="Trusted By"></a> </div>

                    <div class="partner-item"> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/trusted/Client__3-min.webp" loading="lazy" alt="Trusted By"></a> </div>
                    <div class="partner-item"> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/trusted/Client__4-min.webp" loading="lazy" alt="Trusted By"></a> </div>
                    <div class="partner-item pt-2"> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/trusted/bwllogo.webp" loading="lazy" alt="Trusted By"></a> </div>
                    <div class="partner-item "> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/trusted/coinbase-new.webp" loading="lazy" alt="Trusted By" style="width: 128px;"></a> </div>
                    <div class="partner-item splunk pt-1"> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/trusted/splunk-new.webp" loading="lazy" alt="Trusted By" style="width: 142px;"></a> </div>

                    <div class="partner-item montage pt-3"> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/trusted/montage-health-logo.webp" loading="lazy" alt="Trusted By" style=""></a> </div>

                    <div class="partner-item "> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/trusted/stanford.webp" loading="lazy" alt="Trusted By"></a> </div>
                    <div class="partner-item pt-2"> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/trusted/celonas.webp" loading="lazy" alt="Trusted By"></a> </div>
                    <div class="partner-item "> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/trusted/nvidia-new.webp" loading="lazy" alt="Trusted By"></a> </div>
                    <div class="partner-item mr-0 ml-3"> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/trusted/ssai.webp" loading="lazy" alt="Trusted By"></a> </div>
                </div>
            </div>
        </div>
        <!-- Trusted By End -->

        <!-- Customer Success Stories Start -->

        <div class="rs-project style4 pt-80 pb-80 md-pt-70 md-pb-70" data-aos="" data-aos-duration="">
            <div class="container">
                <div class="row md-mb-20">
                    <div class="col-lg-12">
                        <div class="sec-title3 text-center md-center mb-40 md-mb-20">
                            <h2 class="title">Customer Success Stories </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div id="rs-project-style4" class="rs-carousel owl-carousel">

                <!-- <div class="project-item">
                    <div class="project-img"> <a><img src="<?php echo main_url; ?>/assets/images/services/planstatergy/n1.webp" alt="Customer Success Stories" loading="lazy"></a> <span class="stories_img_txt"> Managed Services for Healthcare</span> </div>
                    <div class="project-inner" id="categoryes">
                        <p class="stories_subtext p-2 m-0" align="center"><img src="<?php echo main_url; ?>/assets/images/quote3.png" loading="lazy" style="width:4%" alt="Customer Success Stories"></p><span class="category stories_subtext pb-4"><a> A significant healthcare client approached NetServ concerning aging IT infrastructure and security issues. NetServ implemented a modern IT solution for the hospital's wired and wireless networks and VXLAN-based fabric, CheckPoint Firewalls, and F5 Load Balancers for the data center. NetServ's implementation of Cisco ISE, Cisco ASA, Stealthwatch, Tetration, and Umbrella enhanced network security and segmentation at the hospital. The transformation improved the client's operational efficiency, reduced operating costs, and increased security. NetServ's tailored IT solution enabled the healthcare client to minimize security risks while reaping significant benefits. </a></span>
                        <p class="pt-3 stories_learn_more mb-0"><a href="<?php echo main_url; ?>/success-stories#managed-services-for-healthcare" class=" mt-2 stories_btn">Learn More</a></p>
                    </div>
                </div> -->

                <div class="project-item">
                    <div class="project-img"> <a><img src="<?php echo main_url; ?>/assets/images/services/planstatergy/sec-service.webp" alt="Customer Success Stories" loading="lazy"></a> <span class="stories_img_txt">Transforming the Healthcare Industry for Security </span> </div>
                    <div class="project-inner" id="">
                        <p class="stories_subtext p-2 m-0" align="center"><img src="<?php echo main_url; ?>/assets/images/quote3.png" loading="lazy" style="width:4%" class="qute" alt="Customer Success Stories"></p><span class="category stories_subtext pb-4"><a> In the ever-evolving healthcare industry, our clients face a multitude of challenges. They grappled with poor functionality of their existing security tools, sky-high prices, and sluggish response times from their vendor. They had encountered a cyber attack but lacked a clear response plan, leading to concerns about data leaks. Outsourcing security services to an MSSP left them dissatisfied.</a></span>
                        <p class="pt-3 stories_learn_more mb-0"><a href="<?php echo main_url; ?>contact-us" class=" mt-2 stories_btn">Learn More</a></p>

                    </div>
                </div>
                <div class="project-item">
                    <div class="project-img"> <a><img src="<?php echo main_url; ?>assets/images/sec-service21.webp" alt="Customer Success Stories" loading="lazy"></a> <span class="stories_img_txt">Revolutionizing Life Science Security and Compliance</span> </div>
                    <div class="project-inner" id="">
                        <p class="stories_subtext p-2 m-0" align="center"><img src="<?php echo main_url; ?>/assets/images/quote3.png" loading="lazy" style="width:4%" class="qute" alt="Customer Success Stories"></p><span class="category stories_subtext pb-4"><a> Our recent success story involves a prominent life sciences company addressing challenges in security and compliance. By streamlining expenses, prioritizing compliance, implementing prevention tools, and shifting financial models, we helped them overcome hurdles and achieve excellence in security compliance, positioning executives for potential promotions. </a></span>
                        <p class="pt-3 stories_learn_more mb-0"><a href="<?php echo main_url; ?>contact-us" class=" mt-2 stories_btn">Learn More</a></p>
                    </div>
                </div>
                <div class="project-item">
                    <div class="project-img"> <a><img src="<?php echo main_url; ?>/assets/images/Advanced-Cloud-min.webp" alt="Customer Success Stories" loading="lazy"></a> <span class="stories_img_txt"> Hybrid Cloud Managed Services</span> </div>
                    <div class="project-inner">
                        <p class="stories_subtext p-2 m-0" align="center"><img src="<?php echo main_url; ?>/assets/images/quote3.png" loading="lazy" style="width:4%" class="qute" alt="Customer Success Stories"></p><span class="category stories_subtext pb-4"><a>NetServ has been our incredible partner, bringing extensive experience in hybrid cloud monitoring and operation expertise. Netserv functions as an extension of our own IT team with their 24×7 advanced outcome-based managed services.</a></span>
                        <p class="pt-3 stories_learn_more mb-0"><a href="<?php echo main_url; ?>contact-us" class=" mt-2 stories_btn">Learn More</a></p>
                    </div>
                </div>
                <div class="project-item">
                    <div class="project-img"> <a><img src="<?php echo main_url; ?>/assets/images/SD-WAN1-min.webp" alt="Customer Success Stories" loading="lazy"></a> <span class="stories_img_txt">SD-WAN - Design and Implementation</span> </div>
                    <div class="project-inner">
                        <p class="stories_subtext p-2 m-0" align="center"><img src="<?php echo main_url; ?>/assets/images/quote3.png" loading="lazy" style="width:4%" class="qute" alt="Customer Success Stories"></p><span class="category stories_subtext pb-4"><a>NetServ has been our go-to partner for our advanced global networking requirements, bringing extensive experience of advanced networking skillset. Because of their knowledge in SD-WAN and advanced networking, they successfully deployed a vendor-agnostic SD-WAN solution that solved our business challenges. </a></span>
                        <p class="pt-3 stories_learn_more mb-0"><a href="<?php echo main_url; ?>contact-us" class=" mt-2 stories_btn">Learn More</a></p>
                    </div>
                </div>
                <div class="project-item">
                    <div class="project-img"> <a><img src="<?php echo main_url; ?>/assets/images/Infrastructure_1-min.webp" loading="lazy" alt="Customer Success Stories"></a> <span class="stories_img_txt">SD-Campus Design and Migration</span> </div>
                    <div class="project-inner">
                        <p class="stories_subtext p-2 m-0" align="center"><img src="<?php echo main_url; ?>/assets/images/quote3.png" loading="lazy" style="width:4%" class="qute" alt="Customer Success Stories"></p><span class="category stories_subtext pb-4"><a>NetServ team was there leading our time-sensitive legacy campus network to SD-Campus migration. Their team understood our environment, challenges, and business outcomes, which helped us successfully design and migrate to the SD-Campus environment.</a></span>
                        <!-- <p class="pt-3 stories_learn_more mb-0"><a href="<?php echo main_url; ?>success-stories#Customer-Success-SD-campus" class=" mt-2 stories_btn">Learn More</a></p> -->
                        <p class="pt-3 stories_learn_more mb-0"><a href="<?php echo main_url; ?>contact-us" class=" mt-2 stories_btn">Learn More</a></p>

                    </div>
                </div>
                <div class="project-item">
                    <div class="project-img"> <a><img src="<?php echo main_url; ?>/assets/images/sase1.webp" loading="lazy" alt=" secure access service edge" title=" secure access service edge"></a> <span class="stories_img_txt">SASE POC and Design</span> </div>
                    <div class="project-inner">
                        <p class="stories_subtext p-2 m-0" align="center"><img src="<?php echo main_url; ?>/assets/images/quote3.png" loading="lazy" style="width:4%" class="qute" alt="Customer Success Stories"></p><span class="category stories_subtext pb-4"><a>Today’s cloud-centric world drives the need for a secure access service edge (SASE) architecture which combines networking and security functions in the cloud.
                            </a></span>
                        <p class="pt-3 stories_learn_more mb-0"><a href="<?php echo main_url; ?>contact-us" class=" mt-2 stories_btn">Learn More</a></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Customer Success Stories End -->

        <!-- Security Services Offerings Start -->
        <div class="rs-pricing style1">
            <div class="top-part bg10 pt-50 pb-50 md-pt-73 sm-pb-100">
                <div class="container">
                    <div class="sec-title">
                        <h2 class="title white-color mb-90">Security Services Offerings</h2>
                        <!-- <p class="title white-color pb-80 ">
                            In the healthcare and life science sectors, cyber threats are pervasive. Safeguard organization with NetServ's Threat Detection and Response services, ensuring comprehensive protection. Our 24/7 threat management, powered by AI, enhances security, operational efficiency, and hybrid cloud safety. Bid farewell to alert fatigue and vulnerability blind spots with our MDR services.</p> -->

                    </div>
                </div>
            </div>
            <div class=" bg11 pb-40 md-pb-40">
                <div class="p-3">
                    <div class="row gutter-20">
                        <div class="col-md-3 mt--80 sm-mb-30">
                            <div class="pricing-wrap">
                                <div class="services-icon">
                                    <img class="mb-20 servic-offer" src="<?php echo main_url; ?>/assets/images/services/icons/style8/detection-3.webp" style="height:50px" alt="Security Services Offerings" title="Security Services Offerings">

                                </div>
                                <div class="top-part m-0">
                                    <h6 class="headtxt">Cyber Security </h6>
                                </div>
                                <div class="middle-part p-3">
                                    <div class="p-10">
                                        Our cybersecurity approach fortifies the cloud, networks, endpoints, and emails against ransomware and threats. Swift threat detection and response reduce MTTD and MTTR. Real-time monitoring and Cloud Security Services enhance landscapes with IoT automation. Global Risk Management, Data Security, Application Security, and AI-driven services tackle evolving challenges, ensuring protection and operational continuity.
                                    </div>
                                </div>
                                <div class="btn-part mt-2">
                                    <a href="<?php echo main_url; ?>contact-us">Learn More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 mt--110 sm-mt-0 sm-mb-30">
                            <div class="pricing-wrap">
                                <div class="services-icon">
                                    <img class="mb-20 servic-offer" src="<?php echo main_url; ?>/assets/images/services/icons/style8/sec-card2.webp" style="height:50px" alt="Security Services Offerings" title="Security Services Offerings">

                                </div>
                                <div class="top-part m-0">
                                    <h6 class="headtxt">Cloud Security </h6>
                                </div>
                                <div class="middle-part p-3">
                                    <div class="desc">
                                        We provide a secure, adaptable platform tailored to sector-specific needs. Our enterprise-grade environment ensures seamless cloud migration, accelerates research, and maintains patient trust. We meet rigorous security and compliance standards, facilitating effortless integration of cloud-based applications for enhanced efficiency in existing operations.
                                    </div>
                                </div>
                                <div class="btn-part mt-3">
                                    <a href="<?php echo main_url; ?>contact-us">Learn More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 mt--140 sm-mt-0">
                            <div class="pricing-wrap">
                                <div class="services-icon">
                                    <img class="mb-20 servic-offer" src="<?php echo main_url; ?>/assets/images/services/icons/style8/sec-card1.webp" style="height:50px" alt="Security Services Offerings" title="Security Services Offerings">

                                </div>
                                <div class="top-part m-0">
                                    <h6 class="headtxt">OT/IoT Security </h6>
                                </div>
                                <div class="middle-part p-3">
                                    <div class="desc">
                                        In healthcare and life sciences, IoT security isn't optional; it's foundational. Unlike traditional models, IoT's unique demands require continuous, reliable 3GPP technologies. Shifting to proactive, automated security is essential for enterprises managing the growing IoT landscape, ensuring swift threat responses and cybersecurity risk mitigation.
                                    </div>
                                </div>
                                <div class="btn-part mt-3">
                                    <a href="<?php echo main_url; ?>contact-us">Learn More</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 mt--180 sm-mt-0">
                            <div class="pricing-wrap">
                                <div class="services-icon">
                                    <img class="mb-20 servic-offer" src="<?php echo main_url; ?>/assets/images/services/icons/style8/sec-card.webp" style="height:50px" alt="Security Services Offerings" title="Security Services Offerings">

                                </div>
                                <div class="top-part m-0">
                                    <h6 class="headtxt">IT Infrastructure Security </h6>
                                </div>
                                <div class="middle-part p-3">
                                    <div class="desc">
                                        Optimize operational capabilities, minimize risks, and maximize technology investments. With a comprehensive approach spanning on-premises, cloud, and IT infrastructure, we enhance agility and resilience. Our expert team ensures seamless, secure, and cost-effective multiplatform functionality, allowing industries to focus on core objectives.
                                    </div>
                                </div>
                                <div class="btn-part m-0">
                                    <a href="<?php echo main_url; ?>contact-us">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="video-sec">
                    <div class="container">
                        <div class="video-btn primary">
                            <a class="popup-videos" href="https://www.youtube.com/watch?v=YLN1Argi7ik">
                                <i class="fa fa-play"></i>
                            </a>
                        </div>
                    </div>
                </div> -->
        </div>
        <!-- Security Services Offerings End -->



        <!-- new solution end -->
        <!-- <div class="rs-video">
            <div class="container">
                <div class="sec-title3 text-center md-center mb-20 md-mb-20">
                    <h2 class="title">Why having an MSP is so valuable</h2>
                </div>
                <div class="video-wrap">
                    <div class="video-btn corporate text-center">
                        <div class="i-frame-container"> <iframe width="800" height="400" src="https://www.youtube.com/embed/OYorh8E6EAs?controls=0" title="Why having an MSP is so valuable" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </div>
                    </div>
                </div>
            </div>
        </div> -->

        <!-- About Us Section Start -->
        <div class="rs-about style10 pt-80 pb-100 md-pt-70 md-pb-70" data-aos="fade-up" data-aos-duration="700">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 pr-70 md-pr-15 md-mb-50">
                        <div class="sec-title4 mb-30">
                            <h2 class="title pb-20">About Us</h2>
                            <p class="margin-0">At NetServ, we enable healthcare and life sciences businesses to excel and keep up with the constantly evolving landscape by providing essential Managed IT Services. Our primary focus is guiding organizations to modernize their IT operations, secure their data centers and cloud environments, and implement advanced security measures. With over 100+ highly skilled technology professionals, we deliver unparalleled expertise and experience to each client. We aim to help businesses stay ahead of the curve and achieve their objectives through IT solutions.
                            <p>
                        </div><a href="<?php echo main_url; ?>/about" class="btn btn-primary btn-lg">Learn More</a>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-content">
                            <div class="images-part"> <img src="<?php echo main_url; ?>/assets/images/business-concept-with-team-close-up.webp" alt="About Us" loading="lazy"> </div>
                            <div class="rs-animations">
                                <div class="spinner dot"> <img class="scale" src="<?php echo main_url; ?>/assets/images/about/solutions/2.png" alt="About Us" loading="lazy"> </div>
                                <div class="spinner ball"> <img class="dance2" src="<?php echo main_url; ?>/assets/images/about/solutions/3.png" alt="About Us" loading="lazy"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rs-partner modify2 pt-100 md-pt-40">
                <div class="pb-4" align="center">
                    <h2>Technology Partners</h2>
                </div>
                <div class="container">
                    <div class="rs-carousel owl-carousel owl-carousel1" data-loop="true" data-items="5" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="3000" data-smart-speed="800" data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="3" data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="2" data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="5" data-lg-device="5" data-md-device-nav="false" data-md-device-dots="false">
                    <div class="partner-item pt-4"> <a><img src="<?php echo main_url; ?>/assets/images/techno/ciscologo.webp" loading="lazy" alt="Technology Partners" style="width:120px;"></a> </div>    
                    <div class="partner-item"> <a><img src="<?php echo main_url; ?>/assets/images/techno/crayon.webp" loading="lazy" alt="Technology Partners"></a> </div>
                        <div class="partner-item"> <a><img src="<?php echo main_url; ?>/assets/images/techno/Bigpanda-min-150x35.webp" loading="lazy" alt="Technology Partners"></a> </div>
                        <div class="partner-item"> <a><img src="<?php echo main_url; ?>/assets/images/techno/red-hat-150x35.webp" loading="lazy" alt="Technology Partners"></a> </div>
                        <div class="partner-item"> <a><img src="<?php echo main_url; ?>/assets/images/techno/stackify_logo.webp" loading="lazy" alt="Technology Partners"></a> </div>
                        <div class="partner-item"> <a><img src="<?php echo main_url; ?>/assets/images/techno/AWS-min-113x75.webp" loading="lazy" alt="Technology Partners"></a> </div>
                        <div class="partner-item pt-4"> <a><img src="<?php echo main_url; ?>/assets/images/techno/gogle-cloud.webp" loading="lazy" alt="Technology Partners"></a> </div>
                        <div class="partner-item "> <a><img src="<?php echo main_url; ?>/assets/images/techno/microsoft-logo.webp" loading="lazy" alt="Technology Partners"></a> </div>
                        <div class="partner-item"> <a><img src="<?php echo main_url; ?>/assets/images/techno/ocean.webp" loading="lazy" alt="Technology Partners"></a> </div>
                        <div class="partner-item"> <a><img src="<?php echo main_url; ?>/assets/images/techno/digiswitch-min-117x75.webp" loading="lazy" alt="Technology Partners"></a> </div>
                        <div class="partner-item"> <a><img src="<?php echo main_url; ?>/assets/images/techno/Lovecloud-min.webp" loading="lazy" alt="Technology Partners"></a> </div>
                        <div class="partner-item"> <a><img src="<?php echo main_url; ?>/assets/images/techno/nagios-150x37.webp" loading="lazy" alt="Technology Partners"></a> </div>
                        <div class="partner-item"> <a><img src="<?php echo main_url; ?>/assets/images/techno/opstree-150x30.webp" loading="lazy" alt="Technology Partners"></a> </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- About Us Section End -->

    </div>
    <!-- main-content div end -->


    <?php include 'footer.php'; ?>
    <div id="scrollUp"> <i class="fa fa-angle-up"></i></div>
    <?php include 'home_jslinks.php'; ?>
    <script src="<?php echo main_url; ?>/assets/js/jquery.mb.YTPlayer.min.js"></script>
    <script src="<?php echo main_url; ?>/assets/js/jquery.magnific-popup.min.js"></script>
    <script>
        var totop = $('#footer-id');
        totop.on('click', function() {
            $("html,body").animate({
                scrollTop: 0
            }, 500)
        });
    </script>
    <script>
        var scripts = ["<?php echo main_url; ?>/assets/js/slick.min.js", "<?php echo main_url; ?>/assets/js/imagesloaded.pkgd.min.js", "<?php echo main_url; ?>/assets/js/aos.js", "<?php echo main_url; ?>/assets/js/skill.bars.jquery.js", "<?php echo main_url; ?>/assets/js/waypoints.min.js", "<?php echo main_url; ?>/assets/inc/custom-slider/js/jquery.nivo.slider.js", "<?php echo main_url; ?>/assets/js/plugins.js", "<?php echo main_url; ?>/assets/js/main.js", ];
        scripts.forEach((script) => {
            let tag = document.createElement("script");
            tag.setAttribute("src", script);
            document.body.appendChild(tag);
        });
    </script>

    <script>
        // Add event handler for mouseover on the banner text
        $('.banner-text').on('mouseover', function() {
            $('.owl-carousel').trigger('stop.owl.autoplay');
            $(this).css('cursor', 'pointer');
        });

        // Add event handler for mouseleave on the banner text
        $('.banner-text').on('mouseleave', function() {
            $('.owl-carousel').trigger('play.owl.autoplay');
            $(this).css('cursor', '');
        });
    </script>
</body>

</html>