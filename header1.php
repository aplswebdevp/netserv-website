<header id="rs-header" class="rs-header">
    <div class="menu-area menu-sticky">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="logo-area"> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/logo.png" alt="Netserv-logo" title="netserv" width="" height="" /></a> </div>
                </div>
                <div class="col-lg-9 text-right">
                    <div class="rs-menu-area">
                        <div class="main-menu">
                            <div class="mobile-menu"> <a class="rs-menu-toggle"> <i class="fa fa-bars"></i> </a> </div>
                            <nav class="rs-menu pr-65">
                                <ul class="nav-menu">
                                    <li class="menu-item"><a href="<?php echo main_url; ?>">Home</a></li>
                                    <li class="menu-item-has-children"> <a href="#">About Us</a>
                                        <ul class="sub-menu">
                                            <li><a href="<?php echo main_url; ?>about">About Us</a></li>
                                            <!-- <li><a href="<?php echo main_url; ?>/leadership">Leadership</a></li> -->
                                            <li><a href="<?php echo main_url; ?>career">Careers</a></li>
                                        </ul>
                                    </li>
                                    <!-- uncomented code start -->
                                    <li class="menu-item-has-children"> <a href="#">Industries </a>

                                        <ul class="sub-menu">
                                            <li><a href="<?php echo main_url; ?>healthcare-solutions">Healthcare</a></li>
                                            <li><a href="<?php echo main_url; ?>life-science">Life Science</a></li>

                                            <li><a href="<?php echo main_url; ?>finance">Financial Services</a></li>
                                            <li><a href="<?php echo main_url; ?>government">Government </a></li>
                                            <!-- <li><a href="<?php echo main_url; ?>/coming-soon ">Manufacturing </a></li> -->


                                        </ul>

                                    </li>

                                    <li class="rs-mega-menu mega-rs menu-item-has-children" id="service_id"> <a href="#">Services</a>
                                        <ul class="mega-menu">
                                            <li class="mega-menu-container pb-3">
                                                <div class="mega-menu-innner">

                                                    <div class="single-megamenu">
                                                        <!-- <ul class="sub-menu border_bottom">
                                                            <li class="menu-title"><a href="<?php echo main_url; ?>/services/consulting-services/consulting">Consulting</a></li>
                                                            <li><a href="<?php echo main_url; ?>/services/consulting-services/plan-strategy">Plan and Strategy</a></li>
                                                            <li><a href="<?php echo main_url; ?>/services/consulting-services/advisory-services">Advisory Services</a></li>
                                                            <li><a href="<?php echo main_url; ?>/services/consulting-services/assessments-services">Assessments</a></li>
                                                            <li><a href="<?php echo main_url; ?>/services/consulting-services/workshop">Workshops</a></li>
                                                            <li class="pb-4"><a href="#"></a></li>
                                                        </ul> -->
                                                        <ul class="sub-menu last-sub-menu">
                                                            <li class="menu-title"><a href="<?php echo main_url; ?>services/managed-services/managed-services">Managed Services</a></li>
                                                            <!--  -->
                                                            <li><a href="<?php echo main_url; ?>services/managed-services/managed-cybersecurity-services">Managed CyberSecurity </a></li>
                                                            <li><a href="<?php echo main_url; ?>services/managed-services/managed-soc-and-noc-services">Managed SOC and NOC</a></li>
                                                            <li><a href="<?php echo main_url; ?>services/managed-services/managed-network-services">Managed Network and Communication</a></li>
                                                            <li><a href="<?php echo main_url; ?>services/managed-services/managed-data-backup-and-disaster-recovery-services">Managed Data Backup and Disaster Recovery</a></li>
                                                            <li><a href="<?php echo main_url; ?>services/managed-services/managed-cloud-services">Managed Cloud Services</a></li>


                                                            <!--  -->
                                                            <li><a href="<?php echo main_url; ?>services/managed-services/managed-infrastructure">Managed Infrastructure</a></li>
                                                            <li><a href="<?php echo main_url; ?>/services/managed-services/managed-daas-and-vdi">Managed VDI/DaaS</a></li>
                                                            <!-- <li><a href="<?php echo main_url; ?>/services/managed-services/application-management-services">Application Managed Services</a></li> -->
                                                            <!-- <li><a href="<?php echo main_url; ?>/services/managed-services/managed-security-services">Managed Security Services</a></li> -->
                                                            <!-- <li class=""><a href="<?php echo main_url; ?>/services/managed-services/full-stack-managed-services">Full Stack Managed AIOps</a></li> -->
                                                        </ul>

                                                    </div>
                                                    <div class="single-megamenu">


                                                        <ul class="sub-menu border_bottom">
                                                            <li class="menu-title"><a href="<?php echo main_url; ?>services/professional-services/professional-services">Professional Services</a></li>
                                                            <li><a href="<?php echo main_url; ?>services/professional-services/data-center">Data Center</a></li>
                                                            <li><a href="<?php echo main_url; ?>services/professional-services/cloud-modernization">Cloud Modernization</a></li>
                                                            <li><a href="<?php echo main_url; ?>services/professional-services/security">Security</a></li>
                                                            <li><a href="<?php echo main_url; ?>services/professional-services/network">Network</a></li>
                                                            <!-- <li class="pb-4"><a href="#"></a></li> -->
                                                        </ul>
                                                        <ul class="sub-menu">
                                                            <li class="menu-title"><a href="<?php echo main_url; ?>services/software-development/software-development">Software Development</a></li>
                                                            <li><a href="<?php echo main_url; ?>services/software-development/software-development#agile-software-development">Agile Software development</a></li>
                                                            <li><a href="<?php echo main_url; ?>services/software-development/software-development#application-customization-support">App Customization and Support</a></li>
                                                            <li><a href="<?php echo main_url; ?>services/software-development/software-development#api-integration">API Integration</a></li>
                                                            <li class="pb-4"><a href="#"></a></li>
                                                        </ul>
                                                        <!-- <ul class="sub-menu last-sub-menu border_bottom">
                                                            <li class="menu-title"><a href="<?php echo main_url; ?>/services/training/training">Training</a></li>
                                                            <li><a href="<?php echo main_url; ?>/services/training/training#cloud">Cloud</a></li>
                                                            <li><a href="<?php echo main_url; ?>/services/training/training#data_center">Data Center</a></li>
                                                            <li><a href="<?php echo main_url; ?>/services/training/training#enterprise_network">Enterprise Network</a></li>
                                                            <li><a href="#"></a></li>
                                                            <li class=""><a href="#"></a></li>
                                                        </ul> -->
                                                    </div>
                                                    <div class="single-megamenu">
                                                        <ul class="sub-menu border_bottom">
                                                            <li class="menu-title"><a href="<?php echo main_url; ?>services/consulting-services/consulting">Consulting</a></li>
                                                            <li><a href="<?php echo main_url; ?>services/consulting-services/plan-strategy">Plan and Strategy</a></li>
                                                            <li><a href="<?php echo main_url; ?>services/consulting-services/advisory-services">Consulting and Advisory</a></li>
                                                            <li><a href="<?php echo main_url; ?>services/consulting-services/assessments-services">Assessment</a></li>
                                                            <li><a href="<?php echo main_url; ?>services/consulting-services/workshop">Workshops</a></li>
                                                            <li class="pb-4"><a href="#"></a></li>
                                                        </ul>
                                                        <!-- <ul class="sub-menu">
                                                            <li class="menu-title"><a href="<?php echo main_url; ?>/services/services-startups/services-for-startups">Services for Startups</a></li>
                                                            <li><a href="<?php echo main_url; ?>/services/services-startups/support-services-startups">Support Services</a></li>
                                                            <li><a href="<?php echo main_url; ?>/services/software-development/software-development">Software Development</a></li>
                                                            <li><a href="<?php echo main_url; ?>/services/services-startups/partner-enablement">Partner Enablement</a></li>
                                                            <li class="pb-3"><a href="<?php echo main_url; ?>/services/services-startups/managed-services-startups">Managed Services</a></li>
                                                        </ul> -->
                                                    </div>

                                                </div>
                                            </li>
                                        </ul>
                                    </li>

                                    <!-- uncomented code end -->

                                    <li class="menu-item-has-children"> <a href="#">Resources</a>
                                        <ul class="sub-menu">
                                            <li><a href="<?php echo main_url; ?>/blog">Blog</a></li>
                                            <li><a href="<?php echo main_url; ?>use-cases/">Use Cases</a></li>
                                            <li><a href="<?php echo main_url; ?>/cybersecurity-news/">Cybersecurity News</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item" id="contact_us"><a href="<?php echo main_url; ?>contact-us">Contact Us</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="expand-btn-inner mt-3">
                            <ul>
                                <li> <a id="nav-expander" class="humburger nav-expander" href="#"> <span class="dot1"></span> <span class="dot2"></span> <span class="dot3"></span> <span class="dot4"></span> <span class="dot5"></span> <span class="dot6"></span> <span class="dot7"></span> <span class="dot8"></span> <span class="dot9"></span> </a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="right_menu_togle hidden-md">
        <div class="close-btn"> <span id="nav-close" class="humburger"> <span class="dot1"></span> <span class="dot2"></span> <span class="dot3"></span> <span class="dot4"></span> <span class="dot5"></span> <span class="dot6"></span> <span class="dot7"></span> <span class="dot8"></span> <span class="dot9"></span> </span>
        </div>
        <div class="canvas-logo"> <a href="<?php echo main_url; ?>"><img src="<?php echo main_url; ?>/assets/images/logo.png" alt="logo" /></a> </div>
        <div class="canvas-contact">
            <ul class="contact">
                <li><i class="flaticon-email"></i><a href="mailto:support@rstheme.com">info@ngnetserv.com
                    </a></li>
                <li> <i class="flaticon-location"></i>4580, Auto Mall Pkwy STE 121, Fremont,<br /> CA 94538, USA <br>Phone: 4083378781</li>
                <li> <i class="flaticon-location"></i>104 & 105, A-Wing, Shreenath Plaza, FC Road, Shivajinagar, Pune, Maharashtra 411005, India. <br>Phone: 020-48648865</li>
                <li> <i class="flaticon-location"></i>AEJ Building, #12 Q. Abeto St., Abeto -Mirasol, Mandurriao, Iloilo City, Philippines



                </li>
            </ul>
            <ul class="social">
                <li> <a href="https://twitter.com/netservio"><i class="fa fa-twitter"></i></a> </li>
                <li> <a href="https://www.linkedin.com/company/netserv-llc/"><i class="fa fa-linkedin"></i></a> </li>
                <li> <a href="https://www.youtube.com/channel/UCcTzZVwmHwFHK-YOYb51yOA/featured"><i class="fa fa-youtube-play"></i></a> </li>
                <!-- <li> <a><i class="fa fa-facebook"></i></a> </li> -->
            </ul>
        </div>
    </nav>
</header>