<!-- modernizr js -->
<script src="<?php echo main_url; ?>/assets/js/modernizr-2.8.3.min.js"></script>
<!-- jquery latest version -->
<script src="<?php echo main_url; ?>/assets/js/jquery.min.js"></script>
<!-- Bootstrap v4.4.1 js -->
<script src="<?php echo main_url; ?>/assets/js/bootstrap.min.js"></script>
<!-- Menu js -->
<script src="<?php echo main_url; ?>/assets/js/rsmenu-main.js"></script>
<!-- op nav js -->
<script src="<?php echo main_url; ?>/assets/js/jquery.nav.js"></script>
<!-- owl.carousel js -->
<script src="<?php echo main_url; ?>/assets/owl/owl.carousel.js"></script>
<script src="<?php echo main_url; ?>/assets/js/wow.min.js"></script>
<script src="<?php echo main_url; ?>/assets/js/jquery.counterup.min.js"></script>
<!-- counter top js -->
<script src="<?php echo main_url; ?>/assets/js/waypoints.min.js"></script>
<script src="<?php echo main_url; ?>/assets/js/form-validator.min.js"></script>
<script src="<?php echo main_url; ?>/assets/js/aos.js"></script>

<script>
    // scrollTop init	
    var totop = $('#footer-id');

    totop.on('click', function() {
        $("html,body").animate({
            scrollTop: 0
        }, 500)
    });
</script>