<!DOCTYPE html>
<html lang="zxx">
<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Contact-Us</title>
    <meta name="description" content="">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <link rel="stylesheet" type="text/css" href="assets/css/contact_us.css">
    <style type="text/css">
        .rs-breadcrumbs.bg-1 {
            /* background-image: url(assets/images/breadcrumbs/1.jpg); */
            background-image:linear-gradient(240deg, #ffffff 0%, rgb(234 235 237 / 60%) 50%, rgb(255 255 255 / 0%) 100%), url('assets/images/contactus.jfif');
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
        #redemtion_submit .list-unstyled {
            color: #dc3545;
        }

        #redemtion_submit button {
            background: #106eea;
            color: white;
            font-family: 'Poppins', sans-serif;
            border: 2px solid #106eea;
            border-radius: 5px;
            padding: 10px 20px 10px 20px;
            margin-bottom: 1rem;

        }

        #fname {
            background: #f1f6fc;
            width: 100%;
            padding: 10px 18px;
            border: none;
            outline: none;
            border-radius: 0;
            color: #363636;
            font-size: 16px;
            height: 48px;
        }

        .form-control {
            background: #f1f6fc;
            width: 100%;
            padding: 10px 18px;
            border: none;
            outline: none;
            border-radius: 0;
            color: #363636;
            font-size: 16px;
            height: 48px;
        }

        #phone_number {
            background: #f1f6fc;
            width: 100%;
            padding: 10px 18px;
            border: none;
            outline: none;
            border-radius: 0;
            color: #363636;
            font-size: 16px;
            height: 48px;
        }

        #msg_subject {
            background: #f1f6fc;
            width: 100%;
            padding: 10px 18px;
            border: none;
            outline: none;
            border-radius: 0;
            color: #363636;
            font-size: 16px;
            height: 48px;
        }

        #message {
            background: #f1f6fc;
            width: 100%;
            padding: 10px 18px;
            border: none;
            outline: none;
            border-radius: 0;
            color: #363636;
            font-size: 16px;

        }
        </style>
</head>
<body>
<!-- Preloader area start here -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--End preloader here -->
<!--Full width header Start-->
<div class="full-width-header header-style4">
    <!--Header Start-->
    <?php include 'header.php';?>
    <!--Header End-->
</div>
<!-- Main content Start -->
<!--<div class="main-content">-->
<!--    <!-- Breadcrumbs Section Start -->
<!--    <div class="rs-breadcrumbs bg-1">-->
<!--        <div class="container">-->
<!--            <div class="content-part text-center">-->
<!--                <h1 class="breadcrumbs-title mb-0">Contact</h1>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="rs-cta mt-5 mb-5">-->
<!--        <div class="Container-fluid">-->
<!--            <div class="content part pt-50 pb-50 md-pt-50 md-pb-50">-->
<!--                <div class="sec-title2">-->
<!--                    <p class="title title2 title4 pb-30 text-center sec-title2 p-heading text-heading" >-->
<!--                        If you have any questions about any of our services, Please complete the request form and one of our Technology experts will contact you shortly !-->
<!--                    </p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<div class="main-content">
    <!--test-->
    <div class="rs-contact style1 gray-bg pt-100 pb-100 md-pt-80 md-pb-80">
        <div class="container">
            <div class="white-bg">
                <div class="row">
                    <div class="col-lg-8 form-part">
                        <div class="sec-title mb-35 md-mb-30">
<!--                            <div class="sub-title text- primary">CONTACT US</div>-->
                            <h4 class="title mb-0">Please Enter Your Details</h4>
                        </div>
                        <div id="form-messages"></div>
                        <form id="redemtion_submit">
                            <div class="row contact-form">
                                <div class="col-lg-6 mb-20">
                                    <div class="form-group">
                                        <input type="text" name="first_name" id="first_name" class="form-control" required data-error="Please Enter Your First Name" placeholder="First name">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="col-lg-6 mb-20">
                                    <div class="form-group">
                                        <input type="text" name="last_name" id="last_name" class="form-control" required data-error="Please Enter Your Last Name" placeholder="Last name">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6 mb-16">
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" class="form-control" required data-error="Please Enter Your Email" placeholder="Email">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6 mb-20">
                                    <div class="form-group">
                                        <input type="text" name="job_title" id="job_title" class="form-control" required data-error="Please Enter Your Job Title" placeholder="Job title">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6 mb-16">
                                    <div class="form-group">
                                        <input type="text" name="phone_number" id="phone_number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" maxlength="10" minlength="10" class="form-control" placeholder="Phone Number" required data-error="Please Enter Your Phone Number" placeholder="Email">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6 mb-16">
                                    <div class="form-group">
                                        <select class="form-control" name="country" id="country" required data-error="Please Select Your Country" placeholder="Email">
                                            <option value="">Select country:</option><option value="Afghanistan">Afghanistan</option><option value="Åland Islands">Åland Islands</option><option value="Albania">Albania</option><option value="Algeria">Algeria</option><option value="American Samoa">American Samoa</option><option value="Andorra">Andorra</option><option value="Angola">Angola</option><option value="Anguilla">Anguilla</option><option value="Antarctica">Antarctica</option><option value="Antigua and Barbuda">Antigua and Barbuda</option><option value="Argentina">Argentina</option><option value="Armenia">Armenia</option><option value="Aruba">Aruba</option><option value="Australia">Australia</option><option value="Austria">Austria</option><option value="Azerbaijan">Azerbaijan</option><option value="Bahamas">Bahamas</option><option value="Bahrain">Bahrain</option><option value="Bangladesh">Bangladesh</option><option value="Barbados">Barbados</option><option value="Belarus">Belarus</option><option value="Belgium">Belgium</option><option value="Belize">Belize</option><option value="Benin">Benin</option><option value="Bermuda">Bermuda</option><option value="Bhutan">Bhutan</option><option value="Bolivia, Plurinational State of">Bolivia, Plurinational State of</option><option value="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option><option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option><option value="Botswana">Botswana</option><option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option><option value="British Indian Ocean Territory">British Indian Ocean Territory</option><option value="Brunei Darussalam">Brunei Darussalam</option><option value="Bulgaria">Bulgaria</option><option value="Burkina Faso">Burkina Faso</option><option value="Burundi">Burundi</option><option value="Cambodia">Cambodia</option><option value="Cameroon">Cameroon</option><option value="Canada">Canada</option><option value="Cape Verde">Cape Verde</option><option value="Cayman Islands">Cayman Islands</option><option value="Central African Republic">Central African Republic</option><option value="Chad">Chad</option><option value="Chile">Chile</option><option value="China">China</option><option value="Christmas Island">Christmas Island</option><option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option><option value="Colombia">Colombia</option><option value="COM">Comoros</option><option value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option><option value="Congo">Congo</option><option value="Cook Islands">Cook Islands</option><option value="Costa Rica">Costa Rica</option><option value="Côte d'Ivoire">Côte d'Ivoire</option><option value="Croatia">Croatia</option><option value="Cuba">Cuba</option><option value="Curaçao">Curaçao</option><option value="Cyprus">Cyprus</option><option value="Czech Republic">Czech Republic</option><option value="Denmark">Denmark</option><option value="Djibouti">Djibouti</option><option value="Dominica">Dominica</option><option value="Dominican Republic">Dominican Republic</option><option value="Ecuador">Ecuador</option><option value="Egypt">Egypt</option><option value="El Salvador">El Salvador</option><option value="Equatorial Guinea">Equatorial Guinea</option><option value="Eritrea">Eritrea</option><option value="Estonia">Estonia</option><option value="Ethiopia">Ethiopia</option><option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option><option value="Faroe Islands">Faroe Islands</option><option value="Fiji">Fiji</option><option value="Finland">Finland</option><option value="France">France</option><option value="French Guiana">French Guiana</option><option value="French Polynesia">French Polynesia</option><option value="French Southern Territories">French Southern Territories</option><option value="Gabon">Gabon</option><option value="Gambia">Gambia</option><option value="Georgia">Georgia</option><option value="Germany">Germany</option><option value="Ghana">Ghana</option><option value="Gibraltar">Gibraltar</option><option value="Greece">Greece</option><option value="Greenland">Greenland</option><option value="Grenada">Grenada</option><option value="Guadeloupe">Guadeloupe</option><option value="Guam">Guam</option><option value="Guatemala">Guatemala</option><option value="Guernsey">Guernsey</option><option value="Guinea">Guinea</option><option value="Guinea-Bissau">Guinea-Bissau</option><option value="Guyana">Guyana</option><option value="Haiti">Haiti</option><option value="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option><option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option><option value="Honduras">Honduras</option><option value="Hong Kong">Hong Kong</option><option value="Hungary">Hungary</option><option value="Iceland">Iceland</option><option value="India">India</option><option value="Indonesia">Indonesia</option><option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option><option value="Iraq">Iraq</option><option value="Ireland">Ireland</option><option value="Isle of Man">Isle of Man</option><option value="Israel">Israel</option><option value="Italy">Italy</option><option value="Jamaica">Jamaica</option><option value="Japan">Japan</option><option value="Jersey">Jersey</option><option value="Jordan">Jordan</option><option value="Kazakhstan">Kazakhstan</option><option value="Kenya">Kenya</option><option value="Kiribati">Kiribati</option><option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option><option value="Korea, Republic of">Korea, Republic of</option><option value="Kuwait">Kuwait</option><option value="Kyrgyzstan">Kyrgyzstan</option><option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option><option value="Latvia">Latvia</option><option value="Lebanon">Lebanon</option><option value="Lesotho">Lesotho</option><option value="Liberia">Liberia</option><option value="LBY">Libya</option><option value="Liechtenstein">Liechtenstein</option><option value="Lithuania">Lithuania</option><option value="Luxembourg">Luxembourg</option><option value="Macao">Macao</option><option value="Macedonia, the former Yugoslav Republic of">Macedonia, the former Yugoslav Republic of</option><option value="Madagascar">Madagascar</option><option value="Malawi">Malawi</option><option value="Malaysia">Malaysia</option><option value="Maldives">Maldives</option><option value="Mali">Mali</option><option value="Malta">Malta</option><option value="Marshall Islands">Marshall Islands</option><option value="Martinique">Martinique</option><option value="Mauritania">Mauritania</option><option value="Mauritius">Mauritius</option><option value="Mayotte">Mayotte</option><option value="Mexico">Mexico</option><option value="Micronesia, Federated States of">Micronesia, Federated States of</option><option value="Moldova, Republic of">Moldova, Republic of</option><option value="Monaco">Monaco</option><option value="Mongolia">Mongolia</option><option value="Montenegro">Montenegro</option><option value="Montserrat">Montserrat</option><option value="Morocco">Morocco</option><option value="Mozambique">Mozambique</option><option value="Myanmar">Myanmar</option><option value="Namibia">Namibia</option><option value="Nauru">Nauru</option><option value="Nepal">Nepal</option><option value="Netherlands">Netherlands</option><option value="New Caledonia">New Caledonia</option><option value="New Zealand">New Zealand</option><option value="Nicaragua">Nicaragua</option><option value="Niger">Niger</option><option value="Nigeria">Nigeria</option><option value="Niue">Niue</option><option value="Norfolk Island">Norfolk Island</option><option value="Northern Mariana Islands">Northern Mariana Islands</option><option value="Norway">Norway</option><option value="Oman">Oman</option><option value="Pakistan">Pakistan</option><option value="Palau">Palau</option><option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option><option value="Panama">Panama</option><option value="Papua New Guinea">Papua New Guinea</option><option value="Paraguay">Paraguay</option><option value="Peru">Peru</option><option value="Philippines">Philippines</option><option value="Pitcairn">Pitcairn</option><option value="Poland">Poland</option><option value="Portugal">Portugal</option><option value="Puerto Rico">Puerto Rico</option><option value="Qatar">Qatar</option><option value="Réunion">Réunion</option><option value="Romania">Romania</option><option value="Russian Federation">Russian Federation</option><option value="Rwanda">Rwanda</option><option value="Saint Barthélemy">Saint Barthélemy</option><option value="Saint Helena, Ascension and Tristan da Cunha">Saint Helena, Ascension and Tristan da Cunha</option><option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option><option value="Saint Lucia">Saint Lucia</option><option value="Saint Martin (French part)">Saint Martin (French part)</option><option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option><option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option><option value="Samoa">Samoa</option><option value="San Marino">San Marino</option><option value="Sao Tome and Principe">Sao Tome and Principe</option><option value="Saudi Arabia">Saudi Arabia</option><option value="Senegal">Senegal</option><option value="Serbia">Serbia</option><option value="Seychelles">Seychelles</option><option value="Sierra Leone">Sierra Leone</option><option value="Singapore">Singapore</option><option value="Sint Maarten (Dutch part)">Sint Maarten (Dutch part)</option><option value="Slovakia">Slovakia</option><option value="Slovenia">Slovenia</option><option value="Solomon Islands">Solomon Islands</option><option value="Somalia">Somalia</option><option value="South Africa">South Africa</option><option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option><option value="South Sudan">South Sudan</option><option value="Spain">Spain</option><option value="Sri Lanka">Sri Lanka</option><option value="Sudan">Sudan</option><option value="Suriname">Suriname</option><option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option><option value="Swaziland">Swaziland</option><option value="Sweden">Sweden</option><option value="Switzerland">Switzerland</option><option value="Syrian Arab Republic">Syrian Arab Republic</option><option value="Taiwan, Province of China">Taiwan, Province of China</option><option value="Tajikistan">Tajikistan</option><option value="Tanzania, United Republic of">Tanzania, United Republic of</option><option value="Thailand">Thailand</option><option value="Timor-Leste">Timor-Leste</option><option value="Togo">Togo</option><option value="Tokelau">Tokelau</option><option value="Tonga">Tonga</option><option value="Trinidad and Tobago">Trinidad and Tobago</option><option value="Tunisia">Tunisia</option><option value="Turkey">Turkey</option><option value="Turkmenistan">Turkmenistan</option><option value="Turks and Caicos Islands">Turks and Caicos Islands</option><option value="Tuvalu">Tuvalu</option><option value="Uganda">Uganda</option><option value="Ukraine">Ukraine</option><option value="United Arab Emirates">United Arab Emirates</option><option value="United Kingdom">United Kingdom</option><option value="United States">United States</option><option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option><option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option><option value="Venezuela, Bolivarian Republic of">Venezuela, Bolivarian Republic of</option><option value="Viet Nam">Viet Nam</option><option value="Virgin Islands, British">Virgin Islands, British</option><option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option><option value="Wallis and Futuna">Wallis and Futuna</option><option value="Western Sahara">Western Sahara</option><option value="Yemen">Yemen</option><option value="Zambia">Zambia</option><option value="Zimbabwe">Zimbabwe</option>
                                        </select>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 mb-16">
                                    <div class="form-group">
                                        <textarea name="comments" class="form-control" id="comments" cols="30" rows="2" required data-error="Write your comments" placeholder="Comments"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 mb-16">
                                    <div class="form-group pl-4">
                                        <input class="form-check-input" type="checkbox" id="agree_terms" name="agree_terms" value="1" required data-error="Please accept all terms and conditions.">
                                                I'd like to receive more information about Offer; I understand and agree to the <a href="<?php echo main_url;?>/privacy-policy" target="_blank">privacy policy</a>.
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="g-recaptcha" data-sitekey="6LfF3UcdAAAAAPyOeyqE1p7_sF6vAHUmWTjmHf2I"></div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <button type="submit" class="default-btn btn-bg-two border-radius-50">
                                        Submit <i class='bx bx-chevron-right'></i>
                                    </button>
                                    <div id="msgSubmit" class="h3 text-center hidden"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-4 pl-0 md-pl-pr-15 md-order-first">
                        <div class="contact-info" style="background-image: url('assets/images/request-quote.jpg');background-size: cover;background-position: center;">
                            <div class="contact_txt_center">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--#test-->

</div>
<!-- Main content End -->
<!-- Main content End -->
<!-- Footer Start -->
<?php include 'footer.php';?>
<!-- Footer End -->
<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->
<?php include 'service_jslinks.php'; ?>
<script src="<?php echo main_url; ?>/assets/js/redemtion-form-script.js"></script>
<!--<script>-->
<!--    !(function (e) {-->
<!--        "use strict";-->
<!--        var s = e("#redemtion_submit"),-->
<!--            t = e("#form-messages");-->
<!--        e(s).submit(function (a) {-->
<!--            a.preventDefault();-->
<!--            var r = e(s).serialize();-->
<!--            e.ajax({ type: "POST", url: e(s).attr("action"), data: r })-->
<!--                .done(function (s) {-->
<!--                    e(t).removeClass("error"), e(t).addClass("success"), e(t).text(s), e("#first_name, #last_name, #email, #job_title, #phone_number, #country, #comments, #agree_terms").val("");-->
<!--                })-->
<!--                .fail(function (s) {-->
<!--                    e(t).removeClass("success"), e(t).addClass("error"), "" !== s.responseText ? e(t).text(s.responseText) : e(t).text("Oops! An error occured and your message could not be sent.");-->
<!--                });-->
<!--        });-->
<!--    })(jQuery);-->
<!---->
<!--</script>-->
</body>
</html>