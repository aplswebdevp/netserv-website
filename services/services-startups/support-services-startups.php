<!DOCTYPE html>
<html lang="en">

<head>
   <!-- meta tag -->
   <meta charset="utf-8">
   <title>NetServ - Support-Services-Startups</title>
   <meta name="description" content="No matter where you are in your startup journey, NetServ can help you smart-sourcing your support teams and scale up or down based on your requirements.">
   <meta name="keywords" content="customer support, support team, it support services, contact support, teams support, help center, service support, support service, support level, customer support services, help support, managed it support, help and support, it support and services, service help, startup support, service and support, it managed support services, it support manager, technical customer service, contact customer support, contact help, customer support center, it managed support, managed it support services, it support levels, it services and support, it support team, support agreement, customer help, it support software, support as a service, managed support services, it customer support, it support managed services, it service support, customer service and support, it support center, it support and managed services, support services it, managed it services and support, managed services it support, customer service support team, customer support technical, customer support and technical support, email contact support, email for it support, email for support">
   <!-- responsive tag -->
   <meta http-equiv="x-ua-compatible" content="ie=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- favicon -->
   <link rel="apple-touch-icon" href="">
   <link rel="canonical" href="https://www.ngnetserv.com/services/services-startups/support-services-startups" />
    <?php include '../../service_csslinks.php'; ?>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo main_url; ?>/assets/images/favicon.png">
   <link rel="stylesheet" href="<?php echo main_url; ?>/assets/css/support-services.css">
   <script type='application/ld+json'> 
{
  "@context": "http://www.schema.org",
  "@type": "WebSite",
  "name": "NetSev",
  "url": "http://www.ngnetserv.com/"
}
 </script>
</head>
<style type="text/css">
   .rs-breadcrumbs.bg-3 {
      background-image: linear-gradient(90deg, #fff 0, rgb(234 235 237 / 60%) 50%, rgb(255 255 255 / 0) 100%), url("<?php echo main_url; ?>/assets/images/support-services/download-1.jpg");
      background-size: cover;
      background-position: 10%
   }
</style>

<body class="home-eight">
   <!-- Preloader area start here -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
   <!--End preloader here -->
   <!--Full width header Start-->
   <div class="full-width-header header-style4">
      <!--header-->
      <?php include '../../header.php'; ?>
      <!--Header End-->
   </div>
   <!--Full width header End-->
   <!-- Main content Start -->
   <div class="main-content">
      <!-- Breadcrumbs Section Start -->
      <div class="rs-breadcrumbs bg-3">
         <div class="container">
            <div class="content-part text-center">
               <p><b>Services-<a href="<?php echo main_url; ?>/services/services-startups/services-for-startups" class="text-dark">Services for Startups</a></b></p>
               <!-- <p><b></b></p> -->
               <h1 class="breadcrumbs-title  mb-0">Support Services for Startups
               </h1>
            </div>
         </div>
      </div>
      <!-- Breadcrumbs Section End -->
      <!--start  updated section -->
      <div class="rs-solutions style1 white-bg  modify2 pt-110 pb-84 md-pt-80 md-pb-64">
         <div class="container">
            <div class="sec-title style2 mb-60 md-mb-50 sm-mb-42">
               <div class="first-half">
                  <div class="sec-title mb-24">
                     <h5 class="primary txt_clr title mt-3 support-font"><strong> Would you like to smart source your technical support?
                        </strong>
                     </h5>
                     <p style="font-size: 17px;">No matter where you are in your startup journey, NetServ can help you smart-sourcing your support teams and scale up or down based on your requirements.
                        <br><br>
                        With this service, your team can focus on the innovation and growth needed to meet your growth and business goals. With our white-label support services, you can have a dedicated team to support your technology and solutions.
                     </p>
                  </div>
               </div>
               <div class="last-half">
                  <div class="image-part">
                     <img src="<?php echo main_url; ?>/assets/images/support-services/support-card-1.png" alt="support-services" title="support-services">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--end updated section -->


      <!-- Pricing Plan Section Start -->
      <div class="rs-pricing style1">
         <div class="top-part bg10 pt-50 pb-200 md-pt-73 sm-pb-100">
            <div class="container">
               <div class="sec-title">
                  <h3 class="title white-color  mb-0 text-center support-sub-heading ">Following are some of our support services for startups<br>offerings:
                  </h3>
               </div>
            </div>
         </div>
         <div class="pb-100 md-pb-80 support_service_startups">
            <div class="container">
               <div class="row gutter-20">
                  <div class="col-lg-4 col-md-12 mt--60 sm-mb-30">
                     <div class="pricing-wrap price-heading">
                        <div class="top-part">
                           <div class="price text-uppercase"><span></span>Silver</div>
<!--                           <div class="price-heading "><span></span>(BMSAP)</div>-->
                           <!-- <span class="priod">Per Month</span> -->
                        </div>
                        <ul class="listing-style2 card-style">
                           <li>8*5 coverage(email)</li>
                           <li>Team(shared L1/L2 support team</li>
                           <li>Service Level Agreement(Initial Response P1-2hr, P2-8hrs, P3 -12hrs)</li>
                           <li>White -label services</li>
                           <li>RMA processing</li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-12 mt--120 sm-mt-0 sm-mb-30">
                     <div class="pricing-wrap price-heading">
                        <div class="top-part">
                           <div class="price mb-3"><span></span>Gold</div>
<!--                           <div class="price price-heading"><span></span>(SMSAP)</div>-->
                        </div>
                        <ul class="listing-style2 card-style mb-20">
                           <li> 24*7 Coverage(Phone, email or IM)</li>
                           <li>24*7 Monitoring</li>
                           <li>Team(Dedicated L1/L2 support team, senior technical staff member during onboarding)</li>
                           <li>Service level agreement(initial response P1-1hr,P2-6hrs,P3-12hrs)</li>
                           <li>Proactive feedback loop to engineering</li>
                           <li>KPIs-(CSAT,SLA,case volume reduction)</li>
                           <li>White-label services</li>
                           <li>RMA processing</li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-4 col-md-12 mt--180 sm-mt-0">
                     <div class="pricing-wrap price-heading">
                        <div class="top-part">
                           <div class="price"><span></span>Platinum</div>
<!--                           <div class="price-heading"><span></span>(GMSAP)</div>-->
                        </div>
                        <ul class="listing-style2 card-style mb-20">
                           <li>24*7 Coverage(Phone, Email and IM)</li>
                           <li>24*7 Monitoring</li>
                           <li>Team(Dedicated L1/L2 Support team, Customer Relationship Manager, Senior Technical Staff member during onboarding)</li>
                           <li>Service level agreement(Initial Response P1-30mins,P2-2hrs,P3-8hrs)</li>
                           <li>Proactive feedback loop to engineering</li>
                           <li>Customer issue recreate (lab Environment)</li>
                           <li>Quarterly business review</li>
                           <li>KPI-(CSAT,SLA,Case volume reduction)</li>
                           <li>RMA processing</li>
                           <li>White-label service</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Pricing Plan Section End -->
      <!-- features-benefits-section-starts -->
      <div class="rs-solutions pb-100 md-pb-80">
         <div class="container">
            <div class="row y-middle">
               <div class="col-lg-12">
                  <div class="sec-title mb-24">
                     <h3 class="title support-sub-heading text-center mb-30">Some features and benefits of NetServ's<br>Smart-sourcing technical support
                     </h3>
                     <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                           <ul class="listing-style2 mb-33">
                              <li>Service desk, incident & change management
                              </li>
                              <li>24x7 intelligent monitoring</li>
                              <li>24x7 Operational support</li>
                              <li> White-label technical support option</li>
                           </ul>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                           <ul class="listing-style2 mb-33">
                              <li> Dedicated L1/L2 support team option</li>
                              <li>Proactive feedback loop to engineering</li>
                              <li>KPIs – CSAT, SLAs & Case volume reduction</li>
                              <li> Engineering reviews and QBR</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- features-benefits-section-Ends-->
      <div class="rs-contact style1 pt-100 gray-bg pb-100 md-pt-80 md-pb-80">
         <div class="container">
            <div class="white-bg">
               <div class="row">
                  <div class="col-lg-8 form-part">
                     <div class="sec-title mb-35 md-mb-30">
                        <div class="sub-title primary">CONTACT US</div>
                        <h2 class="title mb-0">Get In Touch</h2>
                     </div>
                     <div id="form-messages"></div>
                     <?php include '../../contact.php'; ?>
                  </div>
                  <div class="col-lg-4 pl-0 md-pl-pr-15 md-order-first">
                     <div class="contact-info">
                        <h3 class="title contact_txt_center" style="line-height: 44px;">
                           If you have any questions about our Services for Startups, please complete the request form and one of our technical expert will contact you shortly !
                        </h3>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Contact Section End -->
   </div>
   </div>
   <!-- Main content End -->
   <!-- Footer Start -->
   <?php include '../../footer.php'; ?>
   <!-- Footer End -->
   <!-- start scrollUp  -->
   <div id="scrollUp">
      <i class="fa fa-angle-up"></i>
   </div>
   <!-- End scrollUp  -->
   <?php include '../../service_jslinks.php'; ?>
</body>

</html>