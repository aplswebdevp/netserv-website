<!DOCTYPE html>
<html lang="en">

<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Agile Software Development</title>
    <meta name="description" content="Continuous Integration, Continuous Deployment (CI/CD) is the process of automating your code builds from development to production environments, helping you get to market faster than your competition without sacrificing quality.">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/services/software-development/software-development#agile-software-development" />
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include '../../service_csslinks.php'; ?>
    <link rel="stylesheet" href="<?php echo main_url; ?>/assets/css/assessment_services.css">
    <script type='application/ld+json'> 
{
  "@context": "http://www.schema.org",
  "@type": "WebSite",
  "name": "NetSev",
  "url": "http://www.ngnetserv.com/"
}
 </script>
</head>
<!-- Internal-css-starts -->
<style type="text/css">
    .rs-breadcrumbs.bg-3 {
        background-image: linear-gradient(90deg, #ffffff 0%, rgb(234 235 237 / 60%) 50%, rgb(255 255 255 / 50%) 100%), url(<?php echo main_url; ?>/assets/images/services/software-development/agile/agile-bg-free2.jpg);
        background-size: cover;
        background-position: 10%;
    }

    .rs-services.style19 .services-item .services-wrap .icon-part {
        line-height: 55px;
    }

    .rs-services.style19 .services-item .services-wrap .services-content .services-title .title a:hover {
        color: #106eea;
    }

    .rs-services.style19 .services-item .services-wrap .icon-part.purple-bg:before {
        border: 1px solid #106eea;
    }

    .rs-services.style19 .services-item .services-wrap .icon-part.purple-bg {
        background: #106eea;
    }

    .rs-services.style15 .addon-services .services-icon img {
        max-width: unset;
        width: 150px;
        height: 150px;
    }

    .col-lg-6 {
        border-bottom: 1px solid white;
    }

    @media only screen and (max-width: 480px) {
        .sec-title .desc {
            display: block;
        }
    }
</style>
<!-- Internal-css-Ends -->

<body class="home-eight">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <!-- Preloader area start here -->

    <!--End preloader here -->
    <!--Full width header Start-->
    <div class="full-width-header header-style4">
        <!--header-->
        <?php include '../../header.php'; ?>
        <!--Header End-->
    </div>
    <!--Full width header End-->
    <!-- Main content Start -->
    <div class="main-content">
        <!-- Breadcrumbs Section Start -->
        <div class="rs-breadcrumbs bg-3">
            <div class="container">
                <div class="content-part text-center">
                    <p><b>Services - Software Development</b></p>
                    <h1 class="breadcrumbs-title  mb-0">Agile Software Development</h1>
                </div>
            </div>
        </div>
        <!-- Breadcrumbs Section End -->
        <!-- 1st intro section starts -->
        <div class="rs-solutions pb-100 mt-100 md-pb-80">
            <div class="container">
                <div class="row y-middle">
                    <div class="col-lg-6">
                        <div class="sec-title mb-24">
                            <div class="desc">Continuous integration, continuous deployment (CI/CD) is the process of automating your code builds from development to production environments, helping you get to market faster than your competition without sacrificing quality.</div>
                        </div>
                        <div class="sec-middle row">
                            <div class="left-list  col-sm-12 col-md-6">
                                <ul class="listing-style regular">
                                    <li><span class="blue-color">Automate SSH into a workload instance to install multiple packages</span></li>
                                    <li><span class="blue-color">Create workload instances without logging in to the portal</span></li>
                                </ul>
                            </div>
                            <div class="right-list col-sm-12 col-md-6">
                                <ul class="listing-style regular">
                                    <li><span class="blue-color">Run automated tasks on more than one workload instance simultaneously</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 md-order-first md-mb-30">
                        <div class="image-part">
                            <img src="<?php echo main_url; ?>/assets/images/services/software-development/agile/card-4.jpg" alt="Services - Software Development">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- 1st intro section ends  -->
        <!--6 cards starts -->
        <div class="rs-services style15 pt-100 md-pt-70">
            <div class="container">
                <div class="sec-title text-center mb-47 md-mb-42">
                    <h2 class="title mb-0">Our <span>Netserv team uses following tools for <br> <span class="txt_clr">CI/CD pipelines</span></span></h2>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 sm-mb-20 mt-4">
                        <a href="https://www.jenkins.io/">
                            <div class="addon-services">
                                <div class="services-icon">
                                    <img src="<?php echo main_url; ?>/assets/images/services/software-development/agile/pipelines_1.jpg" alt="images">
                                </div>
                                <div class="services-text">
                                    <h2 class="title">
                                    Jenkins
                                    </h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4 sm-mb-20 mt-4">
                        <a href="https://azure.microsoft.com/en-us/services/devops/pipelines/">
                            <div class="addon-services">
                                <div class="services-icon">
                                    <img src="<?php echo main_url; ?>/assets/images/services/software-development/agile/pipelines_2.jpg" alt="images">
                                </div>
                                <div class="services-text">
                                    <h2 class="title">Azure pipelines</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4 mt-4">
                        <a href="https://cloud.google.com/build">
                            <div class="addon-services">
                                <div class="services-icon">
                                    <img src="<?php echo main_url; ?>/assets/images/services/software-development/agile/pipelines_3.jpg" alt="images">
                                </div>
                                <div class="services-text">
                                    <h2 class="title">Cloud build</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4 sm-mb-20 mt-4">
                        <a href="https://travis-ci.org/">
                            <div class="addon-services">
                                <div class="services-icon">
                                    <img src="<?php echo main_url; ?>/assets/images/services/software-development/agile/pipelines_4.jpg" alt="images">
                                </div>
                                <div class="services-text">
                                    <h2 class="title">Trivis CI</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4 sm-mb-20 mt-4">
                        <a href="https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/">
                            <div class="addon-services">
                                <div class="services-icon">
                                    <img src="<?php echo main_url; ?>/assets/images/services/software-development/agile/pipelines_5.jpg" alt="images">
                                </div>
                                <div class="services-text">
                                    <h2 class="title">GitLab</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-4 mt-4">
                        <a href="https://circleci.com/">
                            <div class="addon-services">
                                <div class="services-icon">
                                    <img src="<?php echo main_url; ?>/assets/images/services/software-development/agile/pipelines_6.jpg" alt="images">
                                </div>
                                <div class="services-text">
                                    <h2 class="title"> circle CI</h2>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!--6 cards ends -->
        <!-- Conatct-form-starts -->
        <div class="rs-contact style1 gray-bg pt-100 pb-100 md-pt-80 md-pb-80">
            <div class="container">
                <div class="white-bg">
                    <div class="row">
                        <div class="col-lg-8 form-part">
                            <div class="sec-title mb-35 md-mb-30">
                                <div class="sub-title primary">CONTACT US</div>
                                <h2 class="title mb-0">Get In Touch</h2>
                            </div>
                            <div id="form-messages"></div>
                            <form id="contact-form" class="contact-form" method="post" action="https://rstheme.com/products/html/reobiz/mailer.php">
                                <div class="row">
                                    <div class="col-md-6 mb-30">
                                        <div class="common-control">
                                            <input type="text" name="name" placeholder="First Name" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-30">
                                        <div class="common-control">
                                            <input type="email" name="email" placeholder="Last Name" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-30">
                                        <div class="common-control">
                                            <input type="text" name="phone" placeholder="Email" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-30">
                                        <div class="common-control">
                                            <input type="text" name="website" placeholder="Mobile" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-30">
                                        <div class="common-control">
                                            <textarea name="message" placeholder="Message" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="submit-btn">
                                            <button type="submit" class="readon">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-4 pl-0 md-pl-pr-15 md-order-first">
                            <div class="contact-info">
                                <h3 class="title text-center" style="line-height: 44px;">What more? Our Agile Software Development can offer you various other benefits to strengthen your organization’s Agile-Software-Development Infrastructure. Want to try out ?</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Conatct-form-Ends-->
    </div>
    <!-- Main content End -->
    <!-- Footer Start -->
    <?php include '../../footer.php'; ?>
    <!-- Footer End -->
    <!-- start scrollUp  -->
    <div id="scrollUp">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- End scrollUp  -->
    <?php include '../../service_jslinks.php'; ?>
</body>

</html>