<!DOCTYPE html>
<html lang="en">

<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Simplify And Secure IT Infrastructure.</title>
    <meta name="description" content="Our innovative Managed AIOps solution allows you to improve your operational efficiencies by smart event correlation, rapid root cause analysis, and automating manual aspects of IT workflows.">
    <meta name="keywords" content="managed service, managed service provider, managed it services, application management services, managed security services, managed it support, managed it service provider, managed infrastructure services, managed services model, it managed support, support management, managed infrastructure, managed support services, managed application, managed services operations, security managed, a managed service provider, app for portfolio management, app management service, app portfolio management,">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/coming-soon" />
    <?php include './service_csslinks.php'; ?>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo main_url; ?>/assets/images/favicon.png">
    <link rel="stylesheet" type="text/css" href="<?php echo main_url; ?>assets/css/comming-soon.css">
    <script type='application/ld+json'>
        {
            "@context": "http://www.schema.org",
            "@type": "WebSite",
            "name": "NetSev",
            "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .rs-breadcrumbs.bg-3 {
        background-image: linear-gradient(90deg, #ffffff 0%, rgb(234 235 237 / 60%) 50%, rgb(255 255 255 / 0%) 100%), url(<?php echo main_url; ?>/assets/images/services/managed-services/Healthcare.png);
        background-size: cover;
        background-position: 10%;
    }

    .managed-service-img {
        width: 35%;
        margin: 0 auto;
        display: block;
    }

    .desc_txt {
        height: 800px;
    }
</style>

<body class="home-eight">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- Preloader area start here -->
    <!--End preloader here -->
    <!--Full width header Start-->
    <div class="full-width-header header-style4">
        <!--header-->
        <?php include 'header.php'; ?>
        <!--Header End-->
    </div>
    <!--Full width header End-->
    <!-- Main content Start -->
    <div class="bgimg text-center mt-100 mb-200">
        <!-- <div class="topleft">
        </div> -->
        <div class="middle ">
            <h1 class="txt">COMING SOON</h1>
            <small>The page you are looking is under construction.</small>
            <hr>
            <br>
            <p id="demo" style=""></p>
        </div>
        <!-- <div class="bottomleft">
        </div> -->
    </div>
    <!-- Main content End -->
    <!-- Footer Start -->
    <?php include 'footer.php'; ?>
    <!-- Footer End -->
    <!-- start scrollUp  -->
    <div id="scrollUp">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- End scrollUp  -->
    <?php include 'service_jslinks.php'; ?>

    <script>
        // Set the date we're counting down to
        var countDownDate = new Date("Jan 5, 2022 15:37:25").getTime();

        // Update the count down every 1 second
        var countdownfunction = setInterval(function() {

            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Output the result in an element with id="demo"
            document.getElementById("demo").innerHTML = days + "d " + hours + "h " +
                minutes + "m " + seconds + "s ";

            // If the count down is over, write some text
            if (distance < 0) {
                clearInterval(countdownfunction);
                document.getElementById("demo").innerHTML = "EXPIRED";
            }
        }, 1000);
    </script>
</body>

</html>