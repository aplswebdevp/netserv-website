<!DOCTYPE html>
<html lang="en">

<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Finance-SME </title>
    <meta name="description" content="Analyze business requirements to develop technical network solutions and standard frameworks.">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/finance-sme" />
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'>
        {
            "@context": "http://www.schema.org",
            "@type": "WebSite",
            "name": "NetSev",
            "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .bg4 {
        background-image: url(assets/images/bg/bg4.png)
    }

    .rs-collaboration.style1 .img-part img {
        position: relative;
        bottom: 0
    }

    .rs-services.style22 .service-wrap .icon-part img {
        width: 53px;
        height: 53px;
        max-width: unset
    }

    ul.listing-style li {
        position: relative;
        padding-left: 30px;
        line-height: 34px;
        font-weight: 500;
        font-size: 14px
    }

    ul.listing-style.regular2 li {
        font-weight: 400;
        margin-bottom: 0
    }

    .rs-about.style10 .accordion .card .card-body {
        background: #fff
    }
</style>

<body class="home-eight">
    <!-- Preloader area start here -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!--End preloader here -->
    <!--Full width header Start-->
    <div class="full-width-header header-style4">
        <!--header-->
        <?php include 'header.php'; ?>
        <!--Header End-->
    </div>
    <!--Full width header End-->
    <!-- Main content Start -->
    <div class="main-content">
        <!-- Services Section Start -->
        <div class="rs-pricing style1">
            <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
                <div class="container">
                    <div class="sec-title">
                        <!-- <div class="sub-title white-color">Pricing Plan</div> -->
                        <h1 class="title white-color mb-0 text-center" style="font-size: 36px;">Finance SME </h1>
                        <div class="sub-title text-center white-color">Budapest | 5+ Years experience</div>
                    </div>
                </div>
            </div>
        </div>
        <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-left">
                             <p> <span class="txt_clr"><strong>Designation</strong> : </span> Finance SME</p>
                        <p> <span class="txt_clr"><strong>Salary </strong> : </span>  Best in Industry </p>
                        <p> <span class="txt_clr"><strong>Experience </strong> : </span>  5+ years </p>
                        <p> <span class="txt_clr"><strong>Joining  </strong> : </span>Immediate/15 days </p>
                        <p> <span class="txt_clr"><strong>Location  </strong> : </span> Budapest  </p>
                            <p> <span class="txt_clr"><strong>Roles and Responsibilities</strong></span>
                            </p>

                            <ol>
                                <li>Ensures business processes, administration, and financial management.</li>
                                <li>Maintains accounting system.</li>
                                <li>Leads planning and forecasting activities with business partners to achieve business and company goals.</li>
                                <li>Reviews financial reports.</li>
                                <li>Work with AP to resolve invoice issues.</li>
                                <li>Prepares financial forecasts.</li>
                                <li>Monitors financial details to ensure legal compliance.</li>
                                <li>Do license management get renewals done on time after updating the scope.</li>
                                <li>Analyses revenue, expenses, cash flows, and balance sheets.</li>
                                <li>Assists management to make financial decisions.</li>
                                <li>Supervises employees.</li>
                                <li>Investigates means to improve profitability.</li>
                                <li>Reviews and processes payments of the company.</li>
                                <li>Maintains an accurate filing and record-keeping system for all financial statements and company documents.</li>
                                <li>Participates in the execution of changes to procedures, policies, and systems to facilitate expansion, compliance, and scaling of the business.</li>




                            </ol>
                            </p>
                            <p><span class="txt_clr"><strong> Skills</strong> </span>
                            <ol>
                                <li>Attention to detail.</li>
                                <li>Strong quantitative skills.</li>
                                <li>Working knowledge of GAAP principles.</li>
                                <li>Ability to manage multiple projects.</li>
                                <li>Understanding of confidentiality.</li>
                                <li>Adept at analysing information.</li>
                                <li>Strong negotiation skills.</li>
                                <li>Financial modeling skills.</li>

                            </ol>
                            </p>
                            <p><span class="txt_clr"><strong> Education and Experience Requirements</strong> </span>
                            <ol>
                                <li>BA or BS in economics, finance, accounting, economics, or related field.</li>
                                <li>5-7 years managerial experience.</li>

                            </ol>
                            </p>
                        </div>
                        <div class="btn-part">
                            <a href="mailto:hr@ngnetserv.com" class="btn btn-primary">Apply</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main content End -->
    <!-- Footer Start -->
    <?php include 'footer.php'; ?>
    <!-- Footer End -->
    <!-- start scrollUp  -->
    <div id="scrollUp">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- End scrollUp  -->
    <?php include 'service_jslinks.php'; ?>
</body>

</html>