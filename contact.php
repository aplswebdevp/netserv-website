<style>
    #contactForm .list-unstyled {
        color: #dc3545;
    }

    #contactForm button {
        background: #106eea;
        color: white;
        font-family: 'Poppins', sans-serif;
        border: 2px solid #106eea;
        border-radius: 5px;
        padding: 10px 20px 10px 20px;
        margin-bottom: 1rem;

    }
    

    #fname {
        background: #f1f6fc;
        width: 100%;
        padding: 10px 18px;
        border: none;
        outline: none;
        border-radius: 0;
        color: #363636;
        font-size: 16px;
        height: 48px;
    }

    #email {
        background: #f1f6fc;
        width: 100%;
        padding: 10px 18px;
        border: none;
        outline: none;
        border-radius: 0;
        color: #363636;
        font-size: 16px;
        height: 48px;
    }

    #phone_number {
        background: #f1f6fc;
        width: 100%;
        padding: 10px 18px;
        border: none;
        outline: none;
        border-radius: 0;
        color: #363636;
        font-size: 16px;
        height: 48px;
    }

    #msg_subject {
        background: #f1f6fc;
        width: 100%;
        padding: 10px 18px;
        border: none;
        outline: none;
        border-radius: 0;
        color: #363636;
        font-size: 16px;
        height: 48px;
    }

    #message {
        background: #f1f6fc;
        width: 100%;
        padding: 10px 18px;
        border: none;
        outline: none;
        border-radius: 0;
        color: #363636;
        font-size: 16px;

    }
</style>
<form id="contactForm" >
    <div class="row contact-form">
        <div class="col-lg-6 mb-20">
            <div class="form-group">
                <input type="text" name="fname" id="fname" class="form-control" required data-error="Please Enter Your Name" placeholder="Name">
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="col-lg-6 mb-16">
            <div class="form-group">
                <input type="email" name="email" id="email" class="form-control" required data-error="Please Enter Your Email" placeholder="Email">
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="col-lg-6 mb-16">
            <div class="form-group">
                <input type="text" name="phone_number" id="phone_number" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" maxlength="10" minlength="10" class="form-control" placeholder="Phone Number">
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="col-lg-6 mb-16">
            <div class="form-group">
                <input type="text" name="msg_subject" id="msg_subject" class="form-control" placeholder="Website" pattern="((www.)?)(([^.]+)\.)?([a-zA-z0-9\-_]+)(.com|.net|.gov|.org|.in)(\/[^\s]*)?" data-error="Please Enter Valid Url eg : https://www.example.com/">
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 mb-16">
            <div class="form-group">
                <textarea name="message" class="form-control" id="message" cols="30" rows="4" required data-error="Write your message" placeholder="Message"></textarea>
                <div class="help-block with-errors"></div>
            </div>
        </div>       
<!--        <div class="form-group col-md-12">-->
<!--            <div class="g-recaptcha" data-sitekey="6LfF3UcdAAAAAPyOeyqE1p7_sF6vAHUmWTjmHf2I"></div>-->
<!--        </div>-->
        <div class="col-lg-12 col-md-12 ">

            <button type="submit" class="default-btn btn-bg-two border-radius-50">
                Send Message <i class='bx bx-chevron-right'></i>
            </button>
            <div id="msgSubmit" class="h3 text-center hidden"></div>
            <div class="clearfix"></div>
        </div>
    </div>
</form>