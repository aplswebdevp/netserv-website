<!DOCTYPE html>
<html lang="en">

<head>
       <!-- meta tag -->
       <meta charset="utf-8">
       <title>NetServ - Become a Partner</title>
       <meta name="description" content="Many business strategy projects are long on planning but short on action. Our methodology and approach deliver faster results by forming a team with key leaders within the customer's organization and developing the business strategy in main phases.">
       <meta name="keywords" content="strategic plans, strategic development, strategic goals, business strategy,  business goals, business strategic planning, marketing plan and strategy, action plan and strategic plan, action plan and strategy, action plan for business strategy, business consultant strategy, business development & strategy
">
       <!-- responsive tag -->
       <meta http-equiv="x-ua-compatible" content="ie=edge">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <!-- favicon -->
       <link rel="apple-touch-icon" href="">
       <link rel="canonical" href="https://www.ngnetserv.com/become-a-partner" />
       <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
       <?php include 'service_csslinks.php'; ?>
       <!-- <link rel="shortcut icon" type="image/x-icon" href="<?php echo main_url; ?>/assets/images/favicon.png"> -->
       <link rel="stylesheet" href="<?php echo main_url; ?>/assets/css/plan-strategy.css">

       <link rel="stylesheet" href="<?php echo main_url; ?>/assets/css/country_code.css">

       <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/css/intlTelInput.css" rel="stylesheet" media="screen"> -->

       <script type='application/ld+json'>
              {
                     "@context": "http://www.schema.org",
                     "@type": "WebSite",
                     "name": "NetSev",
                     "url": "http://www.ngnetserv.com/"
              }
       </script>
</head>
<!-- Internal-css-starts-->
<style type="text/css">
       .rs-breadcrumbs.bg-3 {
              background-image: linear-gradient(90deg, #fff 0, rgb(234 235 237 / 60%) 50%, rgb(255 255 255 / 0) 100%), url("<?php echo main_url; ?>/assets/images/services/planstatergy/partner_banner.webp");
              background-size: cover;
              background-position: 10%
       }


       /* start css for form  */
       .frmstyle {
              box-shadow: 5px 7px 30px 0px #e9ecef;
              padding: 30px 40px 30px 40px;
       }

       .become_parttext {
              margin-bottom: 307px;

       }


       /* .form-part {
              box-shadow: 0px 0px 10px 0px #eee;
       } */

       .rs-contact.style1 .form-part {
              padding: 60px 60px 60px 60px;
       }

       .frmcss {
              box-shadow: 0px 0px 10px 0px #eee;
              width: 100%;
              padding: 10px 18px;
              border: none;
              outline: none;
              border-radius: 0;
              color: #363636;
              font-size: 16px;
              height: 48px;
       }

       /* end css for form */

       /*start css for only country name dropdown   */


       /* .frmcss {
              background: #f1f6fc;
              width: 100%;
              padding: 10px 18px;
              border: none;
              outline: none;
              border-radius: 0;
              color: #363636;
              font-size: 16px;
              height: 48px;
       } */




       #partnershipForm .list-unstyled {
              color: #e31414;

       }

       #partnershipForm button {
              background: #106eea;
              color: white;
              font-family: 'Poppins', sans-serif;
              border: 2px solid #106eea;
              border-radius: 5px;
              padding: 10px 20px 10px 20px;
              margin-bottom: 1rem;

       }



       #msg_subject {
              background: #f1f6fc;
              width: 100%;
              padding: 10px 18px;
              border: none;
              outline: none;
              border-radius: 0;
              color: #363636;
              font-size: 16px;
              height: 48px;
       }

       #message {
              /* background: #f1f6fc; */
              box-shadow: 0px 0px 10px 0px #eee;

              width: 100%;
              padding: 10px 18px;
              border: none;
              outline: none;
              border-radius: 0;
              color: #363636;
              font-size: 16px;

       }

       .autocomplete {
              position: relative;
              /* display: inline-block; */
       }

       input {
              border: 1px solid transparent;
              background-color: #f1f1f1;
              padding: 10px;
              font-size: 16px;
       }

       /* input[type=text] {
  background-color: #f1f1f1;
  width: 100%;
} */



       .autocomplete-items {
              position: absolute;
              border: 1px solid #d4d4d4;
              border-bottom: none;
              border-top: none;
              z-index: 99;
              /*position the autocomplete items to be the same width as the container:*/
              top: 100%;
              left: 0;
              right: 0;
       }

       .autocomplete-items div {
              padding: 10px;
              cursor: pointer;
              background-color: #fff;
              border-bottom: 1px solid #d4d4d4;
       }

       /*when hovering an item:*/
       .autocomplete-items div:hover {
              background-color: #e9e9e9;
       }

       /*when navigating through the items using the arrow keys:*/
       .autocomplete-active {
              background-color: DodgerBlue !important;
              color: #ffffff;
       }

       .sec-title .sub-title {
              font-family: "Poppins", sans-serif;
              font-weight: 500;
              text-transform: none !important;
              margin-bottom: 8px;
       }

       /*end css for only country name dropdown   */

       /* start css for contact form */

       @media only screen and (min-width: 280px) and (max-width: 1024px) {
              .become_parttext {
                     margin-bottom: 0px;
                     margin-top: 60px;

              }

              .rs-contact.style1 .form-part {
                     padding: 60px 12px 0px 0px;
              }

       }


       /* end css for contact form */
</style>
<!-- Internal-css-Ends-->

<body class="home-eight">
       <!-- Google Tag Manager (noscript) -->
       <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
       <!-- End Google Tag Manager (noscript) -->
       <!-- Preloader area start here -->

       <!--End preloader here -->
       <!--Full width header Start-->
       <div class="full-width-header header-style4">
              <!--header-->
              <?php include 'header.php'; ?>
              <!--Header End-->
       </div>
       <!--Full width header End-->
       <!-- Main content Start -->
       <div class="main-content">
              <!-- Breadcrumbs Section Start -->

              <div class="rs-breadcrumbs bg-3">
                     <div class="container">
                            <div class="content-part text-center">
                                   <!-- <p><b>Services - <a href="<?php echo main_url; ?>/services/managed-services/managed-services"><span class="text-dark">Managed services</span></a></b> </p> -->
                                   <h1 class="breadcrumbs-title  mb-2">Become a Partner
                                   </h1>
                                   <h5 class="tagline-text"> Building a Long-Term Partnership Through Solid Relationships </h5>

                            </div>

                     </div>

              </div>

              <!-- Breadcrumbs Section End -->
              <!--start  section 1 -->
              <div class="rs-solutions style1 white-bg  modify2 pt-110 pb-100 md-pt-80 md-pb-64">
                     <div class="container">
                            <div class="sec-title style2 mb-60 md-mb-50 sm-mb-42">
                                   <div class="first-half y-middle">
                                          <div class="sec-title mb-24">
                                                 <p style="font-size: 17px;" class="mt-10 text-justify">Our collaboration is a synergistic alliance founded on mutual trust and respect and an in-depth understanding of each other's requirements and goals. We create a long-term partnership that promotes growth and success.

                                                 </p>
                                                 <div class="content-part mt-4">

                                                        <h6><b><a href="#" class="text-dark"><button type="button" id="applybtn1" class="btn btn-outline-primary  w-30">Apply Now</button>
                                                                      </a></b></h6>

                                                 </div>
                                          </div>

                                   </div>
                                   <div class="last-half">
                                          <div class="image-part">
                                                 <img src="<?php echo main_url; ?>/assets/images/services/planstatergy/partner1.webp" style="border-radius: 19px;" alt="Become-a-partner" title="Become-a-partner">
                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>
              <!--end section 1 -->

              <!-- 2nd section start  -->
              <div id="rs-about" class="rs-about style1 pt-100 pb-100 bg1 md-pt-80 gray-bg">
                     <div class="container">
                            <div class="row  cards_left_right">
                                   <div class="col-lg-12 md-mb-10">
                                          <h3 class="title text-center mb-20" style="font-weight:500;">
                                                 <strong>Why <span class="txt_clr">NetServ?</span></strong>
                                          </h3>
                                   </div>
                                   <div class="col-lg-6 padding-0">
                                          <img src="<?php echo main_url; ?>/assets/images/services/planstatergy/partner2.webp" style="border-radius: 19px;" alt="Become-a-partner" title="Become-a-partner">
                                   </div>
                                   <div class="col-lg-6 pl-66  md-pt-42 md-pb-42">
                                          <div class="services-part">


                                                 <div class="services-text">
                                                        <div class="desc pl-2 text-justify" style="font-size: 17px;">
                                                               <p> NetServ is a trustworthy and reliable partner for businesses seeking long-term relationships. Our experienced team is dedicated to understanding your specific requirements and providing tailored solutions to help you achieve your objectives. We believe in the power of collaboration, and our partnership approach is intended to foster growth and long-term success.
                                                               </p>
                                                               <p>NetServ prioritizes communication and transparency, ensuring you are always informed and involved in every process step. We take pride in our technical expertise, industry knowledge, and commitment to providing exceptional service.</p>
                                                               <p>Whether you want to outsource IT services, streamline your business operations, or optimize your technology infrastructure, NetServ has the experience and resources to help you succeed.</p>

                                                               <div class="content-part mt-3 pb-3">

                                                                      <h6><b><a href="#" class="text-dark"><button type="button" id="applybtn2" class="btn btn-outline-primary  w-30">Apply Now</button>
                                                                                    </a></b></h6>

                                                               </div>
                                                        </div>



                                                 </div>
                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>
              <!-- 2nd section ends  -->

              <!-- 3rd section start -->
              <div class="rs-solutions  style1 white-bg modify2 pt-100 mb-20 pb-100 md-pt-80 md-pb-24 aos-init aos-animate" data-aos="fade-up" data-aos-duration="2000">
                     <div class="container">
                            <div class="row y-middle">
                                   <!-- <div class="col-lg-12 md-mb-10">
                                          <h3 class="title  mb-20 text-center" style="font-weight:500;">
                                                 <strong>Increasing Revenue Potential with <br>Improved Cross-Selling and Upselling Strategies</strong>
                                          </h3>
                                   </div> -->
                                   <div class="col-lg-6">
                                          <div class="sec-title mb-33">
                                                 <div class="col-lg-12 md-mb-10">
                                                        <h3 class="title  mb-20 " style="font-weight:500;">
                                                               <strong>Increasing <span class="txt_clr">Revenue Potential</span> with Improved Cross-Selling and Upselling Strategies</strong>
                                                        </h3>
                                                 </div>
                                                 <p class="mt-25 text-justify" style="font-size: 17px;">
                                                        You can gain a competitive advantage and increase cross-selling and upselling opportunities by leveraging our managed and professional services and solutions, which span from client to infrastructure and from edge to the cloud. Stay ahead of the competition with our cutting-edge technology and expert advice, allowing you to optimize your operations and maximize revenue potential. With our comprehensive service suite, we can help you identify new opportunities and capitalize on existing ones, ultimately driving your business's growth and success.
                                                 </p>

                                                 <div class="content-part mt-3 pb-3">

                                                        <h6><b><a href="#" class="text-dark"><button type="button" id="applybtn3" class="btn btn-outline-primary  w-30">Apply Now</button>
                                                                      </a></b></h6>

                                                 </div>
                                          </div>
                                   </div>
                                   <div class="col-lg-6 md-order-first md-mb-30">
                                          <div class="image-part">
                                                 <img src="<?php echo main_url; ?>/assets/images/services/planstatergy/partner3.webp" style="border-radius: 19px;" alt="Become-a-partner" title="Become-a-partner">
                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>
              <!-- 3dd section end -->


              <!-- start section 4  -->

              <!-- end section 4  -->




              <!-- 5th form section start -->
              <div class="rs-about style9 gray-bg pt-50 pb-50 md-pt-30 md-pb-30 aos-init aos-animate" data-aos="fade-up" data-aos-duration="2000">
                     <div class="container-fluid">
                            <div class="row y-middle white-bg">
                                   <div class="col-lg-5 pr-73 md-pr-15  md-mb-50 become_parttext">
                                          <div class="sec-title  md-mb-20 pl-38">
                                                 <h2 class="title pb-7 md-mt-10 mb-0"><strong>Become a Partner</strong></h2>
                                          </div>
                                          <div class="mb-20 md-mb-15 pl-38">
                                                 <p style="font-size: 17px;" class="title mt-2 mb-0">
                                                        Collaborate with us to uncover untapped markets and opportunities while transforming your IT landscape. Let our collaboration propel help move your company forward as we collaborate to drive innovation and growth
                                                        through strategic technology initiatives.
                                                 </p>
                                          </div>
                                   </div>

                                   <div class="col-lg-7 formimg pr-0">

                                          <div class="rs-contact style1 md-pt-20 md-pb-20" id="formSec">
                                                 <div class="container">
                                                        <!-- <div class="white-bg"> -->
                                                        <div class="row">
                                                               <div class="col-lg-12 form-part">
                                                                      <div id="form-messages"></div>
                                                                      <div class="frmstyle">
                                                                             <form id="partnershipForm">

                                                                                    <div class="sec-title mb-20 md-mb-30">
                                                                                           <div class="sub-title primary">CONTACT US</div>

                                                                                    </div>

                                                                                    <div class="row contact-form">
                                                                                           <div class="col-lg-6 mb-20">
                                                                                                  <div class="form-group">
                                                                                                         <input type="text" name="fullname" id="fullname" class="form-control frmcss" required data-error="* Please Enter Full Name." placeholder="Full Name" />
                                                                                                         <div class="help-block with-errors"></div>
                                                                                                  </div>
                                                                                           </div>
                                                                                           <div class="col-lg-6 mb-16">
                                                                                                  <div class="form-group">
                                                                                                         <input type="text" name="jobrole" id="jobrole" class="form-control frmcss" required data-error="* Please Enter Job Role." placeholder="Job Role" />
                                                                                                         <div class="help-block with-errors"></div>
                                                                                                  </div>
                                                                                           </div>
                                                                                           <div class="col-lg-6 mb-16">
                                                                                                  <div class="form-group">
                                                                                                         <input type="email" name="email" id="email" class="form-control frmcss" required data-error="* Please Enter Email." placeholder="Email" />
                                                                                                         <div class="help-block with-errors"></div>
                                                                                                  </div>
                                                                                           </div>
                                                                                           <div class="col-lg-6 mb-16">
                                                                                                  <div class="form-group">
                                                                                                         <input type="tel" name="phone_number" id="phone_number" class="form-control frmcss" placeholder="Contact" required data-error="* Please Enter Contact Number." />

                                                                                                         <span id="valid-msg" class="hide">✓ Valid</span>
                                                                                                         <span id="error-msg" class="hide">Invalid number</span>
                                                                                                         <div class="help-block with-errors"></div>
                                                                                                  </div>
                                                                                           </div>
                                                                                           <div class="col-lg-6 mb-16">
                                                                                                  <div class="form-group">
                                                                                                         <input type="text" name="company" id="company" class="form-control one frmcss" required data-error="* Please Enter Company Name." placeholder="Company" />
                                                                                                         <div class="help-block with-errors"></div>
                                                                                                  </div>
                                                                                           </div>
                                                                                           <div class="col-lg-6 mb-16">
                                                                                                  <div class="autocomplete form-group">
                                                                                                         <input id="country" class="form-control frmcss" type="text" name="country" placeholder="Region/ Country" required data-error="* Please Select Your country." />
                                                                                                         <div class="help-block with-errors"></div>
                                                                                                  </div>
                                                                                           </div>

                                                                                           <!--        <div class="form-group col-md-12">-->
                                                                                           <!--            <div class="g-recaptcha" data-sitekey="6LfF3UcdAAAAAPyOeyqE1p7_sF6vAHUmWTjmHf2I"></div>-->
                                                                                           <!--        </div>-->

                                                                                           <div class="col-lg-12 col-md-12 mb-16">
                                                                                                  <div class="form-group">
                                                                                                         <textarea name="message_1" class=" form-control frmcss" id="message_1" cols=" 30" rows="4" required="" data-error="* Write your message" placeholder="Message"></textarea>
                                                                                                         <div class="help-block with-errors"></div>
                                                                                                  </div>
                                                                                           </div>
                                                                                           <div class="col-lg-12 col-md-12">
                                                                                                  <button type="submit" class="default-btn btn-bg-two btn-block border-radius-50">SUBMIT <i class='bx bx-chevron-right'></i></button>
                                                                                                  <div id="msgSubmit" class="h3 text-center hidden"></div>
                                                                                                  <div class="clearfix"></div>
                                                                                           </div>
                                                                                    </div>
                                                                             </form>
                                                                      </div>
                                                               </div>
                                                        </div>
                                                 </div>
                                                 <!-- </div> -->
                                          </div>


                                   </div>
                            </div>
                     </div>
              </div>

              <!-- 5th form section end -->



              <!-- Conatct-form-starts -->

       </div>
       <!-- Conatct-form-Ends-->
       <!-- Card-Section-Ends -->
       <!-- Main content End -->
       <!-- Footer Start -->
       <?php include 'footer.php'; ?>
       <!-- Footer End -->
       <!-- start scrollUp  -->
       <div id="scrollUp">
              <i class="fa fa-angle-up"></i>
       </div>
       <!-- End scrollUp  -->
       <?php include 'service_jslinks.php'; ?>

       <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.js"></script> -->
       <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.min.js"></script> -->

       <!-- <script src="./assets/js/country_code_intltelinput.min.js"></script> -->

       <script src="<?php echo main_url; ?>/assets/js/country_code_intltelinput.min.js"></script>




       <script>
              // start code for region/country autodropdown in form-field


              function autocomplete(inp, arr) {
                     /*the autocomplete function takes two arguments,
                     the text field element and an array of possible autocompleted values:*/
                     var currentFocus;
                     /*execute a function when someone writes in the text field:*/
                     inp.addEventListener("input", function(e) {
                            var a, b, i, val = this.value;
                            /*close any already open lists of autocompleted values*/
                            closeAllLists();
                            if (!val) {
                                   return false;
                            }
                            currentFocus = -1;
                            /*create a DIV element that will contain the items (values):*/
                            a = document.createElement("DIV");
                            a.setAttribute("id", this.id + "autocomplete-list");
                            a.setAttribute("class", "autocomplete-items");
                            /*append the DIV element as a child of the autocomplete container:*/
                            this.parentNode.appendChild(a);
                            /*for each item in the array...*/
                            for (i = 0; i < arr.length; i++) {
                                   /*check if the item starts with the same letters as the text field value:*/
                                   if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                                          /*create a DIV element for each matching element:*/
                                          b = document.createElement("DIV");
                                          /*make the matching letters bold:*/
                                          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                                          b.innerHTML += arr[i].substr(val.length);
                                          /*insert a input field that will hold the current array item's value:*/
                                          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                                          /*execute a function when someone clicks on the item value (DIV element):*/
                                          b.addEventListener("click", function(e) {
                                                 /*insert the value for the autocomplete text field:*/
                                                 inp.value = this.getElementsByTagName("input")[0].value;
                                                 /*close the list of autocompleted values,
                                                 (or any other open lists of autocompleted values:*/
                                                 closeAllLists();
                                          });
                                          a.appendChild(b);
                                   }
                            }
                     });
                     /*execute a function presses a key on the keyboard:*/
                     inp.addEventListener("keydown", function(e) {
                            var x = document.getElementById(this.id + "autocomplete-list");
                            if (x) x = x.getElementsByTagName("div");
                            if (e.keyCode == 40) {
                                   /*If the arrow DOWN key is pressed,
                                   increase the currentFocus variable:*/
                                   currentFocus++;
                                   /*and and make the current item more visible:*/
                                   addActive(x);
                            } else if (e.keyCode == 38) { //up
                                   /*If the arrow UP key is pressed,
                                   decrease the currentFocus variable:*/
                                   currentFocus--;
                                   /*and and make the current item more visible:*/
                                   addActive(x);
                            } else if (e.keyCode == 13) {
                                   /*If the ENTER key is pressed, prevent the form from being submitted,*/
                                   e.preventDefault();
                                   if (currentFocus > -1) {
                                          /*and simulate a click on the "active" item:*/
                                          if (x) x[currentFocus].click();
                                   }
                            }
                     });

                     function addActive(x) {
                            /*a function to classify an item as "active":*/
                            if (!x) return false;
                            /*start by removing the "active" class on all items:*/
                            removeActive(x);
                            if (currentFocus >= x.length) currentFocus = 0;
                            if (currentFocus < 0) currentFocus = (x.length - 1);
                            /*add class "autocomplete-active":*/
                            x[currentFocus].classList.add("autocomplete-active");
                     }

                     function removeActive(x) {
                            /*a function to remove the "active" class from all autocomplete items:*/
                            for (var i = 0; i < x.length; i++) {
                                   x[i].classList.remove("autocomplete-active");
                            }
                     }

                     function closeAllLists(elmnt) {
                            /*close all autocomplete lists in the document,
                            except the one passed as an argument:*/
                            var x = document.getElementsByClassName("autocomplete-items");
                            for (var i = 0; i < x.length; i++) {
                                   if (elmnt != x[i] && elmnt != inp) {
                                          x[i].parentNode.removeChild(x[i]);
                                   }
                            }
                     }
                     /*execute a function when someone clicks in the document:*/
                     document.addEventListener("click", function(e) {
                            closeAllLists(e.target);
                     });
              }

              /*An array containing all the country names in the world:*/
              var countries = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua & Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia & Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central Arfrican Republic", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cuba", "Curacao", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauro", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "North Korea", "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre & Miquelon", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "South Sudan", "Spain", "Sri Lanka", "St Kitts & Nevis", "St Lucia", "St Vincent", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks & Caicos", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States of America", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];

              /*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
              autocomplete(document.getElementById("country"), countries);

              // end code for region/country autodropdown in form-field

              // start code for scroll page on contact-form when click on apply now btn
              window.onbeforeunload = function() {
                     window.scrollTo(0, 0);
              };
              $("#applybtn1").click(function() {
                     $('html, body').animate({
                            scrollTop: $("#partnershipForm").offset().top - 200
                     }, 1000);
              });

              window.onbeforeunload = function() {
                     window.scrollTo(0, 0);
              };
              $("#applybtn2").click(function() {
                     $('html, body').animate({
                            scrollTop: $("#partnershipForm").offset().top - 200
                     }, 1000);
              });
              window.onbeforeunload = function() {
                     window.scrollTo(0, 0);
              };
              $("#applybtn3").click(function() {
                     $('html, body').animate({
                            scrollTop: $("#partnershipForm").offset().top - 200
                     }, 1000);
              });




              // end code for scroll page on contact-form when click on apply now btn

              // start code for country code dropdown 


              var telInput = $("#phone_number"),
                     errorMsg = $("#error-msg"),
                     validMsg = $("#valid-msg");

              // initialise plugin
              telInput.intlTelInput({

                     allowExtensions: true,
                     formatOnDisplay: true,
                     autoFormat: true,
                     autoHideDialCode: true,
                     autoPlaceholder: true,
                     defaultCountry: "us",
                     ipinfoToken: "yolo",

                     nationalMode: false,
                     numberType: "MOBILE",
                     //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                     preferredCountries: ['us', "gb", 'sa', 'ae', 'qa', 'om', 'bh', 'kw', 'ma'],
                     preventInvalidNumbers: true,
                     separateDialCode: true,
                     initialCountry: "",
                     geoIpLookup: function(callback) {
                            $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                                   var countryCode = (resp && resp.country) ? resp.country : "";
                                   callback(countryCode);
                            });
                     },
                     utilsScript: "<?php echo main_url; ?>/assets/js/country_dropdown_utils.js"
              });

              var reset = function() {
                     telInput.removeClass("error");
                     errorMsg.addClass("hide");
                     validMsg.addClass("hide");
              };

              // on blur: validate
              telInput.blur(function() {
                     reset();
                     if ($.trim(telInput.val())) {
                            if (telInput.intlTelInput("isValidNumber")) {
                                   validMsg.removeClass("hide");
                                   /* get code here*/
                                   var getCode = telInput.intlTelInput('getSelectedCountryData').dialCode;
                                   // alert(getCode);
                            } else {
                                   telInput.addClass("error");
                                   errorMsg.removeClass("hide");
                            }
                     }
              });

              // on keyup / change flag: reset
              telInput.on("keyup change", reset);

              // end code for country code dropdown 
       </script>
</body>

</html>