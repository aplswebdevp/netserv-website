(function ($) {
    "use strict";
    $("#partnershipForm")
      .validator()
      .on("submit", function (event) {
        // console.log(event);
        if (event.isDefaultPrevented()) {
          formError();
          //  submitMSG(false, "Did you fill up the form properly?");
        } else {
          event.preventDefault();
          submitForm();
          submitMSG(true, "sending...");
        }
      });
    // function submitForm() {
      
    //   var fname = $("#fullname").val();
    //    var jobrole = $("#jobrole").val();
    //    var email = $("#email").val();
    //   var phone_number = $("#phone_number").val();
    //   var company = $("#company").val();
    //   var country = $("#country").val();
      
  
    //   $.ajax({
    //     type: "POST",
    //     url: "../../assets/php/partner_form.php",
    //     data:
    //       "fullname=" +
    //       fname 
    //       +
    //       "&jobrole=" +
    //       jobrole +
    //      "&email=" +
    //        email 
    //        +
    //       "&phone_number=" +
    //       phone_number +
    //       "&company=" +
    //       company +
    //       "&country=" +
    //       country
    //       ,
    //     beforeSend: function () {
    //       $('#msgSubmit').html('We are processing your form ..');
    //       // console.log('sending....');
    //     },
    //     success: function (text) {
    //       if (text == "success") {
    //         formSuccess();
  
    //       } else {
    //         formError();
    //         submitMSG(false, text);
    //       }
    //     },
    //   });
    // }


    

function submitForm() {
  var fname = $("#fullname").val();
  var jobrole = $("#jobrole").val();
  var email = $("#email").val();
  var telInput = $("#phone_number");
  var countryCode = telInput.intlTelInput('getSelectedCountryData').dialCode;
  var phoneNumber = telInput.val();
  var fullPhoneNumber = "+" + countryCode + phoneNumber; // concatenating country code with phone number
  var company = $("#company").val();
  var country = $("#country").val();
    var message_1 = $("#message_1").val();
    // alert(message);
  
  // sending the concatenated phone number to the server
  $.ajax({
    type: "POST",
    url: "../../assets/php/partner_form.php",
    data: {
      fullname: fname,
      jobrole: jobrole,
      email: email,
      phone_number: fullPhoneNumber, // using the concatenated phone number
      company: company,
      country: country,
      message_1: message_1
    },
    beforeSend: function () {
      $('#msgSubmit').html('We are processing your form ..');
    },
    success: function (text) {
      if (text == "success") {
        formSuccess();
      } else {
        formError();
        submitMSG(false, text);
      }
    },
  });
}

    function formSuccess() {
      $("#partnershipForm")[0].reset();
      submitMSG(true, "Your message submitted! our expert will contact you shortly.");
    }
    function formError() {
      $("#partnershipForm")
        .removeClass()
  
        .one(
          function () {
            $(this).removeClass();
          }
        );
    }
    function submitMSG(valid, msg) {
      if (valid) {
        var msgClasses = "h4 text-success";
      } else {
        var msgClasses = "h4 text-danger";
      }
      $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
    }
  })(jQuery);
  