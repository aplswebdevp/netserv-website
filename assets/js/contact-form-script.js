(function ($) {
  "use strict";
  $("#contactForm")
    .validator()
    .on("submit", function (event) {
      // console.log(event);
      if (event.isDefaultPrevented()) {
        formError();
        //  submitMSG(false, "Did you fill up the form properly?");
      } else {
        event.preventDefault();
        submitForm();
        submitMSG(true, "sending...");
      }
    });
  function submitForm() {
    
    var fname = $("#fname").val();
    var email = $("#email").val();
    var message = $("#message").val();
    var phone_number = $("#phone_number").val();
    var msg_subject = $("#msg_subject").val();
    

    $.ajax({
      type: "POST",
      url: "../../assets/php/form-process.php",
      data:
        "fname=" +
        fname +
        "&email=" +
        email +
        "&phone_number=" +
        phone_number +
        "&msg_subject=" +
        msg_subject +
        
        "&message=" +
        message,
      beforeSend: function () {
        $('#msgSubmit').html('We are processing your form ..');
        //console.log('sending....');
      },
      success: function (text) {
        if (text == "success") {
          formSuccess();

        } else {
          formError();
          submitMSG(false, text);
        }
      },
    });
  }
  function formSuccess() {
    $("#contactForm")[0].reset();
    submitMSG(true, "Your message submitted! our expert will contact you shortly.");
  }
  function formError() {
    $("#contactForm")
      .removeClass()

      .one(
        function () {
          $(this).removeClass();
        }
      );
  }
  function submitMSG(valid, msg) {
    if (valid) {
      var msgClasses = "h4 text-success";
    } else {
      var msgClasses = "h4 text-danger";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
  }
})(jQuery);
