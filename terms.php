<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Terms</title>
    <meta name="description" content="Please read this terms of service agreement carefully, as it contains important information regarding your legal rights and remedies.">
    <meta name="keywords" content="terms of service, terms and conditions, terms conditions, terms and policy, terms of service agreement, policy terms, terms of use policy, agree terms, terms of service policy, terms & policy,">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/terms"/>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'> 
{
  "@context": "http://www.schema.org",
  "@type": "WebSite",
  "name": "NetSev",
  "url": "http://www.ngnetserv.com/"
}
 </script>
</head>
<style type="text/css">
    ul {
        list-style: disc;
    }
</style>
<body class="home-eight">
<!-- Preloader area start here -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--End preloader here -->
<!--Full width header Start-->
<div class="full-width-header header-style4">
    <!--header-->
    <?php include 'header.php';?>
    <!--Header End-->
</div>
<!--Full width header End-->
<!-- Main content Start -->
<div class="main-content">
    <div class="container">
        <br>  <br>
        <div class="">
            <h1 class="pt-4 pb-3" style="font-size: 25px">TERMS OF SERVICES
            </h1>
            <div class="">
                <p>PLEASE READ THIS TERMS OF SERVICE AGREEMENT CAREFULLY, AS IT
                    CONTAINS IMPORTANT INFORMATION REGARDING YOUR LEGAL RIGHTS AND
                    REMEDIES.
                </p>
                <small>Last Revised: 2021-11-09 15:57:02
                </small>
            </div>
        </div>
        <div class="pt-2">
            <h6>1. OVERVIEW
            </h6>
            <p>
                This Terms of Service Agreement ("Agreement") is entered into by and between NetServ,
                registered address Pune, India ("Company") and you, and is made effective as of the date of your
                use of this website http://www.netserv.io ("Site") or the date of electronic acceptance.
                This Agreement sets forth the general terms and conditions of your use of the
                http://www.netserv.io as well as the products and/or services purchased or accessed through this
                Site (the "Services").Whether you are simply browsing or using this Site or purchase Services,
                your use of this Site and your electronic acceptance of this Agreement signifies that you have
                read, understand, acknowledge and agree to be bound by this Agreement our Privacy policy. The
                terms "we", "us" or "our" shall refer to Company. The terms "you", "your", "User" or "customer"
                shall refer to any individual or entity who accepts this Agreement, uses our Site, has access or
                uses the Services. Nothing in this Agreement shall be deemed to confer any third-party rights or
                benefits.
            </p>
            <p>Company may, in its sole and absolute discretion, change or modify this Agreement, and any
                policies or agreements which are incorporated herein, at any time, and such changes or
                modifications shall be effective immediately upon posting to this Site. Your use of this Site or the
                Services after such changes or modifications have been made shall constitute your acceptance of
                this Agreement as last revised.
            </p>
            <p>IF YOU DO NOT AGREE TO BE BOUND BY THIS AGREEMENT AS LAST REVISED,
                DO NOT USE (OR CONTINUE TO USE) THIS SITE OR THE SERVICES.
            </p>
        </div>
        <div class="pt-2">
            <h6>2. ELIGIBILITY
            </h6>
            <p>
                This Site and the Services are available only to Users who can form legally binding contracts
                under applicable law. By using this Site or the Services, you represent and warrant that you are
                (i) at least eighteen (18) years of age, (ii) otherwise recognized as being able to form legally
                binding contracts under applicable law, and (iii) are not a person barred from purchasing or
                receiving the Services found under the laws of the United States or other applicable jurisdiction.
                If you are entering into this Agreement on behalf of a company or any corporate entity, you
                represent and warrant that you have the legal authority to bind such corporate entity to the terms
                and conditions contained in this Agreement, in which case the terms "you", "your", "User" or
                "customer" shall refer to such corporate entity. If, after your electronic acceptance of this
                Agreement, Company finds that you do not have the legal authority to bind such corporate entity,
                you will be personally responsible for the obligations contained in this Agreement.
            </p>
        </div>
        <div class="pt-2">
            <h6>3. RULES OF USER CONDUCT
            </h6>
            <div class="pl-2">
                <ul>
                    <li>
                        Your use of this Site, including any content you submit, will comply with this Agreement
                        and all applicable local, state, national and international laws, rules and regulations.
                    </li>
                </ul>
            </div>
            <p class="pt-2">You will not use this Site in a manner that:</p>
            <div class="pl-2 ">
                <ul>
                    <li>
                        Is illegal, or promotes or encourages illegal activity;
                    </li>
                    <li>Promotes, encourages or engages in child pornography or the exploitation of children;
                    </li>
                    <li>Promotes, encourages or engages in child pornography or the exploitation of children;
                    </li>
                    <li>Promotes, encourages or engages in any spam or other unsolicited bulk email, or
                        computer or network hacking or cracking.
                    </li>
                    <li>Infringes on the intellectual property rights of another User or any other person or entity
                    </li>
                    <li>Violates the privacy or publicity rights of another User or any other person or entity, or
                        breaches any duty of confidentiality that you owe to another User or any other person or
                        entity.
                    </li>
                    <li>Interferes with the operation of this Site.
                    </li>
                    <li>
                        Interferes with the operation of this Site
                    </li>
                </ul>
            </div>
            <p class="pt-2">You will not:
            </p>
            <div class="pl-2 ">
                <ul>
                    <li>
                        copy or distribute in any medium any part of this Site, except where expressly authorized
                        by Company,
                    </li>
                    <li>
                        modify or alter any part of this Site or any of its related technologies
                    </li>
                    <li>
                        access Companies Content (as defined below) or User Content through any technology or
                        means other than through this Site itself.
                    </li>
                </ul>
            </div>
        </div>
        <div class="pt-2">
            <h6>4. INTELLECTUAL PROPERTY
            </h6>
            <p>
                In addition to the general rules above, the provisions in this Section apply specifically to your
                use of Companies Content posted to Site. Companies Content on this Site, including without
                limitation the text, software, scripts, source code, API, graphics, photos, sounds, music, videos
                and interactive features and the trademarks, service marks and logos contained therein
                ("Companies Content"), are owned by or licensed to NetServ in perpetuity, and are subject to
                copyright, trademark, and/or patent protection.
                Companies Content is provided to you "as is", "as available" and "with all faults" for your
                information and personal, non-commercial use only and may not be downloaded, copied,
                reproduced, distributed, transmitted, broadcast, displayed, sold, licensed, or otherwise exploited
                for any purposes whatsoever without the express prior written consent of Company. No right or
                license under any copyright, trademark, patent, or other proprietary right or license is granted by
                this Agreement.
            </p>
        </div>
        <div class="pt-2">
            <h6>5. DISCLAIMER OF REPRESENTATIONS AND WARRANTIES
            </h6>
            <p>
                YOU SPECIFICALLY ACKNOWLEDGE AND AGREE THAT YOUR USE OF THIS SITE
                SHALL BE AT YOUR OWN RISK AND THAT THIS SITE ARE PROVIDED "AS IS", "AS
                AVAILABLE" AND "WITH ALL FAULTS". COMPANY, ITS OFFICERS, DIRECTORS,
                EMPLOYEES, AGENTS, DISCLAIM ALL WARRANTIES, STATUTORY, EXPRESS OR
                IMPLIED, INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF
                TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
                NON-INFRINGEMENT. COMPANY, ITS OFFICERS, DIRECTORS, EMPLOYEES, AND
                AGENTS MAKE NO REPRESENTATIONS OR WARRANTIES ABOUT (I) THE
                ACCURACY, COMPLETENESS, OR CONTENT OF THIS SITE, (II) THE ACCURACY,
                COMPLETENESS, OR CONTENT OF ANY SITES LINKED (THROUGH HYPERLINKS,
                BANNER ADVERTISING OR OTHERWISE) TO THIS SITE, AND/OR (III) THE SERVICES
                FOUND AT THIS SITE OR ANY SITES LINKED (THROUGH HYPERLINKS, BANNER
                ADVERTISING OR OTHERWISE) TO THIS SITE, AND COMPANY ASSUMES NO
                LIABILITY OR RESPONSIBILITY FOR THE SAME.
            </p>
            <p>
                IN ADDITION, YOU SPECIFICALLY ACKNOWLEDGE AND AGREE THAT NO ORAL
                OR WRITTEN INFORMATION OR ADVICE PROVIDED BY COMPANY, ITS OFFICERS,
                DIRECTORS, EMPLOYEES, OR AGENTS, AND THIRD-PARTY SERVICE PROVIDERS
                WILL (I) CONSTITUTE LEGAL OR FINANCIAL ADVICE OR (II) CREATE A
                WARRANTY OF ANY KIND WITH RESPECT TO THIS SITE OR THE SERVICES FOUND
                AT THIS SITE, AND USERS SHOULD NOT RELY ON ANY SUCH INFORMATION OR
                ADVICE.
            </p>
            <p>
                THE FOREGOING DISCLAIMER OF REPRESENTATIONS AND WARRANTIES SHALL
                APPLY TO THE FULLEST EXTENT PERMITTED BY LAW, and shall survive any
                termination or expiration of this Agreement or your use of this Site or the Services found at this
                Site.
            </p>
        </div>
        <div class="pt-2">
            <h6>6. LIMITATION OF LIABILITY
            </h6>
            <p>
                IN NO EVENT SHALL COMPANY, ITS OFFICERS, DIRECTORS, EMPLOYEES, AGENTS,
                AND ALL THIRD PARTY SERVICE PROVIDERS, BE LIABLE TO YOU OR ANY OTHER
                PERSON OR ENTITY FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
                PUNITIVE, OR CONSEQUENTIAL DAMAGES WHATSOEVER, INCLUDING ANY
                DAMAGES THAT MAY RESULT FROM (I) THE ACCURACY, COMPLETENESS, OR
                CONTENT OF THIS SITE, (II) THE ACCURACY, COMPLETENESS, OR CONTENT OF
                ANY SITES LINKED (THROUGH HYPERLINKS, BANNER ADVERTISING OR
                OTHERWISE) TO THIS SITE, (III) THE SERVICES FOUND AT THIS SITE OR ANY SITES
                LINKED (THROUGH HYPERLINKS, BANNER ADVERTISING OR OTHERWISE) TO
                THIS SITE, (IV) PERSONAL INJURY OR PROPERTY DAMAGE OF ANY NATURE
                WHATSOEVER, (V) THIRD-PARTY CONDUCT OF ANY NATURE WHATSOEVER, (VI)
                ANY INTERRUPTION OR CESSATION OF SERVICES TO OR FROM THIS SITE OR ANY
                SITES LINKED (THROUGH HYPERLINKS, BANNER ADVERTISING OR OTHERWISE)
                TO THIS SITE, (VII) ANY VIRUSES, WORMS, BUGS, TROJAN HORSES, OR THE LIKE,
                WHICH MAY BE TRANSMITTED TO OR FROM THIS SITE OR ANY SITES LINKED
                (THROUGH HYPERLINKS, BANNER ADVERTISING OR OTHERWISE) TO THIS SITE,
                (VIII) ANY USER CONTENT OR CONTENT THAT IS DEFAMATORY, HARASSING,
                ABUSIVE, HARMFUL TO MINORS OR ANY PROTECTED CLASS, PORNOGRAPHIC,
                "X-RATED", OBSCENE OR OTHERWISE OBJECTIONABLE, AND/OR (IX) ANY LOSS
                OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF YOUR USE OF THIS SITE
                OR THE SERVICES FOUND AT THIS SITE, WHETHER BASED ON WARRANTY,
                CONTRACT, TORT, OR ANY OTHER LEGAL OR EQUITABLE THEORY, AND
                WHETHER OR NOT COMPANY IS ADVISED OF THE POSSIBILITY OF SUCH
                DAMAGES.
            </p>
            <p>
                IN ADDITION, You SPECIFICALLY ACKNOWLEDGE AND agree that any cause of action
                arising out of or related to this Site or the Services found at this Site must be commenced within
                one (1) year after the cause of action accrues, otherwise such cause of action shall be
                permanently barred.
            </p>
            <p>
                THE FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST
                EXTENT PERMITTED BY LAW, AND shall survive any termination or expiration of this
                Agreement or your use of this Site or the Services found at this Site.
            </p>
        </div>
        <div class="pt-2">
            <h6>7. INDEMNITY
            </h6>
            <p>
                You agree to protect, defend, indemnify and hold harmless Company and its officers, directors,
                employees, agents from and against any and all claims, demands, costs, expenses, losses,
                liabilities and damages of every kind and nature (including, without limitation, reasonable
                attorneys’ fees) imposed upon or incurred by Company directly or indirectly arising from (i)
                your use of and access to this Site; (ii) your violation of any provision of this Agreement or the
                policies or agreements which are incorporated herein; and/or (iii) your violation of any
                third-party right, including without limitation any intellectual property or other proprietary right.
                The indemnification obligations under this section shall survive any termination or expiration of
                this Agreement or your use of this Site or the Services found at this Site.
            </p>
        </div>
        <div class="pt-2">
            <h6>8. DATA TRANSFER
            </h6>
            <p>
                If you are visiting this Site from a country other than the country in which our servers are
                located, your communications with us may result in the transfer of information across
                international boundaries. By visiting this Site and communicating electronically with us, you
                consent to such transfers.
            </p>
        </div>
        <div class="pt-2">
            <h6>9. AVAILABILITY OF WEBSITE
            </h6>
            <p>
                Subject to the terms and conditions of this Agreement and our policies, we shall use
                commercially reasonable efforts to attempt to provide this Site on 24/7 basis. You acknowledge
                and agree that from time to time this Site may be inaccessible for any reason including, but not
                limited to, periodic maintenance, repairs or replacements that we undertake from time to time, or
                other causes beyond our control including, but not limited to, interruption or failure of
                telecommunication or digital transmission links or other failures.
            </p>
            <p>
                Subject to the terms and conditions of this Agreement and our policies, we shall use
                commercially reasonable efforts to attempt to provide this Site on 24/7 basis. You acknowledge
                and agree that from time to time this Site may be inaccessible for any reason including, but not
                limited to, periodic maintenance, repairs or replacements that we undertake from time to time, or
                other causes beyond our control including, but not limited to, interruption or failure of
                telecommunication or digital transmission links or other failures.
            </p>
        </div>
        <div class="pt-2">
            <h6>10. DISCONTINUED SERVICES
            </h6>
            <p>
                Company reserves the right to cease offering or providing any of the Services at any time, for
                any or no reason, and without prior notice. Although Company makes great effort to maximize
                the lifespan of all its Services, there are times when a Service we offer will be discontinued. If
                that is the case, that product or service will no longer be supported by Company. In such case,
                Company will either offer a comparable Service for you to migrate to or a refund. Company will
                not be liable to you or any third party for any modification, suspension, or discontinuance of any
                of the Services we may offer or facilitate access to.
            </p>
        </div>
        <div class="pt-2">
            <h6>11. NO THIRD-PARTY BENEFICIARIES
            </h6>
            <p>
                Nothing in this Agreement shall be deemed to confer any third-party rights or benefits.
            </p>
        </div>
        <div class="pt-2">
            <h6>12. COMPLIANCE WITH LOCAL LAWS
            </h6>
            <p>
                Company makes no representation or warranty that the content available on this Site are
                appropriate in every country or jurisdiction, and access to this Site from countries or jurisdictions
                where its content is illegal is prohibited. Users who choose to access this Site are responsible for
                compliance with all local laws, rules and regulations.
            </p>
        </div>
        <div class="pt-2">
            <h6>13. GOVERNING LAW
            </h6>
            <p>
                This Agreement and any dispute or claim arising out of or in connection with it or its subject
                matter or formation shall be governed by and construed in accordance with the laws of United
                States, California, to the exclusion of conflict of law rules.
            </p>
        </div>
        <div class="pt-2">
            <h6>14. DISPUTE RESOLUTION
            </h6>
            <p>
                Any controversy or claim arising out of or relating to these Terms of Services will be settled by
                binding arbitration. Any such controversy or claim must be arbitrated on an individual basis, and
                must not be consolidated in any arbitration with any claim or controversy of any other party. The
                arbitration must be conducted in United States, California, and judgment on the arbitration award
                may be entered into any court having jurisdiction thereof.
            </p>
        </div>
        <div class="pt-2">
            <h6>15. TITLES AND HEADINGS
            </h6>
            <p>
                The titles and headings of this Agreement are for convenience and ease of reference only and
                shall not be utilized in any way to construe or interpret the agreement of the parties as otherwise
                set forth herein.
            </p>
        </div>
        <div class="pt-2">
            <h6>16. SEVERABILITY
            </h6>
            <p>
                Each covenant and agreement in this Agreement shall be construed for all purposes to be a
                separate and independent covenant or agreement. If a court of competent jurisdiction holds any
                provision (or portion of a provision) of this Agreement to be illegal, invalid, or otherwise
                unenforceable, the remaining provisions (or portions of provisions) of this Agreement shall not
                be affected thereby and shall be found to be valid and enforceable to the fullest extent permitted
                by law.
            </p>
        </div>
        <div class="pt-2">
            <h6>17. CONTACT INFORMATION
            </h6>
            <p>
                If you have any questions about this Agreement, please contact us by email or regular mail at the
                following address:
            </p>
            <p>NetServ
            </p>
            <p>San Jose</p>
        </div>
        <br>
    </div>
</div>
<!-- Team Section End -->
</div>
<!-- Main content End -->
<!-- Footer Start -->
<?php include 'footer.php';?>
<!-- Footer End -->
<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->
<?php include 'service_jslinks.php'; ?>
</body>
</html>