<!DOCTYPE html>
<html lang="en">

<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - UX-engineer  </title>
    <meta name="description" content="Analyze business requirements to develop technical network solutions and standard frameworks.">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/ux-engineer " />
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'>
        {
            "@context": "http://www.schema.org",
            "@type": "WebSite",
            "name": "NetSev",
            "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .bg4 {
        background-image: url(assets/images/bg/bg4.png)
    }

    .rs-collaboration.style1 .img-part img {
        position: relative;
        bottom: 0
    }

    .rs-services.style22 .service-wrap .icon-part img {
        width: 53px;
        height: 53px;
        max-width: unset
    }

    ul.listing-style li {
        position: relative;
        padding-left: 30px;
        line-height: 34px;
        font-weight: 500;
        font-size: 14px
    }

    ul.listing-style.regular2 li {
        font-weight: 400;
        margin-bottom: 0
    }

    .rs-about.style10 .accordion .card .card-body {
        background: #fff
    }
</style>

<body class="home-eight">
<!-- Preloader area start here -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--End preloader here -->
<!--Full width header Start-->
<div class="full-width-header header-style4">
    <!--header-->
    <?php include 'header.php'; ?>
    <!--Header End-->
</div>
<!--Full width header End-->
<!-- Main content Start -->
<div class="main-content">
    <!-- Services Section Start -->
    <div class="rs-pricing style1">
        <div class="top-part bg10 pt-93 pb-124 md-pt-73 sm-pb-100">
            <div class="container">
                <div class="sec-title">
                    <!-- <div class="sub-title white-color">Pricing Plan</div> -->
                    <h1 class="title white-color mb-0 text-center" style="font-size: 36px;">UX Engineer </h1>
                    <div class="sub-title text-center white-color">Budapest (Onsite) | 8+ Years experience</div>
                </div>
            </div>
        </div>
    </div>
    <div id="rs-services" class="rs-services single pt-100 pb-100 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-left">
                        <p> <span class="txt_clr"><strong>Designation</strong> : </span> UX Engineer</p>
                        <p> <span class="txt_clr"><strong>Salary </strong> : </span>  Best in Industry</p>
                        <p> <span class="txt_clr"><strong>Experience </strong> : </span>  8+ years </p>
                        <p> <span class="txt_clr"><strong>Joining  </strong> : </span>Immediate/15 days </p>
                        <p> <span class="txt_clr"><strong>Location  </strong> : </span>Budapest </p>
                        <p> <span class="txt_clr"><strong>Roles and Responsibilities</strong></span>
                        </p>

                        <ol>
                            <li>Translate concepts into user flows, wireframes, mock-ups, and prototypes that lead to intuitive user experiences.</li>
                            <li>Facilitate the client’s product vision by researching, conceiving, sketching, prototyping and user-testing experiences for digital products.</li>
                            <li>Design and deliver wireframes, user stories, user journeys, and mock-ups optimized for a wide range of devices and interfaces.</li>
                            <li>Identify design problems and devise elegant solutions.</li>
                            <li>Make strategic design and user-experience decisions related to core, and new, functions and features.</li>
                            <li>Take a user-centered design approach and rapidly test and iterate your designs.</li>
                            <li>Collaborate with other team members and stakeholders.</li>
                            <li>Ask smart questions, take risks and champion new ideas.</li>
                            <li>Ability to prepare user training content - Videos, mailers etc.</li>


                        </ol>
                        </p>
                        <p><span class="txt_clr"><strong>Requirements</strong> </span>
                        <ol>
                            <li>Expertise in standard UX software such as Sketch, OmniGraffle, Axure, InVision, UXPin, Balsamiq, Framer, and the like is a must. Basic HTML5, CSS3, and JavaScript skills are a plus.</li>
                            <li>Ability to work with clients to understand detailed requirements and design complete user experiences that meet client needs and vision.</li>
                            <li>Extensive experience in using UX design best practices to design solutions, and a deep understanding of mobile-first and responsive design.</li>
                            <li>A solid grasp of user-centered design (UCD), planning and conducting user research, user testing, A/B testing, rapid prototyping, heuristic analysis, usability and accessibility concerns.</li>
                            <li>Ability to iterate designs and solutions efficiently and intelligently.</li>
                            <li>Ability to clearly and effectively communicate design processes, ideas, and solutions to teams and clients.</li>
                            <li>A clear understanding of the importance of user-centered design and design thinking.</li>
                            <li>Ability to work effectively in a team setting including synthesizing abstract ideas into concrete design implications.</li>
                            <li>Be excited about collaborating and communicating closely with teams and other stakeholders via a distributed model, to regularly deliver design solutions for approval.</li>
                            <li>Be passionate about resolving user pain points through great design.</li>
                            <li>Be open to receiving feedback and constructive criticism.</li>
                            <li>Be passionate about all things UX and other areas of design and innovation. Research and showcase knowledge in the industry’s latest trends and technologies.</li>

                        </ol>
                        </p>

                    </div>
                    <div class="btn-part">
                        <a href="mailto:hr@ngnetserv.com" class="btn btn-primary">Apply</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main content End -->
<!-- Footer Start -->
<?php include 'footer.php'; ?>
<!-- Footer End -->
<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->
<?php include 'service_jslinks.php'; ?>
</body>

</html>