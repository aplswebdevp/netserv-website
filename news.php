<!DOCTYPE html>
<html lang="en">
<head>
    <!-- meta tag -->
    <meta charset="utf-8">
    <title>NetServ - Cybersecurity News</title>
    <meta name="description" content="Get latest security updates or news to proactively mitigate threats. Improve your threat detection. Netserv brings you latest news from tech industry. Subscribe to our managed SOC services.">
    <meta name="keywords" content=" managed soc, security news, mitigate threats, threat detection, managed services">
    <!-- responsive tag -->
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="canonical" href="https://www.ngnetserv.com/career"/>
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.png">
    <?php include 'service_csslinks.php'; ?>
    <script type='application/ld+json'>
        {
            "@context": "http://www.schema.org",
            "@type": "WebSite",
            "name": "NetSev",
            "url": "http://www.ngnetserv.com/"
        }
    </script>
</head>
<style type="text/css">
    .bg4{background-image:url(assets/images/bg/bg4.png)}.rs-collaboration.style1 .img-part img{position:relative;bottom:0}.rs-services.style22 .service-wrap .icon-part img{width:53px;height:53px;max-width:unset}ul.listing-style li{position:relative;padding-left:30px;line-height:34px;font-weight:500;font-size:14px}ul.listing-style.regular2 li{font-weight:400;margin-bottom:0}.rs-about.style10 .accordion .card .card-body{background:#fff}
    .accordian-space{
        margin-bottom: 3px!important;
    }
</style>
<body class="home-eight">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VL7HQH"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Preloader area start here -->

<!--End preloader here -->
<!--Full width header Start-->
<div class="full-width-header header-style4">
    <!--header-->
    <?php include 'header.php'; ?>
    <!--Header End-->
</div>
<!-- Main content Start -->
<div class="main-content">
    <!-- Breadcrumbs Section Start -->
    <div class="rs-breadcrumbs bg-8">
        <div class="container">
            <div class="content-part text-center pt-160 pb-160">
                <h1 class="breadcrumbs-title white-color mb-0">Cybersecurity News</h1>
            </div>
        </div>
    </div>
    <!-- Breadcrumbs Section End -->

    <!-- Blog Section  Start -->
    <div class="rs-blog inner pt-100 pb-100 md-pt-80 md-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">

                    <div class="blog-wrap shadow mb-70 xs-mb-50">
                        <div class="image-part">
                            <a href="https://www.msspalert.com/cybersecurity-breaches-and-attacks/ransomware/two-separate-ransomware-groups-attack-one-healthcare-network/?utm_medium=email&utm_source=sendpress&utm_campaign" target="_blank"><img src="assets/images/news11-min.jpg" alt=""></a>
                        </div>
                        <div class="content-part">
                            <h3 class="title mb-10"><a href="https://www.msspalert.com/cybersecurity-breaches-and-attacks/ransomware/two-separate-ransomware-groups-attack-one-healthcare-network/?utm_medium=email&utm_source=sendpress&utm_campaign" target="_blank">Two Separate Ransomware Groups Attack One Healthcare Network</a></h3>
                            <p class="desc mb-20">A ransomware attack on a Canadian healthcare organization in late 2021 saw two separate threat actors simultaneously inside a company’s network, one exfiltrating data and the other encrypting information, a new Sophos report said.</p>
                            <div class="btn-part">
                                <a class="readon-arrow" href="https://www.msspalert.com/cybersecurity-breaches-and-attacks/ransomware/two-separate-ransomware-groups-attack-one-healthcare-network/?utm_medium=email&utm_source=sendpress&utm_campaign" target="_blank">Read more</a>
                            </div>
                        </div>
                    </div>
                    <div class="blog-wrap shadow">
                        <div class="image-part">
                            <a href="https://www.msspalert.com/cybersecurity-guests/why-effective-security-operations-is-the-key-defense-against-ransomware/" target="_blank"><img src="assets/images/news22-min.jpg" alt=""></a>
                        </div>
                        <div class="content-part">
                            <h3 class="title mb-10"><a href="https://www.msspalert.com/cybersecurity-guests/why-effective-security-operations-is-the-key-defense-against-ransomware/" target="_blank">Why Effective Security Operations Is the Key Defense Against Ransomware</a></h3>
                            <p class="desc mb-20">Ransomware poses a genuine risk to ending any business, regardless of its current size, maturity, or success. </p>
                            <div class="btn-part">
                                <a class="readon-arrow" href="https://www.msspalert.com/cybersecurity-guests/why-effective-security-operations-is-the-key-defense-against-ransomware/" target="_blank">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 md-mb-50 pl-35 lg-pl-15 md-order-first">
                    <div id="sticky-sidebar" class="blog-sidebar">
                        <div class="sidebar-search sidebar-grid shadow mb-50">
                            <form class="search-bar">
                                <input type="text" placeholder="Search...">
                                <span>
                                          <button type="submit"><i class="flaticon-search"></i></button>
                                        </span>
                            </form>
                        </div>

                        <div class="sidebar-popular-post sidebar-grid shadow mb-50">
                            <div class="sidebar-title">
                                <h3 class="title semi-bold mb-20">Recent news </h3>
                            </div>
                            <div class="single-post mb-20">
<!--                                <div class="post-image">-->
<!--                                    <a href="blog-single.html"><img src="assets/images/news1-min.png" alt="post image"></a>-->
<!--                                </div>-->
                                <div class="post-desc">
                                    <div class="post-title">
                                        <h5 class="margin-0"><a href="https://www.msspalert.com/cybersecurity-breaches-and-attacks/ransomware/two-separate-ransomware-groups-attack-one-healthcare-network/?utm_medium=email&utm_source=sendpress&utm_campaign" target="_blank">Two Separate Ransomware Groups Attack One Healthcare Network </a></h5>
                                    </div>
                                    <ul>
                                        <li><i class="fa fa-calendar"></i> 28 June, 2019</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="single-post mb-20">
<!--                                <div class="post-image">-->
<!--                                    <a href="blog-single.html"><img src="assets/images/news2-min.png" alt="post image"></a>-->
<!--                                </div>-->
                                <div class="post-desc">
                                    <div class="post-title">
                                        <h5 class="margin-0"><a href="https://www.msspalert.com/cybersecurity-guests/why-effective-security-operations-is-the-key-defense-against-ransomware/" target="_blank">Why Effective Security Operations Is the Key Defense Against Ransomware </a></h5>
                                    </div>
                                    <ul>
                                        <li><i class="fa fa-calendar"></i> 28 June, 2019</li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
            <div id="sticky-end"></div>
        </div>
    </div>
    <!-- Blog Section  End -->
</div>
<!-- Main content End -->
<!-- Footer Start -->
<?php include 'footer.php'; ?>
<!-- Footer End -->
<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>
<!-- End scrollUp  -->
<?php include 'service_jslinks.php'; ?>
</body>

</html>